﻿using GameAssets.Scripts.Base.Interface;

namespace GameAssets.Scripts.Base
{
    public class BaseCommand : ICommand
    {
        public virtual void Execute()
        {
        }

        public virtual void Undo()
        {
        }

        public virtual bool IsFinished()
        {
            return false;
        }
    }
}