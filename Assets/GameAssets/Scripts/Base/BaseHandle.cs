﻿using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.UI.UIGameStart;
using GameAssets.Scripts.UI.UIPlayMenu;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.Base
{
    public class BaseHandle : MonoBehaviour, IPointerDownHandler, IDragHandler,
        IPointerUpHandler, IBeginDragHandler, IEndDragHandler
    {
        [ShowIf(nameof(canHold))] [Range(0.15f, 0.5f)] [SerializeField]
        private float holdTime = 0.25f;

        [SerializeField] private bool canHold;
        [SerializeField] private bool canDragDrop;
        [SerializeField] private bool useHandleInterface;

        public Vector3 Origin { get; set; }
        public Vector3 End { get; set; }
        public Vector3 Delta { get; set; }

        public bool Dragged { get; set; }

        private float _holdTime;

        private bool _holdDown;

        protected virtual void Update()
        {
            switch (useHandleInterface)
            {
                case true when _holdDown:
                {
                    if (_holdTime <= 0f)
                    {
                        return;
                    }

                    _holdTime -= Time.deltaTime;

                    if (!(_holdTime <= 0f))
                    {
                        return;
                    }

                    _holdDown = false;

                    DoHandleHoldComplete();
                    break;
                }

                case false:
                    switch (Application.platform)
                    {
                        case RuntimePlatform.Android:
                        case RuntimePlatform.IPhonePlayer:
                            if (Input.touchCount > 0)
                            {
                                var touch = Input.GetTouch(0);

                                switch (touch.phase)
                                {
                                    case TouchPhase.Began:
                                        DoHandleDown();
                                        break;

                                    case TouchPhase.Moved:
                                    case TouchPhase.Stationary:
                                        DoHandleDrag();
                                        break;

                                    case TouchPhase.Ended:
                                        DoHandleUp();
                                        break;
                                }
                            }

                            break;

                        case RuntimePlatform.WindowsEditor:
                        case RuntimePlatform.WindowsPlayer:
                        case RuntimePlatform.WebGLPlayer:
                            if (Input.GetMouseButtonDown(0))
                            {
                                DoHandleDown();
                            }
                            else if (Input.GetMouseButton(0))
                            {
                                DoHandleDrag();
                            }
                            else if (Input.GetMouseButtonUp(0))
                            {
                                DoHandleUp();
                            }

                            break;
                    }

                    break;
            }
        }

        protected virtual void DoHandleDown()
        {
            Origin = MathfEx.PlatformPosition;

            if (canHold)
            {
                return;
            }

            Dragged = false;
            End = MathfEx.PlatformPosition;
        }

        protected virtual void DoHandleDrag()
        {
            if (canDragDrop)
            {
                Dragged = !MathfEx.Equal(MathfEx.PlatformPosition, End);

                if (Dragged)
                {
                    Delta = MathfEx.PlatformPosition - End;

                    if (Delta != Vector3.zero && UIManager.Instance.IsUI<UIPlayMenu>())
                    {
                        UIManager.Instance.HideAllAndShowUI<UIGameStart>();
                    }

                    End = MathfEx.PlatformPosition;
                }
                else
                {
                    Delta = Vector3.zero;
                }
            }
        }

        protected virtual void DoHandleUp()
        {
            if (Dragged)
            {
                Dragged = false;
            }

            Delta = Vector3.zero;
        }

        protected virtual void DoHandleHoldComplete() { }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (canHold)
            {
                _holdTime = holdTime;
                _holdDown = true;
            }
            else if (canDragDrop)
            {
                Origin = MathfEx.PlatformPosition;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_holdDown)
            {
                _holdDown = false;
            }
            else if (canDragDrop)
            {
                DoHandleUp();
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (canDragDrop)
            {
                DoHandleDrag();
            }
            else if (_holdDown)
            {
                _holdTime = holdTime;

                DoHandleDrag();
            }
        }

        public virtual void OnBeginDrag(PointerEventData eventData) { }

        public virtual void OnEndDrag(PointerEventData eventData) { }
    }
}