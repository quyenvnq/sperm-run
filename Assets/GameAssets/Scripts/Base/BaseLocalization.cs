﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Base.Interface;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Manager.Localization;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.Base
{
    [Serializable]
    internal class Localization<T>
    {
        public SystemLanguage language;
        public T value;
    }

    internal class LocalizationComparer : IEqualityComparer<SystemLanguage>
    {
        public bool Equals(SystemLanguage x, SystemLanguage y)
        {
            return x == y;
        }

        public int GetHashCode(SystemLanguage obj)
        {
            return obj.GetHashCode();
        }
    }

    public abstract class BaseLocalization<TUI, TValue> : MonoBehaviour, IInitialize
        where TUI : UIBehaviour
    {
        [SerializeField] private List<Localization<TValue>> localizationValues
            = new List<Localization<TValue>>();

        [SerializeField] protected TUI main;

        private Dictionary<SystemLanguage, TValue> dictionaries = new Dictionary<SystemLanguage, TValue>();

        protected virtual void Awake()
        {
            dictionaries = localizationValues.ToDictionary(
                x => x.language,
                x => x.value,
                new LocalizationComparer());

            ListenerChangedLanguage(LocalizationManager.Instance.LanguageDefault());

            GameEvent.OnChangeLanguage += ListenerChangedLanguage;
        }

        private void OnValidate()
        {
            Initialize();
        }

        private void OnDestroy()
        {
            GameEvent.OnChangeLanguage -= ListenerChangedLanguage;
        }

        private void ListenerChangedLanguage(SystemLanguage language)
        {
            if (dictionaries.ContainsKey(language))
            {
                ChangeValue(language, dictionaries[language]);
            }
            else
            {
                var zero = dictionaries.ElementAt(0);

                if (!string.Equals(zero.Value.ToString(), default(TValue)?.ToString(),
                    StringComparison.CurrentCultureIgnoreCase))
                {
                    ChangeValue(zero.Key, zero.Value);
                }
            }
        }

        protected abstract void ChangeValue(SystemLanguage language, object value);

        [Button]
        public virtual void Initialize()
        {
            main = GetComponent<TUI>();
        }
    }
}