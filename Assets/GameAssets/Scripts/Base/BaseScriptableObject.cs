﻿using System;
using GameAssets.Scripts.Base.Interface;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Base
{
    public class BaseScriptableObject : ScriptableObject, IInitialize
    {
        [ReadOnly] public string id;

        public string description;

        private void OnValidate()
        {
            if (string.IsNullOrEmpty(id))
            {
                Initialize();
            }
        }

        [Button]
        public void Initialize()
        {
            id = Guid.NewGuid().ToString();
        }
    }

    [Serializable]
    public class BaseScriptableObjectData
    {
        public string id;
        public string description;
    }
}