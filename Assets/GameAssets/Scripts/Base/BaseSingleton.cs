﻿using GameAssets.Scripts.Base.Interface;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Base
{
    public class BaseSingleton<T> : MonoBehaviour, ISingleton where T : MonoBehaviour
    {
        [SerializeField] private bool isDontDestroy;

        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();
                }

                return _instance;
            }

            private set => _instance = value;
        }

        protected virtual void Awake()
        {
            if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (!isDontDestroy)
            {
                return;
            }

            if (transform.parent != null)
            {
                transform.SetParent(null);
            }

            DontDestroyOnLoad(this);
        }

        private void Update()
        {
            if (GameManager.Instance.IsGameState(GameState.Playing))
            {
                InnerUpdate();
            }
            else
            {
                PreInnerUpdate();
            }
        }

        private void FixedUpdate()
        {
            if (GameManager.Instance.IsGameState(GameState.Playing))
            {
                InnerFixedUpdate();
            }
            else
            {
                PreInnerFixedUpdate();
            }
        }

        private void LateUpdate()
        {
            if (GameManager.Instance.IsGameState(GameState.Playing))
            {
                InnerLateUpdate();
            }
            else
            {
                PreInnerLateUpdate();
            }
        }

        protected virtual void OnDestroy()
        {
            if (Equals(Instance, this))
            {
                Instance = default;
            }
        }

        [Button]
        public virtual void Initialize()
        {
        }

        public virtual void PreInnerUpdate()
        {
        }

        public virtual void InnerUpdate()
        {
        }

        public virtual void PreInnerFixedUpdate()
        {
        }

        public virtual void InnerFixedUpdate()
        {
        }

        public virtual void PreInnerLateUpdate()
        {
        }

        public virtual void InnerLateUpdate()
        {
        }
    }
}