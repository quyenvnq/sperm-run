using DG.Tweening;
using GameAssets.Scripts.General;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.Base
{
    public sealed class BaseStat : MonoBehaviour
    {
        [ReadOnly] public CharacterState characterState;
        public PriorityLevel priorityLevel;

        [Header("[HEALTH FILL]")] [SerializeField]
        private Image imgHealthFill;

        [SerializeField] private float fillDuration = 0.5f;

        [Header("[BASE STAT]")] [SerializeField]
        private float maxHp;

        public float atk;
        public float atkSpeed;
        public float atkRange;
        public float vision;
        public float moveSpeed;

        public float RestHealth => CurrentHp / maxHp;
        
        public bool IsAttack { get; set; }
        public bool IsAlive { get; private set; } = true;

        public float CurrentHp { get; set; }

        private bool IsMove { get; set; }

        private void OnEnable()
        {
            ResetHp();
        }

        private void UpdateHealthFill()
        {
            if (imgHealthFill != null)
            {
                imgHealthFill.DOFillAmount(CurrentHp / maxHp, fillDuration);
            }
        }

        public void ResetHp()
        {
            CurrentHp = maxHp;

            ChangeCharacterState(CharacterState.IdleOrRespawn);
        }

        public void AddHp(float dmg)
        {
            CurrentHp += dmg;

            if (CurrentHp <= 0)
            {
                IsAlive = false;
            }

            UpdateHealthFill();
        }

        public void AddMaxHp(float hp)
        {
            maxHp += hp;
            CurrentHp = maxHp;
        }

        public bool IsCharacterState(CharacterState state)
        {
            return characterState == state;
        }

        public void SetCharacterState(CharacterState state)
        {
            characterState = state;
        }

        public void ChangeCharacterState(CharacterState state)
        {
            if (IsCharacterState(state))
            {
                return;
            }

            SetCharacterState(state);
            Debug.Log($"CS => {state}");

            switch (state)
            {
                case CharacterState.IdleOrRespawn:
                    IsMove = false;
                    IsAlive = true;
                    IsAttack = false;
                    break;

                case CharacterState.Move:
                    IsMove = true;
                    IsAttack = false;
                    IsAlive = true;
                    break;

                case CharacterState.Attack:
                    IsMove = false;
                    IsAttack = true;
                    IsAlive = true;
                    break;

                case CharacterState.Death:
                    IsMove = false;
                    IsAttack = false;
                    IsAlive = false;
                    break;
            }
        }
    }
}