﻿using System;
using UnityEngine;

namespace GameAssets.Scripts.Base
{
    public class BaseUI : MonoBehaviour
    {
        private bool _loading;

        #region MonoBehaviour

        protected virtual void OnValidate()
        {
            name = GetType().Name;
        }

        #endregion

        #region Public

        public void DoAnimation(bool active)
        {
            if (active)
            {
                if (!gameObject.activeInHierarchy)
                {
                    gameObject.SetActive(true);
                }
            }
            else
            {
                if (gameObject.activeInHierarchy)
                {
                    gameObject.SetActive(false);
                }
            }
        }

        #endregion

        #region Private

        #endregion

        #region Protected

        protected void Click(Action callback)
        {
            if (_loading)
            {
                return;
            }

            _loading = true;

            callback();

            _loading = false;
        }

        #endregion
    }
}