﻿using System;
using GameAssets.Scripts.Ex;
using UnityEngine;

namespace GameAssets.Scripts.Base
{
    public class BaseView : MonoBehaviour
    {
        private static bool _loading;

        public void DoAnimation(bool active)
        {
            if (active)
            {
                if (!gameObject.activeInHierarchy)
                {
                    gameObject.SetActive(true);
                }
            }
            else
            {
                if (gameObject.activeInHierarchy)
                {
                    gameObject.SetActive(false);
                }
            }
        }

        protected void Click(Action callback)
        {
            if (_loading)
            {
                SpawnerEx.CreateNotification("Đang xử lý ...");
                return;
            }

            _loading = true;

            callback();

            _loading = false;
        }
    }
}