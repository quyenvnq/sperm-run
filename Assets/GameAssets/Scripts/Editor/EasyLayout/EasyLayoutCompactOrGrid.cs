﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Base class for the compact and grid layout groups.
    /// </summary>
    public abstract class EasyLayoutCompactOrGrid : EasyLayoutBaseType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EasyLayoutCompactOrGrid"/> class.
        /// </summary>
        /// <param name="layout">Layout.</param>
        protected EasyLayoutCompactOrGrid(EasyLayout layout)
            : base(layout)
        {
        }

        #region Position

        /// <summary>
        /// Row aligns.
        /// </summary>
        protected static readonly List<float> RowAligns = new List<float>
        {
            0.0f, // HorizontalAligns.Left
            0.5f, // HorizontalAligns.Center
            1.0f, // HorizontalAligns.Right
        };

        /// <summary>
        /// Inner aligns.
        /// </summary>
        protected static readonly List<float> InnerAligns = new List<float>
        {
            0.0f, // InnerAligns.Top
            0.5f, // InnerAligns.Middle
            1.0f, // InnerAligns.Bottom
        };

        private readonly List<float> _rowsWidths = new List<float>();
        private readonly List<float> _maxColumnsWidths = new List<float>();

        private readonly List<float> _columnsHeights = new List<float>();
        private readonly List<float> _maxRowsHeights = new List<float>();

        private void CalculateRowsWidths()
        {
            _rowsWidths.Clear();

            for (var i = 0; i < elementsGroup.Rows; i++)
            {
                var row = elementsGroup.GetRow(i);
                var rowWidth = (row.Count - 1) * layout.Spacing.x + row.Sum(element => element.Width);

                _rowsWidths.Add(rowWidth);
            }
        }

        private void CalculateMaxColumnsWidths()
        {
            _maxColumnsWidths.Clear();

            for (var i = 0; i < elementsGroup.Rows; i++)
            {
                var row = elementsGroup.GetRow(i);

                for (var j = 0; j < row.Count; j++)
                {
                    if (_maxColumnsWidths.Count == j)
                    {
                        _maxColumnsWidths.Add(0);
                    }

                    _maxColumnsWidths[j] = Mathf.Max(_maxColumnsWidths[j], row[j].Width);
                }
            }
        }

        private void CalculateColumnsHeights()
        {
            _columnsHeights.Clear();

            for (var i = 0; i < elementsGroup.Columns; i++)
            {
                var column = elementsGroup.GetColumn(i);
                var columnHeight = (column.Count - 1) * layout.Spacing.y +
                                   column.Sum(element => element.Height);
                _columnsHeights.Add(columnHeight);
            }
        }

        private void CalculateMaxRowsHeights()
        {
            _maxRowsHeights.Clear();

            for (var i = 0; i < elementsGroup.Rows; i++)
            {
                var row = elementsGroup.GetRow(i);
                var rowHeight = row.Aggregate(0f, (current, element) => Mathf.Max(current, element.Height));

                _maxRowsHeights.Add(rowHeight);
            }
        }

        private static Vector2 GetMaxCellSize(IEnumerable<LayoutElementInfo> row)
        {
            var x = 0f;
            var y = 0f;

            foreach (var t in row)
            {
                x = Mathf.Max(x, t.Width);
                y = Mathf.Max(y, t.Height);
            }

            return new Vector2(x, y);
        }

        /// <summary>
        /// Get aligned width.
        /// </summary>
        /// <param name="element">Element.</param>
        /// <param name="maxWidth">Maximum width.</param>
        /// <param name="cellMaxSize">Max size of the cell.</param>
        /// <param name="emptyWidth">Width of the empty space.</param>
        /// <returns>Aligned width.</returns>
        protected abstract Vector2 GetAlignByWidth(LayoutElementInfo element, float maxWidth,
            Vector2 cellMaxSize, float emptyWidth);

        /// <summary>
        /// Get aligned height.
        /// </summary>
        /// <param name="element">Element.</param>
        /// <param name="maxHeight">Maximum height.</param>
        /// <param name="cellMaxSize">Max size of the cell.</param>
        /// <param name="emptyHeight">Height of the empty space.</param>
        /// <returns>Aligned height.</returns>
        protected abstract Vector2 GetAlignByHeight(LayoutElementInfo element, float maxHeight,
            Vector2 cellMaxSize, float emptyHeight);

        private Vector2 CalculateOffset(Vector2 size)
        {
            var rectTransform = layout.transform as RectTransform;
            var anchorPosition = GroupPositions[(int) layout.GroupPosition];

            if (rectTransform is null)
            {
                return anchorPosition;
            }

            var startPosition = new Vector2(
                rectTransform.rect.width * (anchorPosition.x - rectTransform.pivot.x),
                rectTransform.rect.height * (anchorPosition.y - rectTransform.pivot.y));

            startPosition.x -= anchorPosition.x * size.x;
            startPosition.y += (1 - anchorPosition.y) * size.y;

            startPosition.x += layout.GetMarginLeft() * (1 - anchorPosition.x * 2);
            startPosition.y += layout.GetMarginTop() * (1 - anchorPosition.y * 2);

            startPosition.x += layout.PaddingInner.left;
            startPosition.y -= layout.PaddingInner.top;
            return startPosition;
        }

        /// <summary>
        /// Calculate group size.
        /// </summary>
        /// <returns>Size.</returns>
        protected override Vector2 CalculateGroupSize()
        {
            return CalculateGroupSize(true, layout.Spacing,
                new Vector2(layout.PaddingInner.Horizontal, layout.PaddingInner.Vertical));
        }

        /// <summary>
        /// Calculate positions of the elements.
        /// </summary>
        /// <param name="size">Size.</param>
        protected override void CalculatePositions(Vector2 size)
        {
            var offset = CalculateOffset(size);

            if (layout.IsHorizontal)
            {
                CalculatePositionsHorizontal(size, offset);
            }
            else
            {
                CalculatePositionsVertical(size, offset);
            }
        }

        private void CalculatePositionsHorizontal(Vector2 size, Vector2 offset)
        {
            var position = offset;

            CalculateRowsWidths();
            CalculateMaxColumnsWidths();

            for (var x = 0; x < elementsGroup.Rows; x++)
            {
                var row = elementsGroup.GetRow(x);
                var rowCellMaxSize = GetMaxCellSize(row);

                foreach (var element in row)
                {
                    var align = GetAlignByWidth(element, _maxColumnsWidths[element.column],
                        rowCellMaxSize, size.x - _rowsWidths[x]);

                    element.PositionTopLeft = GetElementPosition(position, align);

                    position.x += (layout.LayoutType == LayoutTypes.Compact
                        ? element.Width
                        : _maxColumnsWidths[element.column]) + layout.Spacing.x;
                }

                position.x = offset.x;
                position.y -= rowCellMaxSize.y + layout.Spacing.y;
            }
        }

        private Vector2 CalculatePositionsVertical(Vector2 size, Vector2 offset)
        {
            var position = offset;

            CalculateMaxRowsHeights();
            CalculateColumnsHeights();

            for (var y = 0; y < elementsGroup.Columns; y++)
            {
                var column = elementsGroup.GetColumn(y);
                var columnCellMAXSize = GetMaxCellSize(column);

                foreach (var element in column)
                {
                    var align = GetAlignByHeight(element, _maxRowsHeights[element.row],
                        columnCellMAXSize, size.y - _columnsHeights[y]);

                    element.PositionTopLeft = GetElementPosition(position, align);
                    position.y -= (layout.LayoutType == LayoutTypes.Compact
                        ? element.Height
                        : _maxRowsHeights[element.row]) + layout.Spacing.y;
                }

                position.y = offset.y;
                position.x += columnCellMAXSize.x + layout.Spacing.x;
            }

            return size;
        }

        /// <summary>
        /// Gets the user interface element position.
        /// </summary>
        /// <returns>The user interface position.</returns>
        /// <param name="position">Position.</param>
        /// <param name="align">Align.</param>
        private static Vector2 GetElementPosition(Vector2 position, Vector2 align)
        {
            return new Vector2(position.x + align.x, position.y - align.y);
        }

        #endregion

        #region Sizes

        /// <summary>
        /// Shrink elements on overflow.
        /// </summary>
        protected void ShrinkOnOverflow()
        {
            if (elementsGroup.Count == 0)
            {
                return;
            }

            var size = layout.InternalSize;
            var rows = elementsGroup.Rows - 1;
            var columns = elementsGroup.Columns - 1;

            var sizeWithoutSpacing = new Vector2(size.x - layout.Spacing.x * columns,
                size.y - layout.Spacing.y * rows);
            var uiSizeWithoutSpacing = CalculateGroupSize();

            uiSizeWithoutSpacing.x -= layout.Spacing.x * columns;
            uiSizeWithoutSpacing.x -= layout.Spacing.y * rows;

            var scale = GetShrinkScale(sizeWithoutSpacing, uiSizeWithoutSpacing);

            foreach (var element in elementsGroup.Elements)
            {
                element.NewWidth = element.Width * scale;
                element.NewHeight = element.Height * scale;
            }
        }

        private static float GetShrinkScale(Vector2 requiredSize, Vector2 currentSize)
        {
            var scale = requiredSize.x / currentSize.x;
            return scale > 1 || currentSize.y * scale > requiredSize.y
                ? Mathf.Min(1f, requiredSize.y / currentSize.y)
                : Mathf.Min(1f, scale);
        }

        /// <summary>
        /// Shrink columns width to fit.
        /// </summary>
        protected void ShrinkColumnWidthToFit()
        {
            ShrinkToFit(layout.InternalSize.x, elementsGroup, layout.Spacing.x,
                RectTransform.Axis.Horizontal);
        }

        /// <summary>
        /// Resize columns width to fit.
        /// </summary>
        /// <param name="increaseOnly">Size can be only increased.</param>
        protected void ResizeColumnWidthToFit(bool increaseOnly)
        {
            ResizeToFit(layout.InternalSize.x, elementsGroup, layout.Spacing.x,
                RectTransform.Axis.Horizontal, increaseOnly);
        }

        #endregion
    }
}