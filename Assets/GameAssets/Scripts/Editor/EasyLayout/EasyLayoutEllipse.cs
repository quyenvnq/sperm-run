﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Ellipse layout group.
    /// </summary>
    public class EasyLayoutEllipse : EasyLayoutBaseType
    {
        private readonly List<LayoutElementInfo> _ellipseGroup = new List<LayoutElementInfo>();

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyLayoutEllipse"/> class.
        /// </summary>
        /// <param name="layout">Layout.</param>
        public EasyLayoutEllipse(EasyLayout layout)
            : base(layout)
        {
        }

        /// <summary>
        /// Get element position in the group.
        /// </summary>
        /// <param name="element">Element.</param>
        /// <returns>Position.</returns>
        public override EasyLayoutPosition GetElementPosition(RectTransform element)
        {
            var targetID = element.GetInstanceID();

            for (var x = 0; x < _ellipseGroup.Count; x++)
            {
                if (_ellipseGroup[x].Rect.GetInstanceID() == targetID)
                {
                    return new EasyLayoutPosition(x, 0);
                }
            }

            return new EasyLayoutPosition(-1, -1);
        }

        /// <summary>
        /// Calculate group size.
        /// </summary>
        /// <returns>Size.</returns>
        protected override Vector2 CalculateGroupSize()
        {
            var settings = layout.EllipseSettings;
            var size = layout.InternalSize;

            if (!settings.WidthAuto)
            {
                size.x = settings.Width;
            }

            if (!settings.HeightAuto)
            {
                size.y = settings.Height;
            }

            var maxSize = FindElementsMaxSize();
            var align = GetAlignRate(settings.Align) * 2f;

            if (settings.ElementsRotate)
            {
                var rotationAngle = Mathf.Deg2Rad * settings.ElementsRotationStart;
                var rotationSin = Math.Sin(rotationAngle);
                var rotationCos = Math.Cos(rotationAngle);
                var length = maxSize.x * rotationCos + maxSize.y * rotationSin;
                size.x -= Mathf.Abs((float) length) * (align + 1f);
                size.y -= Mathf.Abs((float) length) * (align + 1f);
            }
            else
            {
                size.x -= maxSize.x * (align + 1f);
                size.y -= maxSize.y * (align + 1f);
            }

            size.x = Mathf.Max(1f, size.x);
            size.y = Mathf.Max(1f, size.y);
            return size;
        }

        private Vector2 FindElementsMaxSize()
        {
            var maxSize = Vector2.zero;

            foreach (var element in elementsGroup.Elements)
            {
                maxSize.x = Mathf.Max(maxSize.x, element.Width);
                maxSize.y = Mathf.Max(maxSize.y, element.Height);
            }

            return maxSize;
        }

        /// <summary>
        /// Calculate positions of the elements.
        /// </summary>
        /// <param name="size">Size.</param>
        protected override void CalculatePositions(Vector2 size)
        {
            if (_ellipseGroup.Count == 0)
            {
                return;
            }

            var settings = layout.EllipseSettings;

            var angleAuto = settings.Fill == EllipseFill.Closed
                ? 360f / _ellipseGroup.Count
                : settings.ArcLength / Mathf.Max(1, _ellipseGroup.Count - 1);
            var angleStep = settings.AngleStepAuto ? angleAuto : settings.AngleStep;

            var angle = settings.AngleStart + settings.AngleFiller + settings.AngleScroll;

            var center = new Vector2(size.x / 2.0f, size.y / 2.0f);
            var align = GetAlignRate(settings.Align);

            var rotationAngleRad = Mathf.Deg2Rad * settings.ElementsRotationStart;
            var rotationSin = (float) Math.Sin(rotationAngleRad);
            var rotationCos = (float) Math.Cos(rotationAngleRad);
            var pivot = new Vector2(0.5f, 0.5f);

            foreach (var element in _ellipseGroup)
            {
                element.NewPivot = pivot;

                var positionAngleRad = Mathf.Deg2Rad * angle;
                var positionSin = Mathf.Sin(positionAngleRad);
                var positionCos = Mathf.Cos(positionAngleRad);

                var elementPos = new Vector2(center.x * positionCos, center.y * positionSin);

                if (settings.ElementsRotate)
                {
                    var length = Mathf.Abs(element.Width * rotationCos) +
                                 Mathf.Abs(element.Height * rotationSin);
                    var alignFix = new Vector2(
                        length * positionCos * align,
                        length * positionSin * align);
                    elementPos += alignFix;
                }
                else
                {
                    var alignFix = new Vector2(
                        element.Width * positionCos * align,
                        element.Height * positionSin * align);
                    elementPos += alignFix;
                }

                element.NewEulerAnglesZ = settings.ElementsRotate
                    ? angle + settings.ElementsRotationStart
                    : 0f;
                element.positionPivot = elementPos;
                angle -= angleStep;
            }
        }

        private static float GetAlignRate(EllipseAlign align)
        {
            switch (align)
            {
                case EllipseAlign.Outer:
                    return -0.5f;

                case EllipseAlign.Center:
                    return 0f;

                case EllipseAlign.Inner:
                    return 0.5f;

                default:
                    Debug.LogWarning($"Unknown ellipse align: {align}");
                    break;
            }

            return 0f;
        }

        /// <summary>
        /// Calculate sizes of the elements.
        /// </summary>
        protected override void CalculateSizes()
        {
        }

        /// <summary>
        /// Group elements.
        /// </summary>
        protected override void Group()
        {
            _ellipseGroup.Clear();
            _ellipseGroup.AddRange(elementsGroup.Elements);

            if (layout.RightToLeft)
            {
                _ellipseGroup.Reverse();
            }
        }
    }
}