﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// EasyLayout utilities.
    /// </summary>
    public static class EasyLayoutEx
    {
        /// <summary>
        /// Maximum count of items in group.
        /// </summary>
        /// <param name="grp">Group.</param>
        /// <returns>Maximum count.</returns>
        public static int MaxCount(IEnumerable<List<LayoutElementInfo>> grp)
        {
            return grp.Aggregate(0, (current, t) => Mathf.Max(current, t.Count));
        }

        /// <summary>
        /// Transpose the specified group.
        /// </summary>
        /// <param name="group">Group.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        /// <returns>Transposed list.</returns>
        public static List<List<T>> Transpose<T>(List<List<T>> group)
        {
            var result = new List<List<T>>();

            foreach (var x in @group)
            {
                for (var j = 0; j < x.Count; j++)
                {
                    if (result.Count <= j)
                    {
                        result.Add(new List<T>());
                    }

                    result[j].Add(x[j]);
                }
            }

            return result;
        }

        /// <summary>
        /// Get scaled width.
        /// </summary>
        /// <returns>The width.</returns>
        /// <param name="ui">User interface.</param>
        public static float ScaledWidth(RectTransform ui)
        {
            return ui.rect.width * ui.localScale.x;
        }

        /// <summary>
        /// Get scaled height.
        /// </summary>
        /// <returns>The height.</returns>
        /// <param name="ui">User interface.</param>
        public static float ScaledHeight(RectTransform ui)
        {
            return ui.rect.height * ui.localScale.y;
        }

        /// <summary>
        /// Convert the specified input with converter.
        /// </summary>
        /// <param name="input">Input.</param>
        /// <param name="converter">Converter.</param>
        /// <typeparam name="TInput">The 1st type parameter.</typeparam>
        /// <typeparam name="TOutput">The 2nd type parameter.</typeparam>
        /// <returns>List with converted items.</returns>
        public static List<TOutput> Convert<TInput, TOutput>(this List<TInput> input,
            Converter<TInput, TOutput> converter)
        {
#if NETFX_CORE
			var output = new List<TOutput>(input.Count);
			for (int i = 0; i < input.Count; i++)
			{
				output.Add(converter(input[i]));
			}
			
			return output;
#else
            return input.ConvertAll(converter);
#endif
        }

        /// <summary>
        /// Apply for each item in the list.
        /// </summary>
        /// <param name="source">List.</param>
        /// <param name="action">Action.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var element in source)
            {
                action(element);
            }
        }

#if NETFX_CORE
		/// <summary>
		/// Apply for each item in the list.
		/// </summary>
		/// <param name="input">Input.</param>
		/// <param name="action">Action.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static void ForEach<T>(this List<T> list, Action<T> action)
		{
			for (int i = 0; i < list.Count; i++)
			{
				action(list[i]);
			}
		}
#endif
    }
}