﻿using System;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Flex settings.
    /// </summary>
    [Serializable]
    public class EasyLayoutFlexSettings : INotifyPropertyChanged
    {
        /// <summary>
        /// Content positions.
        /// </summary>
        [Serializable]
        public enum Content
        {
            /// <summary>
            /// Position at the start of the block.
            /// </summary>
            Start = 0,

            /// <summary>
            /// Position at the center of the block.
            /// </summary>
            Center = 1,

            /// <summary>
            /// Position at the end of the block.
            /// </summary>
            End = 2,

            /// <summary>
            /// Position with space between.
            /// </summary>
            SpaceBetween = 3,

            /// <summary>
            /// Position with space around.
            /// </summary>
            SpaceAround = 4,

            /// <summary>
            /// Position with space evenly.
            /// </summary>
            SpaceEvenly = 5,
        }

        /// <summary>
        /// Items align.
        /// </summary>
        [Serializable]
        public enum Items
        {
            /// <summary>
            /// Start position.
            /// </summary>
            Start = 0,

            /// <summary>
            /// Center position.
            /// </summary>
            Center = 1,

            /// <summary>
            /// End position.
            /// </summary>
            End = 2,
        }

        [SerializeField] private bool wrap = true;

        /// <summary>
        /// Wrap.
        /// </summary>
        public bool Wrap
        {
            get => wrap;

            set
            {
                if (wrap == value)
                {
                    return;
                }

                wrap = value;

                Changed("Wrap");
            }
        }

        [SerializeField] private Content justifyContent = Content.Start;

        /// <summary>
        /// Elements positions at the main axis.
        /// </summary>
        public Content JustifyContent
        {
            get => justifyContent;

            set
            {
                if (justifyContent == value)
                {
                    return;
                }

                justifyContent = value;

                Changed("JustifyContent");
            }
        }

        [SerializeField] private Content alignContent = Content.Start;

        /// <summary>
        /// Elements positions at the sub axis.
        /// </summary>
        public Content AlignContent
        {
            get => alignContent;

            set
            {
                if (alignContent == value)
                {
                    return;
                }

                alignContent = value;

                Changed("AlignContent");
            }
        }

        [SerializeField] private Items alignItems = Items.Start;

        /// <summary>
        /// Items align.
        /// </summary>
        public Items AlignItems
        {
            get => alignItems;

            set
            {
                if (alignItems == value)
                {
                    return;
                }

                alignItems = value;

                Changed("AlignItems");
            }
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = EasyLayout.DefaultPropertyHandler;

        /// <summary>
        /// Property changed.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        protected void Changed(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}