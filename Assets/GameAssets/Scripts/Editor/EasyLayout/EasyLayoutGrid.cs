﻿using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Easy layout grid layout.
    /// </summary>
    public class EasyLayoutGrid : EasyLayoutCompactOrGrid
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EasyLayoutGrid"/> class.
        /// </summary>
        /// <param name="layout">Layout.</param>
        public EasyLayoutGrid(EasyLayout layout)
            : base(layout)
        {
        }

        /// <summary>
        /// Gets the max columns count.
        /// </summary>
        /// <returns>The max columns count.</returns>
        /// <param name="maxColumns">Max columns.</param>
        private int GetMaxColumnsCount(int maxColumns)
        {
            var baseLength = layout.MainAxisSize;
            var length = baseLength;
            var spacing = layout.IsHorizontal ? layout.Spacing.x : layout.Spacing.y;

            var minColumnsSet = false;
            var minColumns = maxColumns;
            var currentColumns = 0;

            foreach (var element in elementsGroup.Elements)
            {
                if (currentColumns == maxColumns)
                {
                    minColumnsSet = true;
                    minColumns = Mathf.Min(minColumns, currentColumns);

                    currentColumns = 1;
                    length = baseLength - element.AxisSize;
                    continue;
                }

                if (currentColumns == 0)
                {
                    currentColumns = 1;
                    length = baseLength - element.AxisSize;
                    continue;
                }

                if (length >= element.AxisSize + spacing)
                {
                    length -= element.AxisSize + spacing;
                    currentColumns++;
                }
                else
                {
                    minColumnsSet = true;
                    minColumns = Mathf.Min(minColumns, currentColumns);

                    currentColumns = 1;
                    length = baseLength - element.AxisSize;
                }
            }

            if (!minColumnsSet)
            {
                minColumns = currentColumns;
            }

            return minColumns;
        }

        /// <summary>
        /// Group the specified uiElements.
        /// </summary>
        protected override void Group()
        {
            switch (layout.GridConstraint)
            {
                case GridConstraint.Flexible:
                    GroupFlexible();
                    break;

                case GridConstraint.FixedRowCount:
                    GroupByRows();
                    break;

                case GridConstraint.FixedColumnCount:
                    GroupByColumns();
                    break;
            }

            if (!layout.TopToBottom)
            {
                elementsGroup.BottomToTop();
            }

            if (layout.RightToLeft)
            {
                elementsGroup.RightToLeft();
            }
        }

        /// <summary>
        /// Group the specified uiElements.
        /// </summary>
        private void GroupFlexible()
        {
            var maxColumns = 999999;

            while (true)
            {
                var newMAXColumns = GetMaxColumnsCount(maxColumns);

                if (maxColumns == newMAXColumns || newMAXColumns == 1)
                {
                    break;
                }

                maxColumns = newMAXColumns;
            }

            if (layout.IsHorizontal)
            {
                GroupByColumnsHorizontal(maxColumns);
            }
            else
            {
                GroupByRowsVertical(maxColumns);
            }
        }

        /// <summary>
        /// Calculate sizes of the elements.
        /// </summary>
        protected override void CalculateSizes()
        {
            if (layout.ChildrenWidth == ChildrenSize.ShrinkOnOverflow &&
                layout.ChildrenHeight == ChildrenSize.ShrinkOnOverflow)
            {
                ShrinkOnOverflow();
            }
            else
            {
                switch (layout.ChildrenWidth)
                {
                    case ChildrenSize.FitContainer:
                        ResizeColumnWidthToFit(false);
                        break;

                    case ChildrenSize.SetPreferredAndFitContainer:
                        ResizeColumnWidthToFit(true);
                        break;

                    case ChildrenSize.ShrinkOnOverflow:
                        ShrinkColumnWidthToFit();
                        break;
                }

                switch (layout.ChildrenHeight)
                {
                    case ChildrenSize.FitContainer:
                        ResizeRowHeightToFit(false);
                        break;

                    case ChildrenSize.SetPreferredAndFitContainer:
                        ResizeRowHeightToFit(true);
                        break;

                    case ChildrenSize.ShrinkOnOverflow:
                        ShrinkRowHeightToFit();
                        break;
                }
            }
        }

        /// <summary>
        /// Get aligned width.
        /// </summary>
        /// <param name="element">Element.</param>
        /// <param name="maxWidth">Maximum width.</param>
        /// <param name="cellMaxSize">Max size of the cell.</param>
        /// <param name="emptyWidth">Width of the empty space.</param>
        /// <returns>Aligned width.</returns>
        protected override Vector2 GetAlignByWidth(LayoutElementInfo element, float maxWidth,
            Vector2 cellMaxSize, float emptyWidth)
        {
            var cellAlign = GroupPositions[(int) layout.CellAlign];
            return new Vector2(
                (maxWidth - element.Width) * cellAlign.x,
                (cellMaxSize.y - element.Height) * (1 - cellAlign.y));
        }

        /// <summary>
        /// Get aligned height.
        /// </summary>
        /// <param name="element">Element.</param>
        /// <param name="maxHeight">Maximum height.</param>
        /// <param name="cellMaxSize">Max size of the cell.</param>
        /// <param name="emptyHeight">Height of the empty space.</param>
        /// <returns>Aligned height.</returns>
        protected override Vector2 GetAlignByHeight(LayoutElementInfo element, float maxHeight,
            Vector2 cellMaxSize, float emptyHeight)
        {
            var cellAlign = GroupPositions[(int) layout.CellAlign];
            return new Vector2(
                (cellMaxSize.x - element.Width) * cellAlign.x,
                (maxHeight - element.Height) * (1 - cellAlign.y));
        }
    }
}