﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace GameAssets.Scripts.Editor.EasyLayout.Editor
{
    /// <summary>
    /// Conditional editor.
    /// </summary>
    public abstract class ConditionalEditor : UnityEditor.Editor
    {
        /// <summary>
        /// Not displayable fields.
        /// </summary>
        protected List<string> ignoreFields;

        /// <summary>
        /// Fields to display.
        /// </summary>
        protected List<ConditionalFieldInfo> fields;

        /// <summary>
        /// Serialized properties.
        /// </summary>
        private readonly Dictionary<string, SerializedProperty> _serializedProperties =
            new Dictionary<string, SerializedProperty>();

        /// <summary>
        /// Serialized events.
        /// </summary>
        private readonly Dictionary<string, SerializedProperty> _serializedEvents =
            new Dictionary<string, SerializedProperty>();

        /// <summary>
        /// Init.
        /// </summary>
        protected virtual void OnEnable()
        {
            Init();
            _serializedProperties.Clear();

            foreach (var field in fields)
            {
                _serializedProperties[field.name] = null;
            }

            GetSerializedProperties();
        }

        /// <summary>
        /// Init this instance.
        /// </summary>
        protected abstract void Init();

        /// <summary>
        /// Get serialized properties.
        /// </summary>
        private void GetSerializedProperties()
        {
            var property = serializedObject.GetIterator();

            property.NextVisible(true);

            while (property.NextVisible(false))
            {
                if (IsEvent(property))
                {
                    _serializedEvents[property.name] = serializedObject.FindProperty(property.name);
                }
                else
                {
                    if (_serializedProperties.ContainsKey(property.name))
                    {
                        _serializedProperties[property.name] = serializedObject.FindProperty(property.name);
                    }
                    else if (!ignoreFields.Contains(property.name))
                    {
                        Debug.LogWarning($"Field info not found: {property.name}");
                    }
                }
            }
        }

        /// <summary>
        /// Is property event?
        /// </summary>
        /// <param name="property">Property</param>
        /// <returns>true if property is event; otherwise false.</returns>
        protected virtual bool IsEvent(SerializedProperty property)
        {
            var objectType = property.serializedObject.targetObject.GetType();
            var propertyType = objectType.GetField(property.propertyPath,
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            return propertyType != null && typeof(UnityEventBase).IsAssignableFrom(propertyType.FieldType);
        }

        /// <summary>
        /// Check is all displayable fields exists.
        /// </summary>
        /// <returns>true if all displayable fields exists; otherwise false.</returns>
        private bool AllFieldsExists()
        {
            var result = true;

            foreach (var x in _serializedProperties.Where(x => x.Value == null))
            {
                Debug.LogWarning($"Field with name '{x.Key}' not found");

                result = false;
            }

            return result;
        }

        /// <summary>
        /// Check is field can be displayed.
        /// </summary>
        /// <param name="info">Field info.</param>
        /// <returns>true if field can be displayed; otherwise false.</returns>
        private bool CanShow(ConditionalFieldInfo info)
        {
            return !(from condition in info.conditions
                let field = _serializedProperties[condition.Key]
                where !condition.Value(field)
                select condition).Any();
        }

        /// <summary>
        /// Draw inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            if (!AllFieldsExists())
            {
                return;
            }

            serializedObject.Update();

            foreach (var field in fields.Where(CanShow))
            {
                EditorGUI.indentLevel += field.indent;
                EditorGUILayout.PropertyField(_serializedProperties[field.name], true);
                EditorGUI.indentLevel -= field.indent;
            }

            foreach (var ev in _serializedEvents)
            {
                EditorGUILayout.PropertyField(ev.Value, true);
            }

            serializedObject.ApplyModifiedProperties();
            AdditionalGUI();
        }

        /// <summary>
        /// Display additional GUI.
        /// </summary>
        protected virtual void AdditionalGUI()
        {
        }
    }
}