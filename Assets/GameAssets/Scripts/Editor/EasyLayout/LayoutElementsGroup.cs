﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Layout elements group.
    /// </summary>
    public class LayoutElementsGroup
    {
        private int _rows;

        private int _columns;

        private readonly List<LayoutElementInfo> _rowElements = new List<LayoutElementInfo>();

        private readonly List<LayoutElementInfo> _columnElements = new List<LayoutElementInfo>();

        /// <summary>
        /// Elements.
        /// </summary>
        public List<LayoutElementInfo> Elements { get; private set; }

        /// <summary>
        /// Get element by index.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <returns>Element,</returns>
        public LayoutElementInfo this[int index] => Elements[index];

        /// <summary>
        /// Rows.
        /// </summary>
        public int Rows => _rows + 1;

        /// <summary>
        /// Columns.
        /// </summary>
        public int Columns => _columns + 1;

        /// <summary>
        /// Elements count.
        /// </summary>
        public int Count => Elements.Count;

        /// <summary>
        /// Set elements.
        /// </summary>
        /// <param name="newElements">Elements.</param>
        public void SetElements(List<LayoutElementInfo> newElements)
        {
            Elements = newElements;

            Clear();
        }

        /// <summary>
        /// Clear.
        /// </summary>
        public void Clear()
        {
            _rows = -1;
            _columns = -1;

            foreach (var element in Elements)
            {
                element.row = -1;
                element.column = -1;
            }
        }

        /// <summary>
        /// Set position of the element.
        /// </summary>
        /// <param name="index">Index of the element.</param>
        /// <param name="row">Row.</param>
        /// <param name="column">Column.</param>
        public void SetPosition(int index, int row, int column)
        {
            SetPosition(Elements[index], row, column);
        }

        /// <summary>
        /// Set position of the element.
        /// </summary>
        /// <param name="element">Element.</param>
        /// <param name="row">Row.</param>
        /// <param name="column">Column.</param>
        public void SetPosition(LayoutElementInfo element, int row, int column)
        {
            element.row = row;
            element.column = column;

            _rows = Mathf.Max(_rows, row);
            _columns = Mathf.Max(_columns, column);
        }

        /// <summary>
        /// Get elements at row.
        /// </summary>
        /// <param name="row">Row.</param>
        /// <returns>Elements.</returns>
        public List<LayoutElementInfo> GetRow(int row)
        {
            _rowElements.Clear();

            foreach (var elem in Elements.Where(elem => elem.row == row))
            {
                _rowElements.Add(elem);
            }

            return _rowElements;
        }

        /// <summary>
        /// Get elements at column.
        /// </summary>
        /// <param name="column">Column.</param>
        /// <returns>Elements.</returns>
        public List<LayoutElementInfo> GetColumn(int column)
        {
            _columnElements.Clear();

            foreach (var x in Elements.Where(x => x.column == column))
            {
                _columnElements.Add(x);
            }

            return _columnElements;
        }

        /// <summary>
        /// Get target position in the group.
        /// </summary>
        /// <param name="target">Target.</param>
        /// <returns>Position.</returns>
        public EasyLayoutPosition GetElementPosition(RectTransform target)
        {
            var targetID = target.GetInstanceID();

            foreach (var element in Elements.Where(element => element.Rect.GetInstanceID() == targetID))
            {
                return new EasyLayoutPosition(element.row, element.column);
            }

            return new EasyLayoutPosition(-1, -1);
        }

        /// <summary>
        /// Change elements order to bottom to top.
        /// </summary>
        public void BottomToTop()
        {
            foreach (var element in Elements)
            {
                element.row = _rows - element.row;
            }
        }

        /// <summary>
        /// Change elements order to right to left.
        /// </summary>
        public void RightToLeft()
        {
            for (var i = 0; i < Rows; i++)
            {
                var row = GetRow(i);

                foreach (var element in row)
                {
                    element.column = row.Count - 1 - element.column;
                }
            }
        }

        /// <summary>
        /// Get size.
        /// </summary>
        /// <param name="spacing">Spacing.</param>
        /// <param name="padding">Padding.</param>
        /// <returns>Size.</returns>
        public Vector2 Size(Vector2 spacing, Vector2 padding)
        {
            return new Vector2(HorizontalSize(spacing.x, padding.x),
                VerticalSize(spacing.y, padding.y));
        }

        private float HorizontalSize(float spacing, float padding)
        {
            var size = 0f;

            for (var i = 0; i < Rows; i++)
            {
                var block = GetRow(i);
                var blockSize = (block.Count - 1) * spacing + padding + block.Sum(element => element.Width);

                size = Mathf.Max(size, blockSize);
            }

            return size;
        }

        private float VerticalSize(float spacing, float padding)
        {
            var size = 0f;

            for (var i = 0; i < Columns; i++)
            {
                var block = GetColumn(i);
                var blockSize = (block.Count - 1) * spacing + padding + block.Sum(element => element.Height);

                size = Mathf.Max(size, blockSize);
            }

            return size;
        }

        /// <summary>
        /// Sort elements.
        /// </summary>
        public void Sort()
        {
            Elements.Sort(Comparison);
        }

        /// <summary>
        /// Compare elements by row and column.
        /// </summary>
        /// <param name="x">First element.</param>
        /// <param name="y">Second element.</param>
        /// <returns>Result of the comparison.</returns>
        private static int Comparison(LayoutElementInfo x, LayoutElementInfo y)
        {
            var rowComparison = x.row.CompareTo(y.row);
            return rowComparison == 0 ? x.column.CompareTo(y.column) : rowComparison;
        }
    }
}