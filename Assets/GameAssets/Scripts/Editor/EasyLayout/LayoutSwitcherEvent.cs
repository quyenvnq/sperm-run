﻿using System;
using UnityEngine.Events;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Layout switcher event.
    /// </summary>
    [Serializable]
    public class LayoutSwitcherEvent : UnityEvent<UILayout>
    {
    }
}