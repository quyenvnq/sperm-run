﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// User interface layout.
    /// </summary>
    [Serializable]
    public class UILayout
    {
        /// <summary>
        /// The aspect ratio.
        /// </summary>
        [SerializeField] public Vector2 aspectRatio;

        /// <summary>
        /// The aspect ratio as float.
        /// </summary>
        public float AspectRatioFloat => aspectRatio.x / aspectRatio.y;

        /// <summary>
        /// The maximum display size.
        /// </summary>
        [SerializeField] public float maxDisplaySize;

        /// <summary>
        /// The positions.
        /// </summary>
        [HideInInspector] public List<UIPosition> positions = new List<UIPosition>();

        /// <summary>
        /// Save the specified objects positions.
        /// </summary>
        /// <param name="objects">Objects.</param>
        public void Save(List<RectTransform> objects)
        {
            positions = Convert(objects);
        }

        /// <summary>
        /// Convert list.
        /// </summary>
        /// <param name="objects">Objects to convert.</param>
        /// <returns>Position.</returns>
        protected static List<UIPosition> Convert(List<RectTransform> objects)
        {
            var result = new List<UIPosition>(objects.Count);
            
            result.AddRange(objects.Select(SavePosition));
            return result;
        }

        /// <summary>
        /// Save the object position.
        /// </summary>
        /// <returns>The position.</returns>
        /// <param name="obj">Object.</param>
        protected static UIPosition SavePosition(RectTransform obj)
        {
            if (obj == null)
            {
                return null;
            }

            var position = new UIPosition
            {
                @object = obj,
                active = obj.gameObject.activeSelf,
                position = obj.localPosition,
                anchorMax = obj.anchorMax,
                anchorMin = obj.anchorMin,
                sizeDelta = obj.sizeDelta,
                pivot = obj.pivot,
                rotation = obj.localRotation.eulerAngles,
                scale = obj.localScale
            };

            return position;
        }

        /// <summary>
        /// Load this instance.
        /// </summary>
        public void Load()
        {
            positions.ForEach(Load);
        }

        /// <summary>
        /// Load the specified position.
        /// </summary>
        /// <param name="position">Position.</param>
        private static void Load(UIPosition position)
        {
            if (position == null)
            {
                return;
            }

            if (position.@object == null)
            {
                return;
            }

            var obj = position.@object;
            obj.gameObject.SetActive(position.active);

            obj.localPosition = position.position;
            obj.anchorMax = position.anchorMax;
            obj.anchorMin = position.anchorMin;
            obj.sizeDelta = position.sizeDelta;
            obj.pivot = position.pivot;

            obj.localRotation = Quaternion.Euler(position.rotation);
            obj.localScale = position.scale;
        }
    }
}