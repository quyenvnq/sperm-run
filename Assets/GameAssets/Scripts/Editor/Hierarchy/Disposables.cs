using System;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy
{
    public readonly struct GUIBackgroundColor : IDisposable
    {
        private readonly Color _before;

        public GUIBackgroundColor(Color color)
        {
            _before = GUI.backgroundColor;
            GUI.backgroundColor = color;
        }

        public void Dispose()
        {
            GUI.backgroundColor = _before;
        }
    }

    public readonly struct GUIContentColor : IDisposable
    {
        private readonly Color _before;

        public GUIContentColor(Color color)
        {
            _before = GUI.contentColor;
            GUI.contentColor = color;
        }

        public void Dispose()
        {
            GUI.contentColor = _before;
        }
    }

    public readonly struct GUIColor : IDisposable
    {
        private readonly Color _before;

        public GUIColor(Color color)
        {
            _before = GUI.color;
            GUI.color = color;
        }

        public GUIColor(Color color, float alpha)
        {
            _before = GUI.color;
            color.a = alpha;
            GUI.color = color;
        }

        public void Dispose()
        {
            GUI.color = _before;
        }
    }

    public sealed class GUIIndent : IDisposable
    {
        public GUIIndent()
        {
            EditorGUI.indentLevel++;
        }

        public GUIIndent(string label)
        {
            EditorGUILayout.LabelField(label);
            EditorGUI.indentLevel++;
        }

        public void Dispose()
        {
            EditorGUI.indentLevel--;
            EditorGUILayout.Separator();
        }
    }

    public readonly struct GUIEnabled : IDisposable
    {
        private readonly bool _before;

        public GUIEnabled(bool enabled)
        {
            _before = GUI.enabled;
            GUI.enabled = _before && enabled;
        }

        public void Dispose()
        {
            GUI.enabled = _before;
        }
    }

    public sealed class GUIFade : IDisposable
    {
        private AnimBool _anim;

        public bool Visible { get; private set; }

        public GUIFade()
        {
            Visible = true;
        }

        public void SetTarget(bool target)
        {
            if (_anim == null)
            {
                _anim = new AnimBool(target);

                _anim.valueChanged.AddListener(() =>
                {
                    if (EditorWindow.focusedWindow)
                    {
                        EditorWindow.focusedWindow.Repaint();
                    }
                });
            }

            _anim.target = target;
            Visible = EditorGUILayout.BeginFadeGroup(_anim.faded);
        }

        public void Dispose()
        {
            EditorGUILayout.EndFadeGroup();
        }
    }
}