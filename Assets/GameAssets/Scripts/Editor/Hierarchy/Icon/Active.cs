using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class Active : IconBase
    {
        protected override IconPosition Side => IconPosition.All;

        public override Texture2D PreferencesPreview => Utility.GetBackground(Styles.ActiveToggleStyle, true);

        public override void DoGUI(Rect rect)
        {
            using (new GUIBackgroundColor(EnhancedHierarchy.CurrentGameObject.activeSelf
                ? Styles.BackgroundColorEnabled
                : Styles.BackgroundColorDisabled))
            {
                GUI.changed = false;

                GUI.Toggle(rect, EnhancedHierarchy.CurrentGameObject.activeSelf,
                    Styles.ActiveContent, Styles.ActiveToggleStyle);

                if (!GUI.changed)
                {
                    return;
                }

                var objs = GetSelectedObjectsAndCurrent();
                var active = !EnhancedHierarchy.CurrentGameObject.activeSelf;

                // ReSharper disable once CoVariantArrayConversion
                Undo.RecordObjects(objs.ToArray(), EnhancedHierarchy.CurrentGameObject.activeSelf
                    ? "Disabled GameObject"
                    : "Enabled GameObject");

                foreach (var obj in objs)
                {
                    obj.SetActive(active);
                }
            }
        }
    }
}