using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class None : IconBase
    {
        public override float Width => 0f;
        public override string Name => "None";
        protected override IconPosition Side => IconPosition.All;

        public override void DoGUI(Rect rect)
        {
        }
    }
}