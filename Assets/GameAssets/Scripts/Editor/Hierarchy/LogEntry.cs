using System;
using System.Collections.Generic;
using GameAssets.Scripts.Editor.Hierarchy.Icon;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameAssets.Scripts.Editor.Hierarchy
{
    /// <summary>
    /// Log Entries from the console, to check if a game object has any errors or warnings.
    /// </summary>
    public sealed class LogEntry
    {
        private const double UPDATE_FREQUENCY = 0.75; // Every 750ms

        private static readonly Type LOGEntriesType;
        private static readonly Type LOGEntryType;

        public int RowIndex { get; }
        public string Condition { get; }
        public int ErrorNum { get; }
        public string File { get; }
        public int Line { get; }
        public int Column { get; }
        public EntryMode Mode { get; }
        public int InstanceID { get; }
        public int Identifier { get; }
        public Object ObjectReference { get; }
        public MonoScript Script { get; }
        public Type ClassType { get; }

        public static readonly Dictionary<GameObject, List<LogEntry>> GameObjectEntries =
            new Dictionary<GameObject, List<LogEntry>>(100);

        public static readonly List<LogEntry> CompileEntries = new List<LogEntry>(100);

        private static int _lastCount;
        private static bool _entriesDirty;
        private static bool _lastCompileFailedState;
        private static double _lastUpdatedTime;
        private static readonly Warning Warning = new Warning();

        static LogEntry()
        {
            try
            {
                LOGEntriesType = ReflectionEx.FindType("UnityEditorInternal.LogEntries");
                LOGEntryType = ReflectionEx.FindType("UnityEditorInternal.LogEntry");

                LOGEntriesType ??= ReflectionEx.FindType("UnityEditor.LogEntries");
                LOGEntryType ??= ReflectionEx.FindType("UnityEditor.LogEntry");

                ReloadReferences();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Preferences.ForceDisableButton(new Warning());
            }

            Application.logMessageReceived += (logString, stackTrace, type) => MarkEntriesDirty();

            EditorApplication.update += () =>
            {
                try
                {
                    if (!_entriesDirty && EditorUtility.scriptCompilationFailed !=
                        _lastCompileFailedState)
                    {
                        _lastCompileFailedState = EditorUtility.scriptCompilationFailed;

                        MarkEntriesDirty();
                    }

                    if (!(EditorApplication.timeSinceStartup - _lastUpdatedTime > UPDATE_FREQUENCY))
                    {
                        return;
                    }

                    if (!_entriesDirty)
                    {
                        var currentCount = GetLogCount();

                        if (_lastCount > currentCount)
                        {
                            // Console possibly cleared
                            if (Preferences.DebugEnabled)
                            {
                                Debug.Log("Detected console clear");
                            }

                            MarkEntriesDirty();
                        }

                        _lastCount = currentCount;
                    }

                    if (_entriesDirty)
                    {
                        ReloadReferences();
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    Preferences.ForceDisableButton(new Warning());
                }
            };
        }

        private LogEntry(object nativeEntry, int rowIndex)
        {
            RowIndex = rowIndex;

            if (nativeEntry.HasField("condition"))
            {
                Condition = nativeEntry.GetInstanceField<string>("condition");
            }
            else if (nativeEntry.HasField("message"))
            {
                Condition = nativeEntry.GetInstanceField<string>("message");
            }
            else
            {
                throw new MissingFieldException("LogEntry doesn't have a message field");
            }

            if (nativeEntry.HasField("errorNum"))
            {
                ErrorNum = nativeEntry.GetInstanceField<int>("errorNum");
            }

            File = nativeEntry.GetInstanceField<string>("file");
            Line = nativeEntry.GetInstanceField<int>("line");

            if (nativeEntry.HasField("column"))
            {
                Column = nativeEntry.GetInstanceField<int>("column");
            }

            Mode = nativeEntry.GetInstanceField<EntryMode>("mode");
            InstanceID = nativeEntry.GetInstanceField<int>("instanceID");
            Identifier = nativeEntry.GetInstanceField<int>("identifier");

            if (InstanceID != 0)
            {
                ObjectReference = EditorUtility.InstanceIDToObject(InstanceID);
            }

            if (ObjectReference)
            {
                Script = ObjectReference as MonoScript;
            }

            if (Script != null)
            {
                ClassType = Script.GetClass();
            }
        }

        public static void MarkEntriesDirty()
        {
            if (!_entriesDirty && Preferences.enabled && Preferences.IsButtonEnabled(Warning))
            {
                _entriesDirty = true;
            }
        }

        private static void ReloadReferences()
        {
            if (Preferences.DebugEnabled)
            {
                Debug.Log("Reloading Logs References");
            }

            GameObjectEntries.Clear();
            CompileEntries.Clear();

            try
            {
                var count = LOGEntriesType.InvokeStaticMethod<int>("StartGettingEntries");
                var nativeEntry = Activator.CreateInstance(LOGEntryType);

                for (var i = 0; i < count; i++)
                {
                    LOGEntriesType.InvokeStaticMethod("GetEntryInternal", i, nativeEntry);

                    var proxyEntry = new LogEntry(nativeEntry, i);
                    var go = proxyEntry.ObjectReference as GameObject;

                    if (proxyEntry.ObjectReference && !go)
                    {
                        var component = proxyEntry.ObjectReference as Component;

                        if (component != null)
                        {
                            go = component.gameObject;
                        }
                    }

                    if (proxyEntry.ClassType != null)
                    {
                        CompileEntries.Add(proxyEntry);
                    }

                    if (go == null)
                    {
                        continue;
                    }

                    if (GameObjectEntries.ContainsKey(go))
                    {
                        GameObjectEntries[go].Add(proxyEntry);
                    }
                    else
                    {
                        GameObjectEntries.Add(go, new List<LogEntry> {proxyEntry});
                    }
                }

                EditorApplication.RepaintHierarchyWindow();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Preferences.ForceDisableButton(new Warning());
            }
            finally
            {
                _entriesDirty = false;
                _lastUpdatedTime = EditorApplication.timeSinceStartup;

                LOGEntriesType.InvokeStaticMethod("EndGettingEntries");
            }
        }

        public bool HasMode(EntryMode toCheck)
        {
            return (Mode & toCheck) != 0;
        }

        public void OpenToEdit()
        {
            LOGEntriesType.InvokeStaticMethod("RowGotDoubleClicked", RowIndex);
        }

        private static int GetLogCount()
        {
            return LOGEntriesType.InvokeStaticMethod<int>("GetCount");
        }

        public override string ToString()
        {
            return Condition;
        }
    }
}