using System;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy
{
    /// <summary>
    /// Generic preference item interface.
    /// </summary>
    public interface IPrefItem
    {
        bool Drawing { get; }
        object Value { get; set; }

        GUIContent Label { get; }

        GUIEnabled GetEnabledScope();
        GUIEnabled GetEnabledScope(bool enabled);
        GUIFade GetFadeScope(bool enabled);
    }

    /// <summary>
    /// Generic preference item.
    /// </summary>
    [Serializable]
    public sealed class PrefItem<T> : IPrefItem
    {
        [Serializable]
        private struct Wrapper
        {
            [SerializeField] public T value;
        }

        private const string KEY_PREFIX = "EH.";

        private string _key;
        private Wrapper _wrapper;
        private T _defaultValue;

        private readonly GUIFade _fade;

        public GUIContent Label { get; private set; }

        public bool Drawing => _fade.Visible;

        public T DefaultValue
        {
            get => _defaultValue;
            set => SetDefaultValue(value);
        }

        public T Value
        {
            get => _wrapper.value;
            set => SetValue(value, false);
        }

        private bool UsingDefaultValue => !EditorPrefs.HasKey(_key);

        object IPrefItem.Value
        {
            get => Value;
            set => Value = (T) value;
        }

        public PrefItem(string key, T defaultValue, string text = "", string tooltip = "")
        {
            _key = KEY_PREFIX + key;
            _defaultValue = defaultValue;

            Label = new GUIContent(text, tooltip);
            _fade = new GUIFade();

            Preferences.Contents.Add(Label);

            Preferences.onResetPreferences += ResetValue;

            if (UsingDefaultValue)
            {
                _wrapper.value = Clone(defaultValue);
            }
            else
            {
                LoadValue();
            }
        }

        public void SetDefaultValue(T newDefault)
        {
            if (UsingDefaultValue)
            {
                _wrapper.value = Clone(newDefault);
            }

            _defaultValue = newDefault;
        }

        private void LoadValue()
        {
            try
            {
                if (!EditorPrefs.HasKey(_key))
                {
                    return;
                }

                var json = EditorPrefs.GetString(_key);

                _wrapper = JsonUtility.FromJson<Wrapper>(json);
            }
            catch (Exception e)
            {
                Debug.LogWarningFormat("Failed to load preference item \"{0}\", using default value: {1}", _key,
                    _defaultValue);
                Debug.LogException(e);
                ResetValue();
            }
        }

        private void SetValue(T newValue, bool forceSave)
        {
            try
            {
                if (Value != null && Value.Equals(newValue) && !forceSave)
                {
                    return;
                }

                _wrapper.value = newValue;

                var json = JsonUtility.ToJson(_wrapper, Preferences.DebugEnabled);

                EditorPrefs.SetString(_key, json);
            }
            catch (Exception e)
            {
                Debug.LogWarningFormat("Failed to save {0}: {1}", _key, e);
                Debug.LogException(e);
            }
            finally
            {
                _wrapper.value = newValue;
            }
        }

        private void ResetValue()
        {
            if (UsingDefaultValue)
            {
                return;
            }

            if (Preferences.DebugEnabled)
            {
                Debug.LogFormat("Deleted preference {0}", _key);
            }

            _wrapper.value = Clone(_defaultValue);

            EditorPrefs.DeleteKey(_key);
        }

        public void ForceSave()
        {
            SetValue(_wrapper.value, true);
        }

        private T Clone(T other)
        {
            if (typeof(T).IsValueType)
            {
                return other;
            }

            var wrapper = new Wrapper
            {
                value = other
            };

            return JsonUtility.FromJson<Wrapper>(JsonUtility.ToJson(wrapper, Preferences.DebugEnabled)).value;
        }

        public GUIEnabled GetEnabledScope()
        {
            return GetEnabledScope(Value.Equals(true));
        }

        public GUIEnabled GetEnabledScope(bool enabled)
        {
            return new GUIEnabled(enabled);
        }

        public GUIFade GetFadeScope(bool enabled)
        {
            _fade.SetTarget(enabled);
            return _fade;
        }

        public static implicit operator T(PrefItem<T> pb)
        {
            if (pb != null)
            {
                return pb.Value;
            }

            Debug.LogError("Cannot get the value of a null PrefItem");
            return default;
        }

        public static implicit operator GUIContent(PrefItem<T> pb)
        {
            if (pb != null)
            {
                return pb.Label;
            }

            Debug.LogError("Cannot get the content of a null PrefItem");
            return new GUIContent("Null PrefItem");
        }
    }
}