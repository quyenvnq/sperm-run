using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameAssets.Scripts.Editor.Hierarchy.Icon;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.Editor.Hierarchy
{
    public static partial class Preferences
    {
        public const string DEVELOPER_EMAIL = "samuelschultze@gmail.com";

        public static Action onResetPreferences = () => { };
        public static readonly List<GUIContent> Contents = new List<GUIContent>();

        public static readonly Version PluginVersion = new Version(2, 4, 5);
        public static readonly DateTime PluginDate = new DateTime(2020, 05, 04);

        private static readonly GUIContent ResetSettingsContent =
            new GUIContent("Use Defaults", "Reset all settings to default");

        private static readonly GUIContent UnlockAllContent = new GUIContent("Unlock All Objects",
            "Unlock all objects in the current scene, it's highly recommended to do this when disabling or removing the extension to prevent data loss\nThis might take a few seconds on large scenes");

        private static readonly GUIContent MailDeveloperContent = new GUIContent("Support Email",
            $"Request support from the developer\n\n{DEVELOPER_EMAIL}");

        private static readonly GUIContent VersionContent = new GUIContent(
            $"Version: {PluginVersion} ({PluginDate:d})");

        private static Vector2 _scroll;

        private static readonly ReorderableList LeftIconsList;
        private static readonly ReorderableList RightIconsList;
        private static readonly ReorderableList RowColorsList;

        private static readonly string[] MiniLabelsNames;

        private static GenericMenu LeftIconsMenu => GetGenericMenuForIcons(leftIcons, IconBase.AllLeftIcons);

        private static GenericMenu RightIconsMenu => GetGenericMenuForIcons(rightIcons, IconBase.AllRightIcons);

        private static GenericMenu RowColorsMenu
        {
            get
            {
                var menu = new GenericMenu();
                var randomColor = Random.ColorHSV(0f, 1f, 0.5f, 1f, 1f, 1f);

                randomColor.a = 0.3019608f;

                for (var i = 0; i < 32; i++)
                {
                    if (perLayerRowColors.Value.Contains(new LayerColor(i)))
                    {
                        continue;
                    }

                    var mode = perLayerRowColors.Value.LastOrDefault().mode;
                    var layerName = LayerMask.LayerToName(i);
                    var layer = new LayerColor(i, randomColor, mode);

                    if (string.IsNullOrEmpty(layerName))
                    {
                        layerName = $"Layer: {i}";
                    }

                    menu.AddItem(new GUIContent(layerName), false, () =>
                    {
                        RowColorsList.list.Add(layer);
                        perLayerRowColors.ForceSave();
                    });
                }

                return menu;
            }
        }

        private static GenericMenu GetGenericMenuForIcons<T>(PrefItem<T> preferenceItem,
            IEnumerable<IconBase> icons) where T : IList
        {
            var menu = new GenericMenu();

            foreach (var i in icons)
            {
                var icon = i;

                if (!preferenceItem.Value.Contains(icon) && icon != IconBase.None && icon != IconBase.None)
                {
                    menu.AddItem(new GUIContent(icon.Name), false, () =>
                    {
                        preferenceItem.Value.Add(icon);
                        preferenceItem.ForceSave();
                    });
                }
            }

            return menu;
        }

        private static ReorderableList GenerateReOrderAbleList<T>(PrefItem<T> preferenceItem)
            where T : IList
        {
            var result = new ReorderableList(preferenceItem.Value, typeof(T), true,
                true, true, true)
            {
                elementHeight = 18f,

                drawHeaderCallback = rect =>
                {
                    rect.xMin -= EditorGUI.indentLevel * 16f;

                    EditorGUI.LabelField(rect, preferenceItem, EditorStyles.boldLabel);
                }
            };

            result.onChangedCallback += list => preferenceItem.ForceSave();
            result.drawElementCallback = (rect, index, focused, active) =>
            {
                var icon = result.list[index] as IconBase;

                if (icon == null)
                {
                    EditorGUI.LabelField(rect, "INVALID ICON");
                    return;
                }

                var content = Utility.GetTempGUIContent(icon.Name, icon.PreferencesTooltip, icon.PreferencesPreview);
                var whiteTexture = !content.image || content.image.name.Contains("eh_icon_white");

                using (new GUIColor(whiteTexture && !EditorGUIUtility.isProSkin
                    ? Styles.BackgroundColorEnabled
                    : Color.white))
                {
                    EditorGUI.LabelField(rect, content);
                }
            };

            onResetPreferences += () => result.list = preferenceItem.Value;

            return result;
        }

        [SettingsProvider]
        private static SettingsProvider RetrieveSettingsProvider()
        {
            var settingsProvider = new SettingsProvider("Preferences/Enhanced Hierarchy",
                SettingsScope.User, Contents.Select(c => c.text))
            {
                guiHandler = OnPreferencesGUI
            };

            return settingsProvider;
        }

        private static void OnPreferencesGUI(string search)
        {
            _scroll = EditorGUILayout.BeginScrollView(_scroll, false, false);

            EditorGUILayout.Separator();

            enabled.DoGUI();

            EditorGUILayout.Separator();
            EditorGUILayout.HelpBox("Each item has a tooltip explaining what it does," +
                                    " keep the mouse over it to see.", MessageType.Info);
            EditorGUILayout.Separator();

            using (enabled.GetEnabledScope())
            {
                using (new GUIIndent("Misc settings"))
                {
                    using (new GUIIndent("Margins"))
                    {
                        rightMargin.DoGUISlider(-50, 50);

                        using (new GUIEnabled(Reflected.HierarchyArea.Supported))
                        {
                            leftMargin.DoGUISlider(-50, 50);
                            indent.DoGUISlider(0, 35);
                        }

                        if (!Reflected.HierarchyArea.Supported)
                        {
                            EditorGUILayout.HelpBox("Custom Indent and Margins are not supported in this Unity version",
                                MessageType.Warning);
                        }
                    }

                    iconsSize.DoGUISlider(13, 23);
                    treeOpacity.DoGUISlider(0f, 1f);

                    using (new GUIIndent())
                    {
                        using (selectOnTree.GetFadeScope(treeOpacity.Value > 0.01f))
                        {
                            selectOnTree.DoGUI();
                        }

                        using (treeStemProportion.GetFadeScope(treeOpacity.Value > 0.01f))
                        {
                            treeStemProportion.DoGUISlider(0f, 1f);
                        }
                    }

                    tooltips.DoGUI();

                    using (new GUIIndent())
                    using (relevantTooltipsOnly.GetFadeScope(tooltips))
                    {
                        relevantTooltipsOnly.DoGUI();
                    }

                    if (EnhancedSelectionSupported)
                    {
                        enhancedSelection.DoGUI();
                    }

                    trailing.DoGUI();
                    changeAllSelected.DoGUI();
                    numericChildExpand.DoGUI();

                    using (new GUIEnabled(Reflected.IconWidthSupported))
                    {
                        disableNativeIcon.DoGUI();
                    }

                    using (hideDefaultIcon.GetFadeScope(IsButtonEnabled(new GameObjectIcon())))
                    {
                        hideDefaultIcon.DoGUI();
                    }

                    using (openScriptsOfLogs.GetFadeScope(IsButtonEnabled(new Warning())))
                    {
                        openScriptsOfLogs.DoGUI();
                    }

                    GUI.changed = false;

                    using (allowSelectingLockedObjects.GetFadeScope(IsButtonEnabled(new Lock())))
                    {
                        allowSelectingLockedObjects.DoGUI();
                    }

#if !UNITY_2019_3_OR_NEWER
                    using(new GUIEnabled(false))
#endif
                    using (allowPickingLockedObjects.GetFadeScope(IsButtonEnabled(new Lock())))
                    {
                        allowPickingLockedObjects.DoGUI();
                    }

                    hoverTintColor.DoGUI();
                }

                using (new GUIIndent("Row separators"))
                {
                    lineSize.DoGUISlider(0, 6);

                    using (lineColor.GetFadeScope(lineSize > 0))
                    {
                        lineColor.DoGUI();
                    }

                    oddRowColor.DoGUI();
                    evenRowColor.DoGUI();

                    GUI.changed = false;

                    var rect = EditorGUILayout.GetControlRect(false, RowColorsList.GetHeight());

                    rect.xMin += EditorGUI.indentLevel * 16f;

                    RowColorsList.DoList(rect);
                }

                GUI.changed = false;
                miniLabels.Value[0] = EditorGUILayout.Popup("Mini Label Top", miniLabels.Value[0], MiniLabelsNames);
                miniLabels.Value[1] = EditorGUILayout.Popup("Mini Label Bottom", miniLabels.Value[1], MiniLabelsNames);

                if (GUI.changed)
                {
                    miniLabels.ForceSave();
                    RecreateMiniLabelProviders();
                }

                using (new GUIIndent())
                {
                    using (smallerMiniLabel.GetFadeScope(miniLabelProviders.Length > 0))
                    {
                        smallerMiniLabel.DoGUI();
                    }

                    using (hideDefaultTag.GetFadeScope(MiniLabelTagEnabled))
                    {
                        hideDefaultTag.DoGUI();
                    }

                    using (hideDefaultLayer.GetFadeScope(MiniLabelLayerEnabled))
                    {
                        hideDefaultLayer.DoGUI();
                    }

                    using (centralizeMiniLabelWhenPossible.GetFadeScope(miniLabelProviders.Length >= 2))
                    {
                        centralizeMiniLabelWhenPossible.DoGUI();
                    }
                }

                leftSideButtonPref.DoGUI();

                using (new GUIIndent())
                using (leftmostButton.GetFadeScope(LeftSideButton != IconBase.None))
                {
                    leftmostButton.DoGUI();
                }

                using (new GUIIndent("Children behaviour on change"))
                {
                    using (lockAskMode.GetFadeScope(IsButtonEnabled(new Lock())))
                    {
                        lockAskMode.DoGUI();
                    }

                    using (layerAskMode.GetFadeScope(IsButtonEnabled(new Layer()) || MiniLabelLayerEnabled))
                    {
                        layerAskMode.DoGUI();
                    }

                    using (tagAskMode.GetFadeScope(IsButtonEnabled(new Tag()) || MiniLabelTagEnabled))
                    {
                        tagAskMode.DoGUI();
                    }

                    using (staticAskMode.GetFadeScope(IsButtonEnabled(new Static())))
                    {
                        staticAskMode.DoGUI();
                    }

                    using (iconAskMode.GetFadeScope(IsButtonEnabled(new GameObjectIcon())))
                    {
                        iconAskMode.DoGUI();
                    }

                    EditorGUILayout.HelpBox(
                        $"Pressing down {Utility.CtrlKey} while clicking on a button will make it temporary have the opposite children change mode",
                        MessageType.Info);
                }

                LeftIconsList.displayAdd = LeftIconsMenu.GetItemCount() > 0;

                LeftIconsList.DoLayoutList();

                RightIconsList.displayAdd = RightIconsMenu.GetItemCount() > 0;

                RightIconsList.DoLayoutList();

                EditorGUILayout.HelpBox("Alt + Click on child expand toggle makes it expand all the grandchildren too",
                    MessageType.Info);

                if (IsButtonEnabled(new Memory()))
                {
                    EditorGUILayout.HelpBox(
                        "\"Memory Used\" may create garbage and consequently framerate stutterings, leave it disabled if maximum performance is important for your project",
                        MessageType.Warning);
                }

                if (IsButtonEnabled(new Lock()))
                {
                    EditorGUILayout.HelpBox(
                        "Remember to always unlock your game objects when removing or disabling this extension, as you won't be able to unlock without it and may lose scene data",
                        MessageType.Warning);
                }

                GUI.enabled = true;

                EditorGUILayout.EndScrollView();

                using (new EditorGUILayout.HorizontalScope())
                {
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.LabelField(VersionContent, GUILayout.Width(170f));
                }

                using (new EditorGUILayout.HorizontalScope())
                {
                    if (GUILayout.Button(ResetSettingsContent, GUILayout.Width(120f)))
                    {
                        onResetPreferences();
                    }

                    if (GUILayout.Button(MailDeveloperContent, GUILayout.Width(120f)))
                    {
                        OpenSupportEmail();
                    }
                }

                EditorGUILayout.Separator();
                Styles.ReloadTooltips();
                EditorApplication.RepaintHierarchyWindow();
            }
        }

        public static void OpenSupportEmail(Exception e = null)
        {
            Application.OpenURL(GetEmailURL(e));
        }

        private static string GetEmailURL(Exception e)
        {
            var full = new StringBuilder();
            var body = new StringBuilder();

            string EscapeURL(string url)
            {
                return UnityEngine.Networking.UnityWebRequest.EscapeURL(url).Replace("+", "%20");
            }

            body.Append(EscapeURL("\r\nDescribe your problem or make your request here\r\n"));
            body.Append(EscapeURL("\r\nAdditional Information:"));
            body.Append(EscapeURL($"\r\nVersion: {PluginVersion.ToString(3)}"));
            body.Append(EscapeURL($"\r\nUnity {InternalEditorUtility.GetFullUnityVersion()}"));
            body.Append(EscapeURL($"\r\n{SystemInfo.operatingSystem}"));

            if (e != null)
            {
                body.Append(EscapeURL($"\r\n{e}"));
            }

            full.Append("mailto:");
            full.Append(DEVELOPER_EMAIL);
            full.Append("?subject=");
            full.Append(EscapeURL("Enhanced Hierarchy - Support"));
            full.Append("&body=");
            full.Append(body);

            return full.ToString();
        }

        private static LayerColor LayerColorField(Rect rect, LayerColor layerColor)
        {
            var value = layerColor;
            var rect1 = rect;
            var rect2 = rect;
            var rect3 = rect;
            var rect4 = rect;

            rect1.xMax = rect1.xMin + 175f;
            rect2.xMin = rect1.xMax;
            rect2.xMax = rect2.xMin + 80f;
            rect3.xMin = rect2.xMax;
            rect3.xMax = rect3.xMin + 100;
            rect4.xMin = rect3.xMax;

            value.layer = EditorGUI.LayerField(rect1, value.layer);
            value.layer = EditorGUI.DelayedIntField(rect2, value.layer);
            value.color = EditorGUI.ColorField(rect3, value.color);
            value.mode = (TintMode) EditorGUI.EnumPopup(rect4, value.mode);

            return value.layer > 31 || value.layer < 0 ? layerColor : value;
        }

        private static void DoGUI(this PrefItem<int> prefItem)
        {
            if (prefItem.Drawing)
            {
                prefItem.Value = EditorGUILayout.IntField(prefItem, prefItem);
            }
        }

        private static void DoGUI(this PrefItem<float> prefItem)
        {
            if (prefItem.Drawing)
            {
                prefItem.Value = EditorGUILayout.FloatField(prefItem, prefItem);
            }
        }

        private static void DoGUISlider(this PrefItem<int> prefItem, int min, int max)
        {
            if (prefItem.Drawing)
            {
                prefItem.Value = EditorGUILayout.IntSlider(prefItem, prefItem, min, max);
            }
        }

        private static void DoGUISlider(this PrefItem<float> prefItem, float min, float max)
        {
            if (prefItem.Drawing)
            {
                prefItem.Value = EditorGUILayout.Slider(prefItem, prefItem, min, max);
            }
        }

        private static void DoGUI(this PrefItem<bool> prefItem)
        {
            if (prefItem.Drawing)
            {
                prefItem.Value = EditorGUILayout.Toggle(prefItem, prefItem);
            }
        }

        private static void DoGUI(this PrefItem<string> prefItem)
        {
            if (prefItem.Drawing)
            {
                prefItem.Value = EditorGUILayout.TextField(prefItem.Label, prefItem);
            }
        }

        private static void DoGUI(this PrefItem<Color> prefItem)
        {
            if (prefItem.Drawing)
            {
                prefItem.Value = EditorGUILayout.ColorField(prefItem, prefItem);
            }
        }

        private static void DoGUI<T>(this PrefItem<T> prefItem) where T : struct, IConvertible
        {
            if (prefItem.Drawing)
            {
                prefItem.Value = (T) (object) EditorGUILayout.EnumPopup(prefItem,
                    (Enum) (object) prefItem.Value);
            }
        }

        private static void DoGUI(this PrefItem<IconData> prefItem)
        {
            if (!prefItem.Drawing)
            {
                return;
            }

            var icons = IconBase.AllLeftOfNameIcons;
            var index = Array.IndexOf(icons, prefItem.Value.Icon);
            var labels = (from icon in icons select new GUIContent(icon)).ToArray();

            index = EditorGUILayout.Popup(prefItem, index, labels);

            if (index < 0 || index >= icons.Length)
            {
                return;
            }

            if (prefItem.Value.Icon.Name == icons[index].Name)
            {
                return;
            }

            prefItem.Value.Icon = icons[index];
            
            prefItem.ForceSave();
        }
    }
}