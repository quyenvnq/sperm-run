using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameAssets.Scripts.Editor.Hierarchy
{
    public static class Reflected
    {
        private static bool _gameObjectStylesTypeLoaded = false;
        private static Type _gameObjectTreeViewStylesType;

        private static readonly Type HierarchyWindowType =
            ReflectionEx.FindType("UnityEditor.SceneHierarchyWindow");

        private static EditorWindow _hierarchyWindowInstance;

        public static bool HierarchyFocused =>
            EditorWindow.focusedWindow && EditorWindow.focusedWindow.GetType() == HierarchyWindowType;

        public static Color PlaymodeTint
        {
            get
            {
                try
                {
                    return !EditorApplication.isPlayingOrWillChangePlaymode
                        ? Color.white
                        : ReflectionEx.FindType("UnityEditor.HostView")
                            .GetStaticField<object>("kPlayModeDarken")
                            .GetInstanceProperty<Color>("Color");
                }
                catch (Exception e)
                {
                    if (Preferences.DebugEnabled)
                    {
                        Debug.LogException(e);
                    }

                    return Color.white;
                }
            }
        }

        public static EditorWindow HierarchyWindowInstance
        {
            get
            {
                if (_hierarchyWindowInstance)
                {
                    return _hierarchyWindowInstance;
                }

                var lastHierarchy = (EditorWindow) null;

                try
                {
                    lastHierarchy = HierarchyWindowType.GetStaticField<EditorWindow>("s_LastInteractedHierarchy");
                }
                catch (Exception e)
                {
                    if (Preferences.DebugEnabled)
                    {
                        Debug.LogException(e);
                    }
                }

                return lastHierarchy != null
                    ? _hierarchyWindowInstance = lastHierarchy
                    : _hierarchyWindowInstance = (EditorWindow) Resources
                        .FindObjectsOfTypeAll(HierarchyWindowType).FirstOrDefault();
            }
        }

        public static void ShowIconSelector(Object[] targets, Rect activatorRect,
            bool showLabelIcons)
        {
            using (ProfilerSample.Get())
            {
                try
                {
                    var iconSelectorType = ReflectionEx.FindType("UnityEditor.IconSelector");

                    if (iconSelectorType.HasMethod<Object[], Rect, bool>("ShowAtPosition"))
                    {
                        if (!iconSelectorType.InvokeStaticMethod<bool, Object[], Rect, bool>(
                            "ShowAtPosition", targets, activatorRect, showLabelIcons))
                        {
                            Debug.LogWarning("Failed to open icon selector");
                        }

                        return;
                    }

                    var instance = ScriptableObject.CreateInstance(iconSelectorType);

                    if (instance.HasMethod<Object[], Rect, bool>("Init"))
                    {
                        instance.InvokeMethod("Init", targets, activatorRect, showLabelIcons);
                    }
                    else
                    {
                        var affectedObj = targets.FirstOrDefault();

                        instance.InvokeMethod("Init", affectedObj, activatorRect, showLabelIcons);

                        After.Condition(() => !instance, () =>
                        {
                            var icon = GetObjectIcon(affectedObj);

                            foreach (var obj in targets)
                            {
                                SetObjectIcon(obj, icon);
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    Debug.LogWarning($"Failed to open icon selector\n{e}");
                }
            }
        }

        public static void SetObjectIcon(Object obj, Texture2D texture)
        {
            using (ProfilerSample.Get())
            {
                typeof(EditorGUIUtility).InvokeStaticMethod("SetIconForObject", obj, texture);
                EditorUtility.SetDirty(obj);
            }
        }

        public static Texture2D GetObjectIcon(Object obj)
        {
            using (ProfilerSample.Get())
            {
                return typeof(EditorGUIUtility).InvokeStaticMethod<Texture2D, Object>("GetIconForObject", obj);
            }
        }

        public static bool GetTransformIsExpanded(GameObject go)
        {
            using (ProfilerSample.Get())
            {
                try
                {
                    var data = TreeView.GetInstanceProperty<object>("data");
                    return data.InvokeMethod<bool, int>("IsExpanded", go.GetInstanceID());
                }
                catch (Exception e)
                {
                    Preferences.numericChildExpand.Value = false;

                    Debug.LogException(e);
                    Debug.LogWarningFormat("Disabled \"{0}\" because it failed to get hierarchy info",
                        Preferences.numericChildExpand.Label.text);
                    return false;
                }
            }
        }

        public static void SetHierarchySelectionNeedSync()
        {
            using (ProfilerSample.Get())
            {
                try
                {
                    if (HierarchyWindowInstance)
                    {
                        SceneHierarchyOrWindow.SetInstanceProperty("selectionSyncNeeded", true);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogWarningFormat("Enabling \"{0}\" because it caused an exception",
                        Preferences.allowSelectingLockedObjects.Label.text);
                    Debug.LogException(e);

                    Preferences.allowSelectingLockedObjects.Value = true;
                }
            }
        }

        private static object SceneHierarchy => HierarchyWindowInstance.GetInstanceProperty<object>("sceneHierarchy");

        private static object SceneHierarchyOrWindow =>
            HierarchyWindowInstance.GetInstanceProperty<object>("sceneHierarchy");

        public static object TreeView => SceneHierarchyOrWindow.GetInstanceProperty<object>("treeView");

        public static object TreeViewGUI => TreeView.GetInstanceProperty<object>("gui");

        public static bool IconWidthSupported => TreeView != null && TreeViewGUI != null &&
                                                 TreeViewGUI.HasField("k_IconWidth");

        // Icon to the left side of obj name, introduced on Unity 2018.3
        public static float IconWidth
        {
            get => !IconWidthSupported ? 0 : TreeViewGUI.GetInstanceField<float>("k_IconWidth");
            set => TreeViewGUI.SetInstanceField("k_IconWidth", value);
        }

        public static class HierarchyArea
        {
            static HierarchyArea()
            {
                if (Preferences.DebugEnabled && !Supported)
                {
                    Debug.LogWarning("HierarchyArea not supported!");
                }
            }

            public static bool Supported
            {
                get
                {
                    try
                    {
                        return HierarchyWindowInstance != null && TreeView != null &&
                               TreeViewGUI != null;
                    }
                    catch
                    {
                        return false;
                    }
                }
            }

            public static float IndentWidth
            {
                get => TreeViewGUI.GetInstanceField<float>("k_IndentWidth");
                set => TreeViewGUI.SetInstanceField("k_IndentWidth", value);
            }

            private static float _baseIndentDefault = float.NaN;

            public static float BaseIndent
            {
                get
                {
                    var val = TreeViewGUI.GetInstanceField<float>("k_BaseIndent");

                    if (float.IsNaN(_baseIndentDefault))
                    {
                        _baseIndentDefault = val;
                    }

                    return val;
                }
                set
                {
                    if (float.IsNaN(_baseIndentDefault))
                    {
                        _baseIndentDefault = BaseIndent;
                    }

                    TreeViewGUI.SetInstanceField("k_BaseIndent", _baseIndentDefault + value);
                }
            }

            public static float BottomRowMargin
            {
                get => TreeViewGUI.GetInstanceField<float>("k_BottomRowMargin");
                set => TreeViewGUI.SetInstanceField("k_BottomRowMargin", value);
            }

            public static float TopRowMargin
            {
                get => TreeViewGUI.GetInstanceField<float>("k_TopRowMargin");
                set => TreeViewGUI.SetInstanceField("k_TopRowMargin", value);
            }

            public static float HalfDropBetweenHeight
            {
                get => TreeViewGUI.GetInstanceField<float>("k_HalfDropBetweenHeight");
                set => TreeViewGUI.SetInstanceField("k_HalfDropBetweenHeight", value);
            }

            public static float LineHeight
            {
                get => TreeViewGUI.GetInstanceField<float>("k_LineHeight");
                set => TreeViewGUI.SetInstanceField("k_LineHeight", value);
            }

            public static float SpaceBetweenIconAndText
            {
                get => TreeViewGUI.GetInstanceField<float>("k_SpaceBetweenIconAndText");
                set => TreeViewGUI.SetInstanceField("k_SpaceBetweenIconAndText", value);
            }

            public static float IconLeftPadding
            {
                get => TreeViewGUI.GetInstanceProperty<float>("iconLeftPadding");
                set => TreeViewGUI.SetInstanceProperty("iconLeftPadding", value);
            }

            public static float IconRightPadding
            {
                get => TreeViewGUI.GetInstanceProperty<float>("iconRightPadding");
                set => TreeViewGUI.SetInstanceProperty("iconRightPadding", value);
            }
        }

        private static Type GameObjectTreeViewStylesType
        {
            get
            {
                if (_gameObjectStylesTypeLoaded)
                {
                    return _gameObjectTreeViewStylesType;
                }

                _gameObjectStylesTypeLoaded = true;
                _gameObjectTreeViewStylesType =
                    TreeViewGUI.GetType().GetNestedType("GameObjectStyles", ReflectionEx.FULL_BINDING);
                return _gameObjectTreeViewStylesType;
            }
        }

        public static bool NativeHierarchyHoverTintSupported =>
            GameObjectTreeViewStylesType != null &&
            GameObjectTreeViewStylesType.HasField("hoveredBackgroundColor");

        // I implement the hover tint and then a few weeks later
        // unity implements it as a native feature
        public static Color NativeHierarchyHoverTint
        {
            get
            {
                if (!Preferences.DebugEnabled || NativeHierarchyHoverTintSupported)
                {
                    return GameObjectTreeViewStylesType.GetStaticField<Color>("hoveredBackgroundColor");
                }

                Debug.LogWarning("Native hover tint not supported!");
                return Color.clear;
            }

            set
            {
                if (Preferences.DebugEnabled && !NativeHierarchyHoverTintSupported)
                {
                    Debug.LogWarning("Native hover tint not supported!");
                    return;
                }

                GameObjectTreeViewStylesType.SetStaticField("hoveredBackgroundColor", value);
            }
        }
    }
}