using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy
{
    /// <summary>
    /// All the styles, icons, colors and contents used in hierarchy
    /// </summary>
    public static class Styles
    {
        static Styles()
        {
            if (EditorGUIUtility.isProSkin)
            {
                BackgroundColorEnabled = new Color32(155, 155, 155, 255);
                BackgroundColorDisabled = new Color32(155, 155, 155, 100);
                NormalColor = new Color32(56, 56, 56, 255);
                SelectedFocusedColor = new Color32(62, 95, 150, 255);
                SelectedUnfocusedColor = new Color32(72, 72, 72, 255);
            }
            else
            {
                BackgroundColorEnabled = new Color32(65, 65, 65, 255);
                BackgroundColorDisabled = new Color32(65, 65, 65, 120);
                NormalColor = new Color32(194, 194, 194, 255);
                SelectedFocusedColor = new Color32(62, 125, 231, 255);
                SelectedUnfocusedColor = new Color32(143, 143, 143, 255);
            }

            ChildToggleColor = new Color32(30, 30, 30, 255);
            MonoBehaviourIconTexture = Utility.FindOrLoad(MONO_BEHAVIOUR_ICON);

            var offTexture = Utility.FindOrLoad(LOCK_OFF);
            var onTexture = Utility.FindOrLoad(LOCK_ON);

            LockToggleStyle = Utility.CreateStyleFromTextures(onTexture, offTexture);

            offTexture = Utility.FindOrLoad(ACTIVE_OFF);
            onTexture = Utility.FindOrLoad(ACTIVE_ON);

            ActiveToggleStyle = Utility.CreateStyleFromTextures(onTexture, offTexture);

            offTexture = Utility.FindOrLoad(RENDERER_OFF);
            onTexture = Utility.FindOrLoad(RENDERER_ON);

            RendererToggleStyle = Utility.CreateStyleFromTextures(onTexture, offTexture);

            offTexture = Utility.FindOrLoad(STATIC_OFF);
            onTexture = Utility.FindOrLoad(STATIC_ON);

            StaticToggleStyle = Utility.CreateStyleFromTextures(onTexture, offTexture);

            onTexture = Utility.FindOrLoad(TAG);

            TagStyle = Utility.CreateStyleFromTextures(onTexture, onTexture);
            TagStyle.padding = new RectOffset(5, 17, 0, 1);
            TagStyle.border = new RectOffset();

            onTexture = Utility.FindOrLoad(LAYERS);

            LayerStyle = Utility.CreateStyleFromTextures(onTexture, onTexture);
            LayerStyle.padding = new RectOffset(5, 17, 0, 1);
            LayerStyle.border = new RectOffset();

            TreeLineTexture = Utility.FindOrLoad(TREE_LINE);
            TreeTeeTexture = Utility.FindOrLoad(TREE_TEE);
            TreeElbowTexture = Utility.FindOrLoad(TREE_ELBOW);

            InfoIcon = Utility.FindOrLoad(INFO);
            WarningIcon = Utility.FindOrLoad(WARNING);
            ErrorIcon = Utility.FindOrLoad(ERROR);

            FadeTexture = Utility.FindOrLoad(FADE);

            LabelNormal = new GUIStyle("PR Label");
            LabelDisabled = new GUIStyle("PR DisabledLabel");
            LabelPrefab = new GUIStyle("PR PrefabLabel");
            LabelPrefabDisabled = new GUIStyle("PR DisabledPrefabLabel");
            LabelPrefabBroken = new GUIStyle("PR BrokenPrefabLabel");
            LabelPrefabBrokenDisabled = new GUIStyle("PR DisabledBrokenPrefabLabel");

            MiniLabelStyle = new GUIStyle("ShurikenLabel")
            {
                alignment = TextAnchor.MiddleRight,
                clipping = TextClipping.Overflow,

                normal =
                {
                    textColor = Color.white
                },
                hover =
                {
                    textColor = Color.white
                },
                focused =
                {
                    textColor = Color.white
                },
                active =
                {
                    textColor = Color.white
                }
            };

            ApplyPrefabStyle = new GUIStyle("ShurikenLabel")
            {
                alignment = TextAnchor.MiddleCenter,
                clipping = TextClipping.Overflow,

                normal =
                {
                    textColor = Color.white
                },
                hover =
                {
                    textColor = Color.white
                },
                focused =
                {
                    textColor = Color.white
                },
                active =
                {
                    textColor = Color.white
                }
            };

            EditorApplication.update += () => { ApplyPrefabStyle.fontSize = Preferences.iconsSize - 6; };

            onTexture = Utility.FindOrLoad(CHILD_TOGGLE_ON);
            offTexture = Utility.FindOrLoad(CHILD_TOGGLE_OFF);

            IconButton = new GUIStyle("iconButton")
            {
                padding = new RectOffset(),
                margin = new RectOffset()
            };

            NewToggleStyle = Utility.CreateStyleFromTextures("ShurikenDropdown", onTexture, offTexture);

            NewToggleStyle.fontSize = 8;
            NewToggleStyle.clipping = TextClipping.Overflow;
            NewToggleStyle.alignment = TextAnchor.MiddleLeft;
            NewToggleStyle.imagePosition = ImagePosition.TextOnly;
            NewToggleStyle.border = new RectOffset(0, 1, 0, 1);
            NewToggleStyle.contentOffset = new Vector2(1f, 0f);
            NewToggleStyle.padding = new RectOffset(0, 0, 0, 0);
            NewToggleStyle.overflow = new RectOffset(-1, 1, -3, 0);
            NewToggleStyle.fixedHeight = 0f;
            NewToggleStyle.fixedWidth = 0f;
            NewToggleStyle.stretchHeight = true;
            NewToggleStyle.stretchWidth = true;

            NewToggleStyle.normal.textColor =
                NewToggleStyle.hover.textColor =
                    NewToggleStyle.focused.textColor =
                        NewToggleStyle.active.textColor =
                            NewToggleStyle.onNormal.textColor =
                                NewToggleStyle.onHover.textColor =
                                    NewToggleStyle.onFocused.textColor =
                                        NewToggleStyle.onActive.textColor =
                                            new Color32(230, 230, 230, 255);

            PrefabApplyContent = new GUIContent("A");
            StaticContent = new GUIContent();
            LockContent = new GUIContent();
            ActiveContent = new GUIContent();
            RendererContent = new GUIContent();
            TagContent = new GUIContent();
            LayerContent = new GUIContent();

            ReloadTooltips();
        }

        public static void ReloadTooltips()
        {
            if (Preferences.tooltips && !Preferences.relevantTooltipsOnly)
            {
                PrefabApplyContent.tooltip = "Apply Prefab Changes";
                StaticContent.tooltip = "Static";
                LockContent.tooltip = "Lock/Unlock";
                ActiveContent.tooltip = "Enable/Disable";
                RendererContent.tooltip = "Enable/Disable renderer";
                TagContent.tooltip = "Tag";
                LayerContent.tooltip = "Layer";
            }
            else
            {
                PrefabApplyContent.tooltip = string.Empty;
                StaticContent.tooltip = string.Empty;
                LockContent.tooltip = string.Empty;
                ActiveContent.tooltip = string.Empty;
                RendererContent.tooltip = string.Empty;
                TagContent.tooltip = string.Empty;
                LayerContent.tooltip = string.Empty;
            }
        }

        #region Textures Sources

        private const string TREE_LINE =
            "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAKElEQVQ4EWP8//8/AxEApIgRmzombIKkiI0awMAwGgajYQDKMwOfDgBL0gQdcnVX0wAAAABJRU5ErkJggg==";

        private const string TREE_TEE =
            "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAMklEQVQ4EWP8//8/AxEApIgRmzombIKkiI0awMAw8GHAQkKUYU0wpBgwmpBwhPbApwMAL00FIa1Ycy8AAAAASUVORK5CYII=";

        private const string TREE_ELBOW =
            "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAANUlEQVQ4EWP8//8/AxEApIgRmzombIKkiI0awMAwDMKAhYQ4x5riiDUAayoEWT4MAnHgvQAA7T4FIQ1dYzoAAAAASUVORK5CYII=";

        private const string WARNING =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACh0lEQVRYCWO0sLBgoADIAfU+okA/AxMlmoF6n1Con2IH/BtoB1BqP8UhoA90AQ8lrqAkDXAALZ4AxHkD5QAXE1U2BwsN9mKgA4zJdQQLmRq5WZgZShLdeEHahc7c/tn75y+DAzlmkRsF+d5mXPYyoiwMIAxiAy2nmwOMBLiZ8sLtueEeBrGFeJlagAIkJ0hyQqA32olbnJsDoRXEjnTgtgY6gOQEiTAF7h+8DAclCRYHF0NODEUgMRUplkKghBGGJB4BUhwACvP6FE9eBiYmRgwjQWJJ7rwiQIleDEk8AqQ4IN9Gm91BW54Np3EgOZAaoAIQJgoQ6wBjdlaG3ARXcLaDG+zf8JIBhJEBSA0nG2MTUIyoBEmsA3oCrbglRAWYke3Cygap8bfksgVKEpUgiXGAgwg/k0OQNSLbYbUZSRCkVkyAKR8oZIgkjJVJjAPqQcHKzoaZ8LCaCBQEqY1z4RUDMvtwqYGJEyqKHbTkWB2ACQumHoXe2CCOwkfmgPRsO8XqcO3Rbzug+CFkOWQ2vhDgZmRkaABlO0Ygg1QA0gPRy9AE1Isz/vA5IB9YuNgrS7LitBtbLkBWDNILMgMoBkoPWAEuBxhzczDmxjoTlZOwGgwTBJkBMgvIx1plM8vIyMDUItPLYpx4dAyUscc9TGGkAw+wDsDvSA5ggmRmYuC5cPeXGlDfQpheGI0tBBykRZgdfMy5YGoopkFmgcwEGgRKkCgA3QHg8h7U0GBhJj3hoZiMxAGZBW28NAKFURIkI7BjArLpP1S9E5AOBmJQkQfCoGY3SA6EQepAcQLKuoSyL1AJTrASKLMBKssEMghmOUhsHxSD2PQA/9CjgB6Wotgx6oDREAAA8+NWHMvSwd0AAAAASUVORK5CYII=";

        private const string INFO =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACBUlEQVRYCe1Xu0oDQRSdNQoKooKaNLGw9A+yX2CnP2BjLbYmv5BetEl+w85CEBZsRRQbS0kQtBB8ReM5684yu3PRzSzDNl44mcmde+8589jNJGi1WqpKm6qSnNz/AqpYgcDc9mnzC/ocnAVCYA0wjbFfCbSfEyBG2pG0+dhX+CPgHiDHOxBbXgCdy8BBGIab7XZbNRqNOND1YzAYqG63q6IoOmRd4MOsJW3BDAJqnU6nNDmJOAHWMmxs9MWngMus6vW6GVeqb9Ti8v8pQFqVUgKM5Aw5/RKZFWQUyHRxThQxgWWeAOaVEjABsQ61JicJsFTqbB+tJMBS6YNY15TeA4VXAM+2ruPcSivgXMwlURJQeAscngJLoyTACvLpkM5AYVG+zkD8KvY0a+uAS7OVfKX0DIdDnW+dL2kLrCCd7dLqn2Pk8s5g1S4koNfrqX6/f4cC+8AGwEvLpHaGBN4FMiIkAZl9SsgfkbgNXAIngKtxe1k/FSHtdzqIWY+BWyTsAFfAIjAPxJcWtLpgDX1CqkdCji0A1iWj1mw24U+NwUvAVhAE65j9Bfq7wDnAMd7teJ/7BPi0UCyh74qpePi06fE3OJ6BTEyQ+2dEklXgCFgB9oAbgIReLH8GqO4BOAaegGvA53tB5QWALyY8RTvnm5xk0qGhn/by0/j9/E2AX+akeuUCvgEs3W2OBzZAiQAAAABJRU5ErkJggg==";

        private const string ERROR =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACPklEQVRYCe1XQWsTQRSezXYnSdmBtD3UQ0SCB0HBSPWQi6DngkIvXpSKYtCDN/EniJceSk4K/gLFg9CbFy/+AAWlFAQJQkqyEghIMZON76lD3kxmZhtCyWUfPPbt975537fLZoYEjUaDLTIKixRH7dxA/gaWZvwIa8B/ClmELDvW/gJ8B/KLo6/BPgMhMEeEffkS56/uxHGdBwGBJ+Xv8Zi9HAzY/nC4AugtyOGka6/CarVq7zA2Jo2t81H0+q4QNZc4ckMwBiZZW8rTvTQ9AugjmWEtfQbUgiaI794XYtUnrshoos45/yFlvZum3wD/qnq2a5aB6+ei6M0DIcrHEVcCaOIi53FHyiuHaZoA/ln1zKvvZ/gcnvxtUwjmEn+cJAzTFrhmW4izF6IIP8gbNg5iPgNn4LVXXOKugRTHtfeEWAfsJsVp7TPgfHI6IKv+/wDLhKdpajeEdJKl9htehAHt4UwDmjuNeUI3vp0wU7K1tpbJySKYbyCLr/UPRyOGOU94DeDe7ooOCD/r9/8m1q7wzcA1pgGquIcHS9YAlzDi6nCCUhKe5tbcivEEVCY+JWnKvkt5DQ8Y3F5pxIUC2ygW2dVSiZ0KcZkeShxORjyWH0H+1Bn/7kwDKF6BxJMM40OWCTRiBhF/B70tyLbJUfemAcSVuOJ4TSiSuhLxF4A9hOyrnu1qM2DjHcsEEX8PQ55A9mzDKBbM+MfkNizepAMsdRewFuSBpTcFzWpgasC8wPQXNO/EGdfnBvI38Af9967LZ4JMowAAAABJRU5ErkJggg==";

        private const string MONO_BEHAVIOUR_ICON =
            "iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QUWDigZn/bO1AAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAACKUlEQVQ4y6WTTUhUURTHf+++cUaHmXGczKSNIiEzYWkGBfaxSBL62IaFBbWpIAmCFm2CFq2Cgna2EmqRUm0qhDQjEaLCIsKQjBGRwrRxmnF8Os68d26LmbFBJQgvHA4c7u/8z/9yD2zgGPncDISB0D/uTgBx4G2h4MrncCye6DZN5cZQGICRb5uylkikLEbev+NsR3s74AdeA1mVh0MZ23YvZ7NkMxmyWTsXtkOpx02pp4SGpmYe9DzuBWwgAlCAEdG5QKO18GnGYngqQfR3mqDfR9DnJbKjkfs9T14BB4vHxhENwFhskcsvxhmLWQA0bvEzcGY3wYAPgLr6MKs9IyI42uDis1ECnhL6O3axs8qHYSjQGgyDYMCHaL0Cq7/KwocfCaKxFHfa6tle6UXQXO3/ipXOcHMoirW0TKjcv1bZcYQvP5Ns9ZrUBssQ0dwanuDlt2km5+b5OJ1kaGKGgXMt6yvPzi+yyZ23IJorLbVUeaD3ZDMVLuHp6T05C6thEcFtaKylNI4IC+kMR+8NEp2e40jXILPxJLcHR4vZImVHqKsoYyqWIr6QxuNS9J1vpXXbZrpO7OVYuJprhxvQWtYfu6Wmkmqfi0uP3vD5+xyTsSSd+8OUukw6D0RW/sEauCpUjjKg+9Q+aiq83Ogb4dDd51x4OETA46KuMoCIxrZlzWtbhQbEk1xva8I0FUopTKVwJAeYyixwv4q3qhY4/h/b2A+Mb2Sd+QM5ver/WJ19nQAAAABJRU5ErkJggg==";

        private const string LAYERS =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABrklEQVRYCe2VvUrEQBSFzaqwglgoVjba2/iDta2tnaUI4gPYrxY+gP3C+hKKYG23Ymul2FkIihYKIvE7MgMh683OTMBtcuBjJnPvuXc2m0myPM/HRqnWKJurd7OB5g7UuQNtnqGuQ/M06RgmsISnD16aay26VrSBJlvwDGVpTbGomjHJLYp34BssKdYB5QbVDkqi2BxcQKiUK8/Q+kMTKLIK9xAreeSt7FEZxLwHH5AqeVXD7FPnGIYeu+qvXdXuXGyN8QFipb9gBcxfr1hlsGCOfQjP8c4W/GYfM1AwT7u5jtYRxBxD7zX7mAHXdJvxBY7Bn+2QF9E4+SfwCqph9jEDznTI+AXSJfizrdfujRad+oyLoHrzcAWSvKph9jEDBdMm8yeQHmEd5GtD16G51jZAOZI88mrdxAyUTAtcX4P0CftQ9h64GMNvrjzlnIHrgYUK0ySxU/DqMZlynPlFRuUoN6h2UFKp2A7X7yDdOjTXmmJRNaOSC8WXmd+Bl+Zai66XyZSoGXw9591lfEupU2cD6pe5psm/YiJl1wVPcmNf4z++hr7Xn2OzgeYO/ADhoJ0LKtfgoAAAAABJRU5ErkJggg==";

        private const string TAG =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAA9UlEQVRYCe2UMQ7CMAxFKYxcgqUSA1dgQLDQgQNyE8TEPRiiwgwTCxJq/++QhdRJBhMGf+lJbe3Ev1biquu6SUlNSxZnbTNgHbAO/H0HGlzVFnBapeCQV4N0cRIK3BDL1QML1kDa18cqJgoSg8K6N2IHcBJyhpCWAW5O8xtw4cuYNA2w5gcswZUvIWnfghmKnsE8VJzftA2wxgIc+RDSLwyE6vpv2meAhRxYgRf4kraBooeQ13ALRm8A26F1BjiI9kCcASkG7kzK1BP5OxCdgsO+kZndIN6CVDkk1sDP+thz7BBm/nx+utYZSHZiBqwD1oHiHegB4UkSNS6TXloAAAAASUVORK5CYII=";

        private const string STATIC_ON =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABxElEQVRYCe1WO07DQBCNkZBM7sMF0gRuEUruQMEREAUXoKQLBd+e04QWCiTznr2zmt3Mjm0UksYjjXZ+O+/t+Fs1TTM7pBwdEpzYE4FpAv81gSXurw30FlpBy8LHcMe6RL8vqMgdjApq4ngTWID2Gjov0zczp4jWKnMJ+0b5qVlihvgjlPIOnUPNE6j4GexV8K+xavmEY+43g6GYoG+hyytWjwTBOfYf6AWUfYXEN2zmTSwzqIo1iRfET1RO9go4Uq1oEleInEOldmvdChjFJEFwSk4iB++qukms4PT27y0ITXhyIfEMuw7xe6yW8HIUx45cxI2GDhZskiA45QlKEsfQB6iWweDYNBtDgLUE/QhoFolR4H8hwLESREST4OUYNHbUxYNHQwcLdg6OslaExJhesTYaaOXZJfDAId4TXg8zZwYzMh44L4fcE7xBrfeEi+EmAxF9zRGKwjjJ1VB5OvL3RG//3oIIlxoCLvv1e4IkvNe27GnXxMFGy0+hu6eAJ89rNQl+QwaRyJtYviaQnzyvJyg/XJRBJPIGu/BJgp9wCj/pbk832bfZyZPEGrpwalrs9lcp/UXZr+f9ku2FyURgmsAv12M/P0VUNfwAAAAASUVORK5CYII=";

        private const string STATIC_OFF =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABRklEQVRYCe1WMQ7CMAykCAkexB8oGzwBZn7AwgcQAw9h5kGMXWknwrnUUZo2SU2DKqFasurYTu7ixG0TpdRkSJkOCU7YI4GxAn9ZgRSXO4NeoAnUL/QeiKhrrFVAWa4wEqgTI/YRLLHdubHlA+yzMW6aPnaCGO18V+Wf8DQlw8BZAWfAN8mKcdlf8O+rGJPIMV5Z+TXM2sCX6IgxOMKlmCSO8HjBEZ/0IWCDfygoRST4OILrf3sJqdVuUPPC8QUrYDx4EHxSGYSaIt9sNQy1PGEFy44cjakN0+mxo4ITjoRAdHAJAQKnlmoTcdmxiN64NkynZfvAg31urdXAazisCT8FJ6zyQ+FplRyxRUucWm0DvbfERK4QgbZf5mjgxFT6IooKLiVA4Fto77ITMMuMDccz/EfjmNjVLT2Crut2zhsJjBUYvAJvIjx8JkSpDJcAAAAASUVORK5CYII=";

        private const string ACTIVE_ON =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAABSklEQVRYCe2VsUoDQRCGPVELRbDwDdJr4wskr5XmmiBptU3jK/gEYmWr72CqgHVAOP8vzELYmz02h7ApduDn9v75Z2Zvbm+u6brupKSdlixO7bqB2oHagbMRnyEx98KdcGvxG12/hE/h17i8C4MoExPpnoUfIWX40KDNypsjOleyhbAVcg0tMcQO1hh0KvhGeBNiexUxFa4MrOFiI5YcyTpJh4IuhQ8htrmIVBy+2MhBLjfGJU28ijPpnqck5kJYCt8G1nD4vE6Qy63lkhLPBM9oNTGPjhMOHxrPyNmr1yNM9O5lEHdt/rXjhyMfGs/I2at3tJOwTUyRB+NfHH/ggiaWtDGxu/faYlzRQ8i7Kv4ZsomigyicWMYpY7XIKA6b4Jr7M3oy7X5sct1I7B7OAfJff8djNjCwt8NdRzuIDn+UkRG1A7UDxTvwB4zvJLleQ2PrAAAAAElFTkSuQmCC";

        private const string ACTIVE_OFF =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACEElEQVRYCe2Wvy5EURDGLetPVgQJT6CUEA9Agqh5Go1GsxEFjUoioVLQiJJGaHRCJJ5AQYTOnwTr9yXnbM6eO+dm7WZDsZN8Ofd8M2dmdu6cuVuoVCodfymdfxlcsdsJtCvwryuwQJMegZK6tVVSyJkDCr4ITsESeAUSVW0KjINRt39ivQHX4BPUL0oggRL8CZBo1V622yAlLyi2wBhI+a3hazbGISuJbuwOQZ58oCwD2ebGyFVyeBicAy++EkWIadDvMMt6DGI5gxgCyThJBYf06y9BLD4J6+xKbMxePvzry5zJEBh7bs9w9uY4n0QX+02wDnqAzlqV2HE677u6Vh8igwX2lixBKrgkTGKf/RqQP70OS+YgM/EyhDO6sDzADQKV893pwyQ24ORvwOniRb2UiZeahKk/Cd9ccM2DZ3fR/bDqZb/suNRSMBVWVnAqlyUqr36Fyh2Kr4R0M6EieJ7nOVOBDBEYqXFiUYPpjBpOjXcPrNdxAB/KLhszlkk649Q11FULz8nOakyfRMPXUEE0RM5ALKqEXocGkRpzEVhXdBW+4UHkf6XGaRlovNYrYU94P+ZqkkSxeH1g9KHRByclDyhunbKuJAoYm7cjhyyimwQTYAR8gUdwB65AH9CnXFc0/pRDRaIEWoCwMY/y/LciuPepJBRcY91zmbWRVxDVsLltahQ35/UXp9sJtCvwA5nkXRRXmT2oAAAAAElFTkSuQmCC";

        private const string LOCK_ON =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgBAMAAACBVGfHAAAAJFBMVEUAAAD///////////////////////////////////////////+0CY3pAAAAC3RSTlMAG0vz4+K+tXf5hPH/P1UAAABYSURBVCjPY6AhYEo2U0ARWL179y4UBTsbJWYjK2HbwcDQnYAkwLyRgUHaAEmAewMI4xFQ3Q0GQXABb4jAFrjAbiggTaBQHE1AgBGPAEIL+dYinI7pOboBALjTVXjhsKb8AAAAAElFTkSuQmCC";

        private const string LOCK_OFF =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgBAMAAACBVGfHAAAAJFBMVEUAAAD///////////////////////////////////////////+0CY3pAAAAC3RSTlMAG0vz4+K+tXf5hPH/P1UAAABXSURBVCjPY6AhYEo2U0ARWL179y4UBTsbJWYjK2HbwcDQnYAkwLyRgUHaAM1c7g14BFR3g0EQXMAbIrAFLrAbCkgTKBRHExBgxCOA0EK+tQinY3qObgAArOBUAsnPrEAAAAAASUVORK5CYII=";

        private const string CHILD_TOGGLE_ON =
            "iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QULAAkpnV1J0wAAAHpJREFUOMvt07EJhFAMgOGXw0psXcrKFZzGeRzDHax0CMvvmisOEXk+LP0hRSD5SQhJ6LDJZ0OXjmB1n/XoCUgFRET855/0EK/oFT0lWgr6lrNfqzHd+LMJ9akeDcYMyYjmclZU6C8kPars5dFixv6LGW3xFTBgyK3/AgxhFqUwd4v5AAAAAElFTkSuQmCC";

        private const string CHILD_TOGGLE_OFF =
            "iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QULAAkyFziAPwAAAG5JREFUOMvt06sNgEAQhOEdgiJoWqEHFLRBI9RDQhW0gAN5DeB+FOYMj1uB4CtgMpnsGtAAK/ctQGExYOO5ESjjoLcGIPcIAmjPHAFYmkpSyCzd5NVoN7Peo5HbRrOk+jNjd5KC+0G6vYjP0/4uHSU6sp7/OaNtAAAAAElFTkSuQmCC";

        private const string RENDERER_ON =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgBAMAAACBVGfHAAAAFVBMVEUAAAD///////////////////////9Iz20EAAAABnRSTlMA5ImHSgYkLJCAAAAARUlEQVQoz2MYOMAilpbAwMCWlugAFWBMgwikCUAF2BJgNDkCcMCqKBSAYjNzWpoBhIWwWQCfAEILpqGUuxTD+5gBNGAAAE5iEyGKEpDSAAAAAElFTkSuQmCC";

        private const string RENDERER_OFF =
            "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgBAMAAACBVGfHAAAAHlBMVEUAAAD///////////////////////////////////8kfJuVAAAACXRSTlMA8KodHOPitUoUm3BmAAAAT0lEQVQoz2MYOMCROXMCAwPnzGkNUAHWmRCBmQFQAc4JMJoUAWZFIQMQDQeOM2eKoNisWWw+CUVA0oFlIh4BhBYMQzGsJd/pCO9jBtCAAQBGTx9Z+1SkqgAAAABJRU5ErkJggg==";

        private const string FADE =
            "iVBORw0KGgoAAAANSUhEUgAAALQAAAASCAYAAADyiPTBAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gIGFTIzgL5IaQAAAjVJREFUaN7tWtGuwyAI5WD3cP//f8deusQQFbC2s7eSLFqmVPFAT60QkT+qC4xrrUPnf622CLTXOhTqnOnSXvJeT6qeiOi167b991L1VCi37Hor2MBectYW2X/fcSIbk55bPvZ8jrlO61HRU6MfVfrmfXJbUulPDdslu7mt0hj0WhIRgaktQjGRgbZGixg6ya4l0K93HBIcpzh0tbl42/Ws7fuEden1sbAx4dlASQMD7Kij36ruCQAxAPFr/77vvqbsiHh5AMhlcMaQQJ+7+e/qxBISPvnmd1ksGO8JFvilI0CWXAToJT7wl4JAGoGxgH0RoOUfOhtOXWR3BwsuK0PPDPjStpUHyJ7tKAQCbclA2R7mZE+mRaGs7eO2MjoF7vUUmrYy9I+ztZWxUQmAHnAvGRAcfHI0YRKwRuiG9UXN0ln2rvINbg7QnvFjG2AYN3GaZ36ez71WnysWbskgDo3Jo9qz0+GhGPqMQt6Gs6fb95orTwJ91oALgZIcXHzJIA6NgUDDBECugZsrYAfVDwBFqEzkZTRKnSJPHTJ2bHiyZIZRgAb5TsfhYmCSc3GslzgP+DTQWWVjna2pEhweIM2cia2xJQdOIskOR/CynUnQO9uhwxktJ1qgrYEXHf1LiYIrc7k7p8YkY5Aoh8YB3REw9zoVjTJKKaC4cquv5sdcaMdknwUmxcmfzqND899OisDIQf8zdjIiYNagSxUwc/Z4TYqCeDJ6Lx+mxssmHbAzW/YdQl8/eE1MX9J1OAoAAAAASUVORK5CYII=";

        #endregion

        public static readonly GUIContent PrefabApplyContent;
        public static readonly GUIContent StaticContent;
        public static readonly GUIContent LockContent;
        public static readonly GUIContent ActiveContent;
        public static readonly GUIContent RendererContent;
        public static readonly GUIContent TagContent;
        public static readonly GUIContent LayerContent;

        public static readonly Color NormalColor;
        public static readonly Color BackgroundColorEnabled;
        public static readonly Color BackgroundColorDisabled;
        public static readonly Color SelectedFocusedColor;
        public static readonly Color SelectedUnfocusedColor;
        public static readonly Color ChildToggleColor;

        public static readonly GUIStyle IconButton;
        public static readonly GUIStyle NewToggleStyle;
        public static readonly GUIStyle StaticToggleStyle;
        public static readonly GUIStyle ApplyPrefabStyle;
        public static readonly GUIStyle LockToggleStyle;
        public static readonly GUIStyle ActiveToggleStyle;
        public static readonly GUIStyle RendererToggleStyle;
        public static readonly GUIStyle MiniLabelStyle;
        public static readonly GUIStyle TagStyle;
        public static readonly GUIStyle LayerStyle;

        public static readonly GUIStyle LabelNormal;
        public static readonly GUIStyle LabelDisabled;
        public static readonly GUIStyle LabelPrefab;
        public static readonly GUIStyle LabelPrefabDisabled;
        public static readonly GUIStyle LabelPrefabBroken;
        public static readonly GUIStyle LabelPrefabBrokenDisabled;

        public static readonly Texture2D TreeLineTexture;
        public static readonly Texture2D TreeTeeTexture;
        public static readonly Texture2D TreeElbowTexture;

        public static readonly Texture2D InfoIcon;
        public static readonly Texture2D WarningIcon;
        public static readonly Texture2D ErrorIcon;
        public static readonly Texture2D MonoBehaviourIconTexture;
        public static readonly Texture2D FadeTexture;
    }
}