using System;
using System.Linq;
using System.Text;
using GameAssets.Scripts.Editor.Hierarchy.Icon;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy
{
    /// <summary>
    /// Misc utilities for Enhanced Hierarchy.
    /// </summary>
    public static class Utility
    {
        private const string CTRL = "Ctrl";
        private const string CMD = "Cmd";
        private const string MENU_ITEM_PATH = "Edit/Enhanced Hierarchy %h";

        private static int _errorCount;
        private static readonly GUIContent TempContent = new GUIContent();

        public static string CtrlKey =>
            Application.platform == RuntimePlatform.OSXEditor ? CMD : CTRL;

        [MenuItem(MENU_ITEM_PATH, false, int.MinValue)]
        private static void EnableDisableHierarchy()
        {
            Preferences.enabled.Value = !Preferences.enabled;
            EditorApplication.RepaintHierarchyWindow();
        }

        [MenuItem(MENU_ITEM_PATH, true)]
        private static bool CheckHierarchyEnabled()
        {
            Menu.SetChecked(MENU_ITEM_PATH, Preferences.enabled);
            return true;
        }

        public static void EnableFPSCounter()
        {
            double fps;
            var frames = 0;
            var lastTime = 0d;
            var content = new GUIContent();
            var evt = EventType.Repaint;

            EditorApplication.hierarchyWindowItemOnGUI += (id, rect) =>
            {
                using (ProfilerSample.Get("Enhanced Hierarchy"))
                using (ProfilerSample.Get("FPS Counter"))
                {
                    if (evt == UnityEngine.Event.current.type)
                    {
                        return;
                    }

                    evt = UnityEngine.Event.current.type;

                    if (evt == EventType.Repaint)
                    {
                        frames++;
                    }

                    if (EditorApplication.timeSinceStartup - lastTime < 0.5d)
                    {
                        return;
                    }

                    fps = frames / (EditorApplication.timeSinceStartup - lastTime);
                    lastTime = EditorApplication.timeSinceStartup;
                    frames = 0;

                    content.text = $"{fps:00.0} FPS";
                    content.image = Styles.WarningIcon;

                    SetHierarchyTitle(content);
                }
            };
        }

        public static bool ShouldCalculateTooltipAt(Rect area)
        {
            return area.Contains(UnityEngine.Event.current.mousePosition);
        }

        public static void ForceUpdateHierarchyEveryFrame()
        {
        }

        public static void LogException(Exception e)
        {
            Debug.LogError("Unexpected exception in Enhanced Hierarchy");
            Debug.LogException(e);

            if (_errorCount++ < 10)
            {
                return;
            }

            Debug.LogWarning("Automatically disabling Enhanced Hierarchy, if the error persists contact the developer");

            Preferences.enabled.Value = false;
            _errorCount = 0;

            if (EditorPrefs.GetBool("EHEmailAskDisabled", false))
            {
                return;
            }

            switch (EditorUtility.DisplayDialogComplex("Mail Developer",
                "Enhanced Hierarchy has found an exception, would you like to report a bug to the developer? (If you choose yes your mail app will open with a few techinical information)",
                "Yes", "Not now", "No and don't ask again"))
            {
                case 0:
                    Preferences.OpenSupportEmail(e);
                    EditorUtility.DisplayDialog("Mail Developer",
                        $"Your mail app will open now, if it doesn't please send an email " +
                        $"reporting the bug to {Preferences.DEVELOPER_EMAIL}", "OK");
                    break;

                case 1:
                    break;

                case 2:
                    EditorPrefs.SetBool("EHEmailAskDisabled", true);
                    EditorUtility.DisplayDialog("Mail Developer", "You won't be bothered again, sorry", "OK");
                    break;
            }
        }

        public static void SetHierarchyTitle(string title)
        {
            try
            {
                Reflected.HierarchyWindowInstance.titleContent.text = title;
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Failed to set hierarchy title: {e}");
            }
        }

        public static void SetHierarchyTitle(GUIContent content)
        {
            try
            {
                Reflected.HierarchyWindowInstance.titleContent = content;
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Failed to set hierarchy title: {e}");
            }
        }

        public static GUIStyle CreateStyleFromTextures(Texture2D on, Texture2D off)
        {
            return CreateStyleFromTextures(null, on, off);
        }

        public static GUIStyle CreateStyleFromTextures(GUIStyle reference, Texture2D on,
            Texture2D off)
        {
            using (ProfilerSample.Get())
            {
                var style = reference != null ? new GUIStyle(reference) : new GUIStyle();

                style.active.background = off;
                style.focused.background = off;
                style.hover.background = off;
                style.normal.background = off;
                style.onActive.background = on;
                style.onFocused.background = on;
                style.onHover.background = on;
                style.onNormal.background = on;
                style.imagePosition = ImagePosition.ImageOnly;

                EditorApplication.update += () =>
                {
                    style.fixedHeight = Preferences.iconsSize;
                    style.fixedWidth = Preferences.iconsSize;
                };

                return style;
            }
        }

        public static Texture2D GetBackground(GUIStyle style, bool on)
        {
            return on ? style.onNormal.background : style.normal.background;
        }

        public static Texture2D FindOrLoad(string base64)
        {
            var name = $"Enhanced_Hierarchy_{(long) base64.GetHashCode() - int.MinValue}";

            return FindTextureFromName(name) ?? LoadTexture(base64, name);
        }

        public static Texture2D LoadTexture(string base64, string name)
        {
            using (ProfilerSample.Get())
            {
                try
                {
                    var bytes = Convert.FromBase64String(base64);

                    var texture = new Texture2D(0, 0, TextureFormat.ARGB32, false, false)
                    {
                        name = name,
                        hideFlags = HideFlags.HideAndDontSave
                    };

                    texture.LoadImage(bytes);
                    return texture;
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("Failed to load texture \"{0}\": {1}", name, e);
                    return null;
                }
            }
        }

        public static Texture2D FindTextureFromName(string name)
        {
            using (ProfilerSample.Get())
            {
                try
                {
                    var textures = Resources.FindObjectsOfTypeAll<Texture2D>();
                    return textures.FirstOrDefault(x => x.name == name);
                }
                catch (Exception e)
                {
                    Debug.LogErrorFormat("Failed to find texture \"{0}\": {1}", name, e);
                    return null;
                }
            }
        }

        public static Color GetHierarchyColor(Transform t)
        {
            return !t ? Color.clear : GetHierarchyColor(t.gameObject);
        }

        public static Color GetHierarchyColor(GameObject go)
        {
            return !go ? Color.black : GetHierarchyLabelStyle(go).normal.textColor;
        }

        public static GUIStyle GetHierarchyLabelStyle(GameObject go)
        {
            using (ProfilerSample.Get())
            {
                if (!go)
                {
                    return EditorStyles.label;
                }

                var active = go.activeInHierarchy;
                var prefabType = PrefabUtility.GetPrefabInstanceStatus(go);

                switch (prefabType)
                {
                    case PrefabInstanceStatus.Connected:
                        return active ? Styles.LabelPrefab : Styles.LabelPrefabDisabled;

                    case PrefabInstanceStatus.MissingAsset:
                        return active ? Styles.LabelPrefabBroken : Styles.LabelPrefabBrokenDisabled;

                    default:
                        return active ? Styles.LabelNormal : Styles.LabelDisabled;
                }
            }
        }

        public static Color OverlayColors(Color src, Color dst)
        {
            using (ProfilerSample.Get())
            {
                var alpha = dst.a + src.a * (1f - dst.a);
                var result = (dst * dst.a + src * src.a * (1f - dst.a)) / alpha;

                result.a = alpha;
                return result;
            }
        }

        public static bool TransformIsLastChild(Transform t)
        {
            using (ProfilerSample.Get())
            {
                return !t || t.GetSiblingIndex() == t.parent.childCount - 1;
            }
        }

        public static void ApplyHideFlagsToPrefab(UnityEngine.Object obj)
        {
            var handle = PrefabUtility.GetPrefabInstanceHandle(obj);

            if (handle)
            {
                handle.hideFlags = obj.hideFlags;
            }
        }

        public static void LockObject(GameObject go)
        {
            using (ProfilerSample.Get())
            {
                go.hideFlags |= HideFlags.NotEditable;

                ApplyHideFlagsToPrefab(go);

                if (!Preferences.allowPickingLockedObjects)
                {
                    SceneVisibilityManager.instance.DisablePicking(go, false);
                }

                EditorUtility.SetDirty(go);
            }
        }

        public static void UnlockObject(GameObject go)
        {
            using (ProfilerSample.Get())
            {
                go.hideFlags &= ~HideFlags.NotEditable;
                ApplyHideFlagsToPrefab(go);

                if (!Preferences.allowPickingLockedObjects)
                {
                    SceneVisibilityManager.instance.EnablePicking(go, false);
                }

                EditorUtility.SetDirty(go);
            }
        }

        public static void ApplyPrefabModifications(GameObject go, bool allowCreatingNew)
        {
            var isPrefab = PrefabUtility.IsPartOfAnyPrefab(go);

            if (isPrefab)
            {
                var prefab = PrefabUtility.GetNearestPrefabInstanceRoot(go);

                if (!prefab)
                {
                    Debug.LogError("Prefab asset not valid!");
                    return;
                }

                if (PrefabUtility.GetPrefabInstanceStatus(prefab) == PrefabInstanceStatus.Connected)
                {
                    PrefabUtility.ApplyPrefabInstance(prefab, InteractionMode.UserAction);
                }
                else if (EditorUtility.DisplayDialog("Apply disconnected prefab",
                    "This is a disconnected game object, do you want to try to reconnect to the last prefab asset?",
                    "Try to Reconnect", "Cancel"))
                {
                    PrefabUtility.RevertPrefabInstance(prefab, InteractionMode.UserAction);
                }

                EditorUtility.SetDirty(prefab);
            }
            else if (allowCreatingNew)
            {
                var path = EditorUtility.SaveFilePanelInProject("Save prefab", "New Prefab",
                    "prefab", "Save the selected prefab");

                if (!string.IsNullOrEmpty(path))
                {
                    PrefabUtility.SaveAsPrefabAssetAndConnect(go, path, InteractionMode.UserAction);
                }
            }
        }

        public static string EnumFlagsToString(Enum value)
        {
            using (ProfilerSample.Get())
            {
                try
                {
                    if ((int) (object) value == -1)
                    {
                        return "Everything";
                    }

                    var str = new StringBuilder();
                    const string SEPARATOR = ", ";

                    foreach (var x in Enum.GetValues(value.GetType()))
                    {
                        var i = (int) x;

                        if (i == 0 || (i & (i - 1)) != 0 || !Enum.IsDefined(value.GetType(), i) ||
                            (Convert.ToInt32(value) & i) == 0)
                        {
                            continue;
                        }

                        str.Append(ObjectNames.NicifyVariableName(x.ToString()));
                        str.Append(SEPARATOR);
                    }

                    if (str.Length > 0)
                    {
                        str.Length -= SEPARATOR.Length;
                    }

                    return str.ToString();
                }
                catch (Exception e)
                {
                    if (Preferences.DebugEnabled)
                    {
                        Debug.LogException(e);
                    }

                    return string.Empty;
                }
            }
        }

        public static GUIContent GetTempGUIContent(string text, string tooltip = null,
            Texture2D image = null)
        {
            TempContent.text = text;
            TempContent.tooltip = tooltip;
            TempContent.image = image;
            return TempContent;
        }

        public static string SafeGetName(this IconBase icon)
        {
            try
            {
                return icon.Name;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Preferences.ForceDisableButton(icon);
                return string.Empty;
            }
        }

        public static float SafeGetWidth(this IconBase icon)
        {
            try
            {
                return icon.Width + (Preferences.iconsSize - 15) / 2f;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Preferences.ForceDisableButton(icon);
                return 0f;
            }
        }

        public static void SafeInit(this IconBase icon)
        {
            try
            {
                icon.Init();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Preferences.ForceDisableButton(icon);
            }
        }

        public static void SafeDoGUI(this IconBase icon, Rect rect)
        {
            try
            {
                rect.yMin -= (Preferences.iconsSize - 15) / 2f;
                rect.xMin -= (Preferences.iconsSize - 15) / 2f;

                icon.DoGUI(rect);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Preferences.ForceDisableButton(icon);
            }
        }

        public static Rect FlipRectHorizontally(Rect rect)
        {
            return Rect.MinMaxRect(rect.xMax, rect.yMin, rect.xMin, rect.yMax);
        }
    }
}