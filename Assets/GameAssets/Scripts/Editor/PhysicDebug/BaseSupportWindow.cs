﻿using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.PhysicDebug
{
    public class BaseSupportWindow : EditorWindow
    {
        private int _toolBarIndex;

        private GUIContent _assetName;
        private GUIContent _support;
        private GUIContent _contact;
        private GUIContent _review;

        private GUIStyle _publisherNameStyle;
        private GUIStyle _toolBarStyle;
        private GUIStyle _greyText;
        private GUIStyle _reviewBanner;
        private GUIStyle _versionLabel;

        private static string AssetName => null;

        private static string SupportForum => null;

        private static string StoreLink => null;

        private static string Version => null;

        private static void ShowWindow<T>() where T : BaseSupportWindow
        {
            var window = CreateInstance<T>();

            window.ShowUtility();

            window.titleContent = new GUIContent("About");

            window.LoadStyles();
        }

        private void LoadStyles()
        {
            var color = "#AAAAAA";

            if (!EditorGUIUtility.isProSkin)
            {
                color = "#353535";
            }

            _assetName = IconContent($"<size=20><b><color={color}> {AssetName}</color></b></size>", "", "");

            _support = IconContent("<size=12><b> Support</b></size>\n <size=9>Get help and talk \n with others.</size>",
                "_Help", "");

            _contact = IconContent("<size=12><b> Contact</b></size>\n <size=9>Reach out and \n get help.</size>",
                "console.infoicon", "");

            _review = IconContent("<size=11><color=white> Please consider leaving a review.</color></size>",
                "Favorite Icon", "");

            var _ = new GUIStyle(EditorStyles.label)
            {
                richText = true
            };

            _publisherNameStyle = new GUIStyle()
            {
                alignment = TextAnchor.MiddleLeft,
                richText = true
            };

            _toolBarStyle = new GUIStyle("LargeButtonMid")
            {
                alignment = TextAnchor.MiddleLeft,
                richText = true
            };

            _greyText = new GUIStyle(EditorStyles.centeredGreyMiniLabel)
            {
                alignment = TextAnchor.MiddleLeft
            };

            _reviewBanner = new GUIStyle("TL SelectionButton")
            {
                alignment = TextAnchor.MiddleCenter,
                richText = true
            };

            _versionLabel = new GUIStyle(EditorStyles.centeredGreyMiniLabel)
            {
                alignment = TextAnchor.MiddleRight,
            };
        }

        private void OnGUI()
        {
            maxSize = minSize = new Vector2(350, 350);

            EditorGUILayout.Space();
            GUILayout.Label(_assetName, _publisherNameStyle);
            EditorGUILayout.Space();

            var toolbarOptions = new GUIContent[2];

            toolbarOptions[0] = _support;
            toolbarOptions[1] = _contact;

            _toolBarIndex = GUILayout.Toolbar(_toolBarIndex, toolbarOptions, _toolBarStyle, GUILayout.Height(50));

            EditorGUILayout.Space();

            switch (_toolBarIndex)
            {
                case 0:
                    EditorGUILayout.LabelField("Talk with others.", _greyText);

                    if (GUILayout.Button("Support Forum"))
                    {
                        Application.OpenURL(SupportForum);
                    }

                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("Detailed code documentation.", _greyText);

                    if (GUILayout.Button("Wiki"))
                    {
                        Application.OpenURL("https://www.rotaryheart.com/Wiki.html");
                    }

                    break;

                case 1:
                    EditorGUILayout.LabelField("Get in touch.", _greyText);

                    if (GUILayout.Button("Email"))
                    {
                        Application.OpenURL("mailto:ma.rotaryheart@gmail.com?");
                    }

                    EditorGUILayout.Space();

                    if (GUILayout.Button("Contact Form"))
                    {
                        Application.OpenURL("https://www.rotaryheart.com/Contact.html");
                    }

                    break;
            }

            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField(new GUIContent($"Version {Version}"), _versionLabel);
            EditorGUILayout.Space();

            if (GUILayout.Button(_review, _reviewBanner, GUILayout.Height(30)))
            {
                Application.OpenURL(StoreLink);
            }
        }

        private static GUIContent IconContent(string text, string icon, string tooltip)
        {
            var content = string.IsNullOrEmpty(icon)
                ? new GUIContent()
                : EditorGUIUtility.IconContent(icon);

            content.text = text;
            content.tooltip = tooltip;
            return content;
        }
    }
}