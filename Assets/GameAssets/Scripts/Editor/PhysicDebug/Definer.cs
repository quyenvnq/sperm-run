﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace GameAssets.Scripts.Editor.PhysicDebug
{
    public static class Definer
    {
        public static void ApplyDefines(List<string> defines)
        {
            if (defines == null || defines.Count == 0)
            {
                return;
            }

            var availableDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(
                EditorUserBuildSettings.selectedBuildTargetGroup);

            var definesSplit = new List<string>(availableDefines.Split(';'));

            foreach (var define in defines.Where(define => !definesSplit.Contains(define)))
            {
                definesSplit.Add(define);
            }

            _ApplyDefine(string.Join(";", definesSplit.ToArray()));
        }

        public static void RemoveDefines(List<string> defines)
        {
            if (defines == null || defines.Count == 0)
            {
                return;
            }

            var availableDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(
                EditorUserBuildSettings.selectedBuildTargetGroup);

            var definesSplit = new List<string>(availableDefines.Split(';'));

            foreach (var define in defines)
            {
                definesSplit.Remove(define);
            }

            _ApplyDefine(string.Join(";", definesSplit.ToArray()));
        }

        private static void _ApplyDefine(string define)
        {
            if (!string.IsNullOrEmpty(define))
            {
                PlayerSettings.SetScriptingDefineSymbolsForGroup(
                    EditorUserBuildSettings.selectedBuildTargetGroup, define);
            }
        }
    }
}