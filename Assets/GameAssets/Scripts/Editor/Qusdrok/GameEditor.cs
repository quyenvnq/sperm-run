using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Ex;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.Editor.Qusdrok
{
    public class GameEditor : EditorWindow
    {
        private Vector2 _scrollPos = Vector2.zero;

        [MenuItem("Tools/Game Editor")]
        public static void Setup()
        {
            var window = GetWindow(typeof(GameEditor), true, "Game Editor");

            window.minSize = new Vector2(600f, 500f);

            var width = window.position.width;
            var height = window.position.height;

            var x = (Screen.currentResolution.width - width) / 2;
            var y = (Screen.currentResolution.height - height) / 2;

            window.position = new Rect(x, y, width, height);
        }

        private void OnGUI()
        {
            _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos, false, false,
                GUILayout.ExpandHeight(true), GUILayout.Height(focusedWindow.position.height));

            CreateTitle("CREATE UI");
            CreateUI();

            CreateTitle("CREATE VIEW");
            CreateView();

            CreateTitle("CREATE ANIMATOR");
            CreateAnimator();

            EditorGUILayout.EndScrollView();
        }

        #region Create GUI Title

        private static void CreateTitle(string title)
        {
            EditorGUILayout.Space(15f);

            EditorGUILayout.LabelField("", title,
                new GUIStyle
                {
                    alignment = TextAnchor.MiddleCenter,
                    fontStyle = FontStyle.Bold,
                    fontSize = 25,
                    normal = new GUIStyleState
                    {
                        textColor = Color.white
                    }
                });

            EditorGUILayout.Space(15f);
        }

        #endregion

        #region Create GUI Animator

        [SerializeField] private List<string> animationNames = new List<string>();

        private string _animatorName;

        private void CreateAnimator()
        {
            _animatorName = EditorGUILayout.TextField("Animator name: ", _animatorName);

            var serialized = new SerializedObject(this);
            var properties = serialized.FindProperty("animationNames");

            EditorGUILayout.PropertyField(properties, true);

            serialized.ApplyModifiedProperties();

            SetupAnimator();
        }

        private void SetupAnimator()
        {
            if (!GUILayout.Button("Save"))
            {
                return;
            }

            if (string.IsNullOrEmpty(_animatorName))
            {
                ShowNotification(new GUIContent("Animator name can't be empty!"));
                return;
            }

            animationNames.RemoveAll(string.IsNullOrEmpty);

            if (animationNames.Count == 0)
            {
                ShowNotification(new GUIContent("Amount of nested animations can't be zero!"));
                return;
            }

            var previousPath = PlayerPrefs.HasKey(ToString())
                ? PlayerPrefs.GetString(ToString())
                : Application.dataPath;

            var path = EditorUtility.SaveFilePanel("Save dialog", previousPath,
                _animatorName, "controller");

            if (path.Length == 0)
            {
                return;
            }

            PlayerPrefs.SetString(ToString(), path);

            path = $"Assets{path.Substring(Application.dataPath.Length)}";

            var animator = AnimatorController.CreateAnimatorControllerAtPath(path);

            foreach (var x in animationNames.Select(animation => new AnimationClip {name = animation}))
            {
                AssetDatabase.AddObjectToAsset(x, animator);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(x));
            }

            ShowNotification(new GUIContent("Animator was created!"));
            Close();
        }

        #endregion

        #region Create GUI UI

        private int _screenUIDirectionSelected;

        private string _uiTitle;

        private bool _hasUIBg;

        private void CreateUI()
        {
            _uiTitle = EditorGUILayout.TextField("UI Title:", _uiTitle);

            var screenUIDirectionType = new[] {"Horizontal", "Vertical", "Both"};

            _screenUIDirectionSelected = EditorGUILayout.Popup("UI Screen Direction:",
                _screenUIDirectionSelected, screenUIDirectionType);

            _hasUIBg = EditorGUILayout.Toggle("UI Has Background:", _hasUIBg);

            if (GUILayout.Button("Create"))
            {
                SetupUI();
            }
        }

        private void SetupUI()
        {
            var selection = Selection.activeGameObject;

            if (selection == null || selection.GetComponent<Canvas>() == null)
            {
                ShowNotification(new GUIContent("You need select game object contain Canvas component"));
                return;
            }

            if (string.IsNullOrEmpty(_uiTitle) || string.IsNullOrWhiteSpace(_uiTitle))
            {
                ShowNotification(new GUIContent("UI Title can't empty or only space"));
                return;
            }

            RemoveNotification();

            var ui = CreateCenterPanel(selection.transform, 0f, 0f, 0f, 0f, _uiTitle, false);

            ui.anchorMin = Vector2.zero;
            ui.anchorMax = Vector2.one;

            switch (_screenUIDirectionSelected)
            {
                case 0:
                    CreateHorizontalPanel(ui, false);
                    break;

                case 1:
                    CreateVerticalPanel(ui, false);
                    break;

                case 2:
                    CreateBothPanel(ui, false);
                    break;
            }

            ShowNotification(new GUIContent("UI was created!"));
            Close();
        }

        #endregion

        #region Create GUI View

        private int _screenViewDirectionSelected;

        private string _viewTitle;

        private bool _hasViewBg;
        private bool _hasSetParentWithSelectedObject;

        private void CreateView()
        {
            _viewTitle = EditorGUILayout.TextField("View Title:", _viewTitle);

            var screenViewDirectionType = new[] {"Horizontal", "Vertical", "Both"};

            _screenViewDirectionSelected = EditorGUILayout.Popup("View Screen Direction:",
                _screenViewDirectionSelected, screenViewDirectionType);

            _hasViewBg = EditorGUILayout.Toggle("View Has Background:", _hasViewBg);
            _hasSetParentWithSelectedObject = EditorGUILayout.Toggle("Set Parent To Selection:",
                _hasSetParentWithSelectedObject);

            if (GUILayout.Button("Create"))
            {
                SetupView();
            }
        }

        private void SetupView()
        {
            if (string.IsNullOrEmpty(_viewTitle) || string.IsNullOrWhiteSpace(_viewTitle))
            {
                ShowNotification(new GUIContent("View Title can't empty or only space"));
                return;
            }

            RemoveNotification();

            var view = new GameObject(_viewTitle);
            var canvas = SetupCanvas(ref view);

            if (_hasSetParentWithSelectedObject)
            {
                var selection = Selection.activeGameObject;

                if (selection == null)
                {
                    ShowNotification(new GUIContent("Selected object is null"));
                }
                else
                {
                    view.transform.SetParent(selection.transform);
                }
            }

            if (_hasViewBg)
            {
                canvas.renderMode = RenderMode.ScreenSpaceCamera;
                canvas.worldCamera = FindObjectOfType<Camera>();
            }

            ShowNotification(new GUIContent("View was created!"));
            Close();
        }

        private Canvas SetupCanvas(ref GameObject go)
        {
            var viewRt = go.AddComponent<RectTransform>();
            var canvas = go.AddComponent<Canvas>();

            canvas.renderMode = RenderMode.ScreenSpaceOverlay;

            var scaler = go.AddComponent<CanvasScaler>();

            scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;

            go.AddComponent<GraphicRaycaster>();

            var img = go.AddComponent<Image>();

            img.enabled = false;
            img.raycastTarget = false;

            void ChangeResolution(float x, float y)
            {
                scaler.referenceResolution = new Vector2(x, y);
                scaler.matchWidthOrHeight = 1f;
            }

            switch (_screenViewDirectionSelected)
            {
                case 0:
                    ChangeResolution(1280f, 720f);
                    CreateHorizontalPanel(viewRt);
                    break;

                case 1:
                    ChangeResolution(720f, 1280f);
                    CreateVerticalPanel(viewRt);
                    break;

                case 2:
                    ChangeResolution(1280f, 720f);
                    CreateBothPanel(viewRt);
                    break;
            }

            return canvas;
        }

        #endregion

        #region Create Panel

        private static void CreateTopPanel(Transform parent, bool worldPositionStays = true)
        {
            var top = new GameObject("Top Panel");
            var topRt = top.AddComponent<RectTransform>();

            topRt.SetParent(parent, worldPositionStays);

            topRt.anchoredPosition = Vector2.down * 25f;
            topRt.sizeDelta = Vector2.up * 50f;
            topRt.anchorMin = Vector2.up;
            topRt.anchorMax = Vector2.one;
        }

        private static void CreateBottomPanel(Transform parent, bool worldPositionStays = true)
        {
            var bottom = new GameObject("Bottom Panel");
            var bottomRt = bottom.AddComponent<RectTransform>();

            bottomRt.SetParent(parent, worldPositionStays);

            bottomRt.anchoredPosition = Vector2.up * 25f;
            bottomRt.sizeDelta = Vector2.up * 50f;
            bottomRt.anchorMin = Vector2.zero;
            bottomRt.anchorMax = Vector2.right;
        }

        private static void CreateHorizontalPanel(Transform parent, bool worldPositionStays = true)
        {
            CreateTopPanel(parent, worldPositionStays);

            var center = new GameObject("Center Panel");
            var centerRt = center.AddComponent<RectTransform>();

            centerRt.SetParent(parent, worldPositionStays);
            centerRt.SetOffset(0f, 0f, 50f, 50f);

            centerRt.anchorMin = Vector2.zero;
            centerRt.anchorMax = Vector2.one;

            CreateBottomPanel(parent, worldPositionStays);
        }

        private static void CreateLeftPanel(Transform parent, bool worldPositionStays = true)
        {
            var left = new GameObject("Left Panel");
            var leftRt = left.AddComponent<RectTransform>();

            leftRt.SetParent(parent, worldPositionStays);

            leftRt.anchoredPosition = Vector2.right * 25f;
            leftRt.sizeDelta = Vector2.right * 50f;
            leftRt.anchorMin = Vector2.zero;
            leftRt.anchorMax = Vector2.up;
        }

        private static void CreateRightPanel(Transform parent, bool worldPositionStays = true)
        {
            var right = new GameObject("Right Panel");
            var rightRt = right.AddComponent<RectTransform>();

            rightRt.SetParent(parent, worldPositionStays);

            rightRt.anchoredPosition = Vector2.left * 25f;
            rightRt.sizeDelta = Vector2.right * 50f;
            rightRt.anchorMin = Vector2.right;
            rightRt.anchorMax = Vector2.one;
        }

        private static void CreateVerticalPanel(Transform parent, bool worldPositionStays = true)
        {
            CreateLeftPanel(parent, worldPositionStays);

            var center = CreateCenterPanel(parent, 50f, 50f, 0f, 0f,
                "Center Panel", worldPositionStays);

            center.anchorMin = Vector2.zero;
            center.anchorMax = Vector2.one;

            CreateRightPanel(parent, worldPositionStays);
        }

        private static void CreateBothPanel(Transform parent, bool worldPositionStays = true)
        {
            CreateTopPanel(parent, worldPositionStays);
            CreateLeftPanel(parent, worldPositionStays);

            var center = CreateCenterPanel(parent, 50f, 50f, 50f, 50f);

            center.anchorMin = Vector2.zero;
            center.anchorMax = Vector2.one;

            CreateBottomPanel(parent, worldPositionStays);
            CreateRightPanel(parent, worldPositionStays);
        }

        private static RectTransform CreateCenterPanel(Transform parent,
            float left, float right, float top, float bottom,
            string title = "Center Panel", bool worldPositionStays = true)
        {
            var center = new GameObject(title);
            var centerRt = center.AddComponent<RectTransform>();

            centerRt.SetParent(parent, worldPositionStays);
            centerRt.SetOffset(left, right, top, bottom);

            return centerRt;
        }

        #endregion
    }
}