﻿using UnityEngine;

namespace GameAssets.Scripts.Event
{
    public class GameEvent : MonoBehaviour
    {
        #region Delegate

        #region Base Delegate

        public delegate void Win();

        public delegate void Draw();

        public delegate void Lose();

        public delegate void ChangeLanguage(SystemLanguage language);

        public delegate void FailedInternet();

        public delegate void ConnectedInternet();

        public delegate void StopBaseCharacter();

        #endregion

        public delegate void CanUpgradeAbility();
        public delegate void StartGame();

        #endregion

        #region Static Event

        #region Base Event

        public static event Win OnWin;
        public static event Draw OnDraw;
        public static event Lose OnLose;

        public static event ChangeLanguage OnChangeLanguage;
        public static event FailedInternet OnFailedInternet;
        public static event ConnectedInternet OnConnectedInternet;
        public static event StopBaseCharacter OnStopBaseCharacter;

        #endregion

        public static event CanUpgradeAbility OnCanUpgradeAbility;
        public static event StartGame OnStartGame;

        #endregion

        #region Base Handle Event
        
        public static void DoWin()
        {
            OnWin?.Invoke();
        }

        public static void DoDraw()
        {
            OnDraw?.Invoke();
        }

        public static void DoLose()
        {
            OnLose?.Invoke();
        }

        public static void DoChangeLanguage(SystemLanguage language)
        {
            OnChangeLanguage?.Invoke(language);
        }

        public static void DoFailedInternet()
        {
            OnFailedInternet?.Invoke();
        }

        public static void DoConnectedInternet()
        {
            OnConnectedInternet?.Invoke();
        }

        public static void DoStopBaseCharacter()
        {
            OnStopBaseCharacter?.Invoke();
        }

        #endregion

        public static void DoCanUpgradeAbility()
        {
            OnCanUpgradeAbility?.Invoke();
        }

        public static void DoStartGame()
        {
            OnStartGame?.Invoke();
        }
    }
}