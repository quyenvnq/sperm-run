﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.Ex
{
    public static class AnimatorEx
    {
        private static Func<Animator, int, string> _resolveHash;
        private static Func<Animator, int, string> _getCurrentStateName;
        private static Func<Animator, int, string> _getNextStateName;
        private static readonly int State = Animator.StringToHash("State");

        public static async Task<float> ClipLength(this Animator animator)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(0.125f));
            
            var clip = animator.GetCurrentAnimatorClipInfo(0);
            return clip[0].clip.length;
        }

        public static async Task<bool> IsPlaying(this Animator animator, string stateName = null)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(0.125f));
            
            var state = animator.GetCurrentAnimatorStateInfo(0);
            var clip = animator.GetCurrentAnimatorClipInfo(0)[0].clip;

            var clipLength = clip.length;
            var currentTime = clipLength * state.normalizedTime;

            Debug.Assert(stateName != null, $"{nameof(stateName)} != null");
            return currentTime >= clipLength && state.IsName(stateName);
        }

        public static async void OnComplete(this Animator animator, Action onComplete = null,
            float frame = 0f, Action onFrameComplete = null)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(0.05f));

            var state = animator.GetCurrentAnimatorStateInfo(0);
            var clips = animator.GetCurrentAnimatorClipInfo(0);

            if (clips.Length < 1)
            {
                return;
            }

            var clipLength = clips[0].clip.length;
            var currentTime = clipLength * state.normalizedTime;

            /*Debug.Log($"START ANIMATION => {clip.name}");*/
            while (currentTime < clipLength)
            {
                state = animator.GetCurrentAnimatorStateInfo(0);
                currentTime = clipLength * state.normalizedTime;

                if (currentTime >= frame)
                {
                    onFrameComplete?.Invoke();
                    onFrameComplete = null;
                }

                await Task.Yield();
            }

            /*Debug.Log($"COMPLETE ANIMATION => {clip.name}");*/
            onComplete?.Invoke();
        }

        public static void ResetNormalizedTime(this Animator animator, string stateName)
        {
            animator.Play(Animator.StringToHash(stateName), -1, 0f);
        }

        private static Func<T, TU, TV> BuildFastOpenMemberDelegate<T, TU, TV>(string methodName)
        {
            var method = typeof(T).GetMethod(methodName,
                BindingFlags.Instance | BindingFlags.NonPublic,
                null, CallingConventions.Any, new[] {typeof(TU)}, null);

            if (method == null)
            {
                throw new ArgumentException($"Can't find method " +
                    $"{typeof(T).FullName}.{methodName}({typeof(TU).FullName})");
            }

            if (method.ReturnType != typeof(TV))
            {
                throw new ArgumentException($"Expected {typeof(T).FullName}.{methodName}({typeof(TU).FullName}) " +
                    $"to have return type of string but was {method.ReturnType.FullName}");
            }

            return (Func<T, TU, TV>) Delegate.CreateDelegate(typeof(Func<T, TU, TV>), method);
        }

        public static string GetCurrentStateName(this Animator animator, int layer = 0)
        {
            _getCurrentStateName ??=
                BuildFastOpenMemberDelegate<Animator, int, string>(nameof(GetCurrentStateName));
            return _getCurrentStateName(animator, layer);
        }

        public static string GetNextStateName(this Animator animator, int layer = 0)
        {
            _getNextStateName ??=
                BuildFastOpenMemberDelegate<Animator, int, string>(nameof(GetNextStateName));
            return _getNextStateName(animator, layer);
        }

        public static string ResolveHash(this Animator animator, int hash)
        {
            _resolveHash ??=
                BuildFastOpenMemberDelegate<Animator, int, string>(nameof(ResolveHash));
            return _resolveHash(animator, hash);
        }

        public static void ChangeState(this Animator animator, AnimationType at)
        {
            if (animator != null)
            {
                animator.SetInteger(State, (int)at);
            }
        }

        public static void ChangeLayer(this Animator animator, string layerName, bool active = true)
        {
            for (var i = 0; i < animator.layerCount; i++)
            {
                animator.SetLayerWeight(i, 0);
            }

            animator.SetLayerWeight(animator.GetLayerIndex(layerName), active ? 1 : 0);
        }
    }
}