﻿using System.Collections;
using DG.Tweening;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.GamePlay;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Ex
{
    public class CameraEx : BaseSingleton<CameraEx>
    {
        [ShowIf(nameof(followTarget))] [SerializeField]
        private Transform target;

        [SerializeField] private Transform background;

        [Range(0f, 50f)] [SerializeField] private float lerpSpeed = 0.25f;

        [SerializeField] private bool followTarget;
        [SerializeField] private bool autoDetectTargetIsPlayer;

        private Camera _camera;

        private Vector3 _originOffset;
        private Vector3 _originalPosition;

        private float _timeAtCurrentFrame;
        private float _timeAtLastFrame;
        private float _fakeDelta;

        #region MonoBehaviour

        private void Start()
        {
            _originalPosition = transform.position;
            _camera = GetComponent<Camera>();

            if (autoDetectTargetIsPlayer)
            {
                target = Player.Instance.transform;
            }

            if (followTarget)
            {
                _originOffset = transform.position - target.position;
            }

            if (background == null)
            {
                return;
            }

            background.ResizeSpriteToScreen();

            background.localScale += new Vector3(0.5f, 0.1f, 0.5f);
        }

        public override void PreInnerUpdate()
        {
            _timeAtCurrentFrame = Time.realtimeSinceStartup;
            _fakeDelta = _timeAtCurrentFrame - _timeAtLastFrame;
            _timeAtLastFrame = _timeAtCurrentFrame;
        }

        public override void PreInnerLateUpdate()
        {
            if (followTarget)
            {
                transform.DOLocalMove(target.position + _originOffset, lerpSpeed);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            transform.DOKill();
        }

        #endregion

        #region Public

        public static void Shake(float duration, float amount)
        {
            Instance.StopAllCoroutines();
            Instance.StartCoroutine(Instance.IEShake(duration, amount));
        }

        public static void StopShake()
        {
            Instance.StopAllCoroutines();
        }

        public void MoveToTarget(Vector3 exit, Transform t)
        {
            var tr = transform;

            followTarget = true;
            target = t;
            _originOffset = tr.localPosition - exit;

            _camera.DOFieldOfView(29.5f, 1f);
        }

        #endregion

        #region Private

        private IEnumerator IEShake(float duration, float amount)
        {
            while (duration > 0)
            {
                transform.localPosition = _originalPosition + Random.insideUnitSphere * amount;
                duration -= _fakeDelta;

                yield return null;
            }

            transform.localPosition = _originalPosition;
        }

        #endregion

        #region Protected

        #endregion
    }
}