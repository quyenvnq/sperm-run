﻿using GameAssets.Scripts.General;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.Ex
{
    public class FlexibleGridEx : LayoutGroup
    {
        [SerializeField] private Alignment alignment;
        [SerializeField] private FitType fitType;

        [SerializeField] private int rows;
        [SerializeField] private int columns;

        [SerializeField] private bool fitX;
        [SerializeField] private bool fitY;

        [SerializeField] private Vector2 cellSize;
        [SerializeField] private Vector2 spacing;

        private Rect _rectParent;
        private RectOffset _padding;

        private float _cellWidth;
        private float _cellHeight;

        public override void CalculateLayoutInputVertical()
        {
            base.CalculateLayoutInputHorizontal();

            _rectParent = rectTransform.rect;
            _padding = padding;

            if (fitType == FitType.Width || fitType == FitType.Height || fitType == FitType.Uniform)
            {
                fitX = true;
                fitY = true;

                var sqrt = Mathf.Sqrt(transform.childCount);

                rows = Mathf.CeilToInt(sqrt);
                columns = Mathf.CeilToInt(sqrt);
            }

            if (fitType == FitType.Width || fitType == FitType.FixedColumns || fitType == FitType.Uniform)
            {
                rows = Mathf.CeilToInt(transform.childCount / (float) columns);
            }

            if (fitType == FitType.Height || fitType == FitType.FixedRows || fitType == FitType.Uniform)
            {
                columns = Mathf.CeilToInt(transform.childCount / (float) rows);
            }

            var parentWidth = _rectParent.width;
            var parentHeight = _rectParent.height;

            if (alignment == Alignment.Horizontal)
            {
                _cellWidth = parentWidth / columns - spacing.x / columns * (columns - 1) -
                             _padding.left / (float) columns - _padding.right / (float) columns;

                _cellHeight = parentHeight / rows - spacing.y / rows * (rows - 1) -
                              _padding.top / (float) rows - _padding.bottom / (float) rows;
            }
            else
            {
                _cellHeight = parentWidth / columns - spacing.x / columns * (columns - 1) -
                              _padding.left / (float) columns - _padding.right / (float) columns;

                _cellWidth = parentHeight / rows - spacing.y / rows * (rows - 1) -
                             _padding.top / (float) rows - _padding.bottom / (float) rows;
            }

            cellSize.x = fitX ? _cellWidth : cellSize.x;
            cellSize.y = fitY ? _cellHeight : cellSize.y;

            for (var i = 0; i < rectChildren.Count; i++)
            {
                var item = rectChildren[i];
                int column;
                int row;

                if (alignment == Alignment.Horizontal)
                {
                    row = i / columns;
                    column = i % columns;

                    var xPos = cellSize.x * column + spacing.x * column + _padding.left;
                    var yPos = cellSize.y * row + spacing.y * row + _padding.top;

                    SetChildAlongAxis(item, 0, xPos, cellSize.x);
                    SetChildAlongAxis(item, 1, yPos, cellSize.y);
                }
                else
                {
                    row = i / rows;
                    column = i % rows;

                    var xPos = cellSize.x * column + spacing.x * column + _padding.left;
                    var yPos = cellSize.y * row + spacing.y * row + _padding.top;

                    SetChildAlongAxis(item, 0, yPos, cellSize.y);
                    SetChildAlongAxis(item, 1, xPos, cellSize.x);
                }
            }
        }

        public override void SetLayoutHorizontal()
        {
        }

        public override void SetLayoutVertical()
        {
        }
    }
}