﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;
using SRandom = System.Random;
using URandom = UnityEngine.Random;

namespace GameAssets.Scripts.Ex
{
    public enum CornerDirection
    {
        BottomLeft,
        TopLeft,
        TopRight,
        BottomRight,
        BottomCenter,
        LeftCenter,
        TopCenter,
        RightCenter,
        Center
    }

    [Serializable]
    internal class CornerData
    {
        public CornerDirection cornerDirection;
        public Vector3 value;
    }

    public static class MathfEx
    {
        private static Dictionary<CornerDirection, Vector3> _corners =
            new Dictionary<CornerDirection, Vector3>();

#region List

        public static Dictionary<CornerDirection, Vector3> GetWorldCorner(this RectTransform rt)
        {
            var v4 = new Vector3[4];

            _corners.Clear();
            rt.GetWorldCorners(v4);

            var bottomCenter = (v4[0] + v4[3]) / 2f;
            var topCenter = (v4[1] + v4[2]) / 2f;
            var leftCenter = (v4[0] + v4[1]) / 2f;
            var rightCenter = (v4[2] + v4[3]) / 2f;
            var center = (leftCenter + rightCenter) / 2f;

            // All direction corner rect transform
            // v4[1]-Top Left     |    Top Center    |    v4[2]-Top Right
            //      -
            // Left Center        |      Center      |     Right Center
            //      -
            // v4[0]-Bottom Left  |   Bottom Center  |    v4[3]-Bottom Right

            _corners = new Dictionary<CornerDirection, Vector3>
            {
                {
                    CornerDirection.BottomLeft, v4[0]
                },
                {
                    CornerDirection.LeftCenter, leftCenter
                },
                {
                    CornerDirection.TopLeft, v4[1]
                },
                {
                    CornerDirection.TopCenter, topCenter
                },
                {
                    CornerDirection.TopRight, v4[2]
                },
                {
                    CornerDirection.RightCenter, rightCenter
                },
                {
                    CornerDirection.BottomRight, v4[3]
                },
                {
                    CornerDirection.BottomCenter, bottomCenter
                },
                {
                    CornerDirection.Center, center
                }
            };

            return _corners;
        }

        /// <summary>
        /// On Transform need you return true/false, true if you have successfully executed the function with the searched child, false otherwise
        /// </summary>
        /// <param name="t"></param>
        /// <param name="onTransform"></param>
        public static void Children(this Transform t, Func<Transform, bool> onTransform)
        {
            foreach (var x in t)
            {
                var tr = x as Transform;

                if (tr == null)
                {
                    continue;
                }

                if (onTransform(t))
                {
                    return;
                }

                if (tr.childCount > 0)
                {
                    Children(t, onTransform);
                }
            }
        }

        public static List<string> ReturnStringSelectedElements<T>(T type) where T : Enum
        {
            var selectedElements = new List<string>();
            var values = Enum.GetValues(typeof(T));
            var currentEnum = type as Enum;

            for (var i = 0; i < values.Length; i++)
            {
                var layer = 1 << i;
                var index = Array.IndexOf(values, currentEnum);

                if ((index & layer) != 0)
                {
                    selectedElements.Add($"{values.GetValue(i)}");
                }
            }

            return selectedElements;
        }

        public static List<int> ReturnIntSelectedElements<T>(T type) where T : Enum
        {
            var selectedElements = new List<int>();
            var values = Enum.GetValues(typeof(T));
            var currentEnum = type as Enum;

            for (var i = 0; i < values.Length; i++)
            {
                var layer = 1 << i;
                var index = Array.IndexOf(values, currentEnum);

                if ((index & layer) != 0)
                {
                    selectedElements.Add(i);
                }
            }

            return selectedElements;
        }

        public static List<int> GetSelectedIndexes<T>(T val) where T : IConvertible
        {
            var selectedIndexes = new List<int>();

            for (var i = 0; i < Enum.GetValues(typeof(T)).Length; i++)
            {
                var layer = 1 << i;

                if ((Convert.ToInt32(val) & layer) != 0)
                {
                    selectedIndexes.Add(i);
                }
            }

            return selectedIndexes;
        }

        public static List<string> GetSelectedStrings<T>(T val) where T : IConvertible
        {
            var selectedStrings = new List<string>();

            for (var i = 0; i < Enum.GetValues(typeof(T)).Length; i++)
            {
                var layer = 1 << i;

                if ((Convert.ToInt32(val) & layer) != 0)
                {
                    selectedStrings.Add(Enum.GetValues(typeof(T)).GetValue(i).ToString());
                }
            }

            return selectedStrings;
        }

#endregion

#region Enum

        public static AngleDirection CalculateAngle(Vector3 origin, Vector3 end)
        {
            if ((origin - end).magnitude <= Const.EPSILON_MIN)
            {
                return AngleDirection.None;
            }

            var swipeAngle = Mathf.Atan2(end.y - origin.y, end.x - origin.x) * 180f / Mathf.PI;

            if (swipeAngle > -45f)
            {
                // Right swipe
                return AngleDirection.Right;
            }

            if (swipeAngle > 45f && swipeAngle <= 135f)
            {
                // Up swipe
                return AngleDirection.Up;
            }

            if (swipeAngle > 135f || swipeAngle <= -135f)
            {
                // Left swipe
                return AngleDirection.Left;
            }

            if (swipeAngle < -45f && swipeAngle >= -135f)
            {
                // Down swipe
                return AngleDirection.Down;
            }

            return AngleDirection.None;
        }

#endregion

#region Void

        public static void Insert<TKey, TValue>(this IDictionary<TKey, TValue> dt, int index, TKey key, TValue value)
        {
            dt.Keys.ToList().Insert(index, key);
            dt.Values.ToList().Insert(index, value);
        }

        public static void LimitCamera(this Transform t)
        {
            var p = GameManager.Camera.WorldToViewportPoint(t.position);

            p.x = Mathf.Clamp01(p.x);
            p.y = Mathf.Clamp01(p.y);

            t.position = GameManager.Camera.ViewportToWorldPoint(p);
        }

        /// <summary>
        /// Keep the ui inside the camera eg tooltip
        /// BL is bottom left
        /// TR is top right
        /// </summary>
        /// <param name="rect">ui need to keep inside camera</param>
        /// <param name="canvasParent">canvas contains the ui to keep inside</param>
        /// <param name="newPosition">new position the tooltip can place</param>
        public static void KeepInsideCamera(this RectTransform rect, RectTransform canvasParent,
            Vector3? newPosition = null)
        {
            var v4 = new Vector3[4];

            if (newPosition != null)
            {
                rect.position = (Vector3) newPosition;
            }

            canvasParent.GetWorldCorners(v4);

            var canvasBL = v4[0];
            var canvasTR = v4[2];
            var canvasSize = canvasTR - canvasBL;

            rect.GetWorldCorners(v4);

            var rectBL = v4[0];
            var rectTR = v4[2];
            var rectSize = rectTR - rectBL;

            var position = rect.position;
            var deltaBL = position - rectBL;
            var deltaTR = rectTR - position;

            position.x = rectSize.x < canvasSize.x
                ? Mathf.Clamp(position.x, canvasBL.x + deltaBL.x, canvasTR.x - deltaTR.x)
                : Mathf.Clamp(position.x, canvasTR.x - deltaTR.x, canvasBL.x + deltaBL.x);

            position.y = rectSize.y < canvasSize.y
                ? Mathf.Clamp(position.y, canvasBL.y + deltaBL.y, canvasTR.y - deltaTR.y)
                : Mathf.Clamp(position.y, canvasTR.y - deltaTR.y, canvasBL.y + deltaBL.y);

            rect.position = position;
        }

#endregion

#region Quaternion

        public static Quaternion ToQuaternion(this Vector3 v)
        {
            var angle = Mathf.Atan2(v.x, v.z);

            return new Quaternion(v.x * Mathf.Sin(angle / 2), v.y * Mathf.Sin(angle / 2),
                v.z * Mathf.Sin(angle / 2), Mathf.Cos(angle / 2));
        }

#endregion

#region Vector3

        public static Vector2 ResizeSpriteToScreen(this Transform t)
        {
            var sr = t.GetComponent<SpriteRenderer>();

            if (sr == null)
            {
                return Vector2.zero;
            }

            var sprite = sr.sprite;
            var width = sprite.bounds.size.x;
            var height = sprite.bounds.size.y;

            var worldScreenHeight = GameManager.Camera.orthographicSize * 2f;
            var worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
            var scale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, 1f);

            t.localScale = scale;
            return sprite.pivot;
        }

        public static Vector3 ScreenPositionInClick(this Vector3 v)
        {
            return !Physics.Raycast(GameManager.Camera.ScreenPointToRay(v), out var hit) ? Vector3.zero : hit.point;
        }

        public static Vector3 Random(this Bounds bounds, float border = 0f)
        {
            var min = bounds.min;
            var max = bounds.max;

            var rnd = new Vector3(URandom.Range(min.x, max.x),
                URandom.Range(min.y, max.y),
                URandom.Range(min.z, max.z));

            var x = rnd.x < 0f ? rnd.x + border : rnd.x - border;
            var z = rnd.z < 0f ? rnd.z + border : rnd.z - border;
            return new Vector3(x, rnd.y, z);
        }

        public static Vector3 PlatformPosition =>
            Application.platform switch
            {
                RuntimePlatform.WindowsEditor => Input.mousePosition,
                RuntimePlatform.WindowsPlayer => Input.mousePosition,
                RuntimePlatform.WebGLPlayer => Input.mousePosition,

                RuntimePlatform.Android => Input.touchCount > 0
                    ? (Vector3) Input.GetTouch(0).position
                    : Vector3.zero,

                RuntimePlatform.IPhonePlayer => Input.touchCount > 0
                    ? (Vector3) Input.GetTouch(0).position
                    : Vector3.zero,

                _ => Vector3.zero
            };

        public static Vector3 Abs(this Vector3 v)
        {
            return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        }

#endregion

#region Int

        public static int? ConvertLayerIndex(this int layer)
        {
            for (var index = 0; index < sizeof(int) * 8; index++)
            {
                if ((layer & 1) == 1)
                {
                    return index;
                }

                layer >>= 1;
            }

            return null;
        }

        public static int DropChance(this List<float> drops)
        {
            var range = drops.Sum();
            var rnd = URandom.Range(0, range);

            var maxDrop = 0f;
            var index = 0;
            var top = 0f;

            for (var i = 0; i < drops.Count; i++)
            {
                var item = drops[i];

                if (maxDrop < item)
                {
                    maxDrop = item;
                    index = i;
                }

                top += item;

                if (rnd < top)
                {
                    return i;
                }
            }

            return index;
        }

#endregion

#region Float

        public static float BoundsContainedPercentage(this Bounds origin, Bounds target)
        {
            var total = 1f;

            for (var i = 0; i < 3; i++)
            {
                var dist = target.min[i] > origin.center[i]
                    ? target.max[i] - origin.max[i]
                    : origin.min[i] - target.min[i];

                total *= Mathf.Clamp01(1f - dist / target.size[i]);
            }

            return total;
        }

        public static float NextFloat(this SRandom rnd, float max, float min)
        {
            return min > max
                ? (float) rnd.NextDouble() * (min - max) + max
                : (float) rnd.NextDouble() * (max - min) + min;
        }

        public static float AngleBetween(Vector3 origin, Vector3 target)
        {
            var direction = origin - target;
            return (360 + Mathf.Atan2(direction.x, direction.z) * (180 / Mathf.PI)) % 360;
        }

#endregion

#region Double

        public static double Abs(this double d)
        {
            return d > 0 ? d : d * -1d;
        }

        public static double NextDouble(this SRandom rnd, double max, double min)
        {
            return min > max
                ? rnd.NextDouble() * (min - max) + max
                : rnd.NextDouble() * (max - min) + min;
        }

#endregion

#region String

        public static string Minimum10(this float value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this int value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this double value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Match(this string s, char c1, char c2 = '\0', bool firstLast = false)
        {
            var begin = firstLast ? s.LastIndexOf(c1) : s.IndexOf(c1);
            var end = c2 == '\0' ? s.Length : s.LastIndexOf(c2);
            return s.Substring(begin + 1, end - begin - 1);
        }

        public static string RandomString(int min = 1, int max = 5)
        {
            if (max >= Const.HASH.Length - 1)
            {
                max = Const.HASH.Length - 1;
            }

            var chars = URandom.Range(min, max);
            var result = $"{Const.HASH[chars]}";

            for (var i = 0; i < chars; i++)
            {
                result += Const.HASH[URandom.Range(0, Const.HASH.Length)];
            }

            return result;
        }

#endregion

#region Bool

        public static bool IsEnglishLetter(this char c)
        {
            return c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z';
        }

        public static bool IsList(this object o)
        {
            if (o == null)
            {
                return false;
            }

            return o is IList
                   && o.GetType().IsGenericType
                   && o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }

        public static bool IsDictionary(this object o)
        {
            if (o == null)
            {
                return false;
            }

            return o is IDictionary
                   && o.GetType().IsGenericType
                   && o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<,>));
        }

        public static bool Equal(float a, float b, float epsilon)
        {
            var absA = Math.Abs(a);
            var absB = Math.Abs(b);
            var diff = Math.Abs(a - b);

            if (Equal(a, b))
            {
                return true;
            }

            if (Equal(a, 0f) || Equal(b, 0f) || absA + absB < Const.EPSILON_MIN)
            {
                return diff < epsilon * Const.EPSILON_MIN;
            }

            return diff / (absA + absB) < epsilon;
        }

        public static bool Equal(float a, float b)
        {
            return Mathf.Abs(a - b) <= Const.EPSILON_MIN;
        }

        public static bool Equal(double a, double b)
        {
            return (a - b).Abs() <= Const.EPSILON_MIN;
        }

        public static bool Equal(Vector3 a, Vector3 b)
        {
            return Equal(a.x, b.x) && Equal(a.y, b.y) && Equal(a.z, b.z);
        }

        public static bool NextBool => URandom.value > 0.5f;

        public static bool Distance(Vector3 a, Vector3 b, float distance)
        {
            return (a - b).sqrMagnitude < distance * distance;
        }

#endregion
    }
}