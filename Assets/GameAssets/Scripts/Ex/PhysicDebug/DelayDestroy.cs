﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace GameAssets.Scripts.Ex.PhysicDebug
{
    public class DelayDestroy : MonoBehaviour
    {
        public float delay = 7.5f;

        public void StartUp()
        {
            Destroy();
        }

        private async void Destroy()
        {
            await UniTask.Delay(TimeSpan.FromSeconds(delay));

            Destroy(gameObject);
        }
    }
}