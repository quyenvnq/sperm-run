﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.Ex.PhysicDebug
{
    [RequireComponent(typeof(Camera))]
    public class GLDebug : MonoBehaviour
    {
        private static GLDebug _instance;
        private static Material _mMatOn;
        private static Material _mMatOff;

        private List<Line> _lines;

        public bool displayLines = true;

        public Shader zOnShader;
        public Shader zOffShader;

        public static GLDebug Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }

                var main = Camera.main;

                _instance = !(main == null)
                    ? main.gameObject.AddComponent<GLDebug>()
                    : throw new Exception(
                        "Couldn't find any main camera to attach the GLDebug script. System will not work");

                return _instance;
            }
        }

        private void Awake()
        {
            SetMaterial();

            _lines = new List<Line>();

            if (_instance == null)
            {
                _instance = this;
            }
            else
            {
                if (_instance != this)
                {
                    Destroy(this);
                }
            }
        }

        private void SetMaterial()
        {
            if (_mMatOn == null)
            {
                _mMatOn = !(zOnShader == null)
                    ? new Material(zOnShader)
                    : new Material(Shader.Find("Debug/GLlineZOn"));

                _mMatOn.hideFlags = HideFlags.HideAndDontSave;
                _mMatOn.shader.hideFlags = HideFlags.HideAndDontSave;
            }

            if (!(_mMatOff == null))
            {
                return;
            }

            _mMatOff = !(zOffShader == null)
                ? new Material(zOffShader)
                : new Material(Shader.Find("Debug/GLlineZOff"));

            _mMatOff.hideFlags = HideFlags.HideAndDontSave;
            _mMatOff.shader.hideFlags = HideFlags.HideAndDontSave;
        }

        private void OnPostRender()
        {
            if (!displayLines)
            {
                return;
            }

            _mMatOn.SetPass(0);
            
            GL.Begin(1);

            Line line;

            for (var index = _lines.Count - 1; index >= 0; --index)
            {
                int num;

                if (_lines[index].depthTest)
                {
                    line = _lines[index];
                    num = line.DurationElapsed(true) ? 1 : 0;
                }
                else
                {
                    num = 0;
                }

                if (num != 0)
                {
                    _lines.RemoveAt(index);
                }
            }

            GL.End();

            _mMatOff.SetPass(0);

            GL.Begin(1);

            for (var index = _lines.Count - 1; index >= 0; --index)
            {
                int num;

                if (!_lines[index].depthTest)
                {
                    line = _lines[index];
                    num = line.DurationElapsed(true) ? 1 : 0;
                }
                else
                {
                    num = 0;
                }

                if (num != 0)
                {
                    _lines.RemoveAt(index);
                }
            }

            GL.End();
        }

        private static void _DrawLine(Vector3 start, Vector3 end, Color color, float duration, bool depthTest)
        {
            if ((duration != 0.0 || Instance.displayLines) && start != end)
            {
                Instance._lines.Add(new Line(start, end, color, Time.time, duration, depthTest));
            }
        }

        public static void DrawLine(Vector3 start, Vector3 end, Color? color = null, float duration = 0f,
            bool depthTest = false)
        {
            _DrawLine(start, end, color ?? Color.white, duration, depthTest);
        }

        public static void DrawRay(Vector3 start, Vector3 dir, Color? color = null, float duration = 0f,
            bool depthTest = false)
        {
            if (dir != Vector3.zero)
            {
                DrawLine(start, start + dir, color, duration, depthTest);
            }
        }

        private readonly struct Line
        {
            private readonly Vector3 _start;
            private readonly Vector3 _end;

            private readonly Color _color;

            private readonly float _startTime;
            private readonly float _duration;

            public readonly bool depthTest;

            public Line(Vector3 start, Vector3 end, Color color, float startTime, float duration, bool depthTest)
            {
                _start = start;
                _end = end;
                _color = color;
                _startTime = startTime;
                _duration = duration;
                this.depthTest = depthTest;
            }

            public bool DurationElapsed(bool drawLine)
            {
                if (!drawLine)
                {
                    return Time.time - (double) _startTime >= _duration;
                }

                GL.Color(_color);
                GL.Vertex(_start);
                GL.Vertex(_end);

                return Time.time - (double) _startTime >= _duration;
            }
        }
    }
}