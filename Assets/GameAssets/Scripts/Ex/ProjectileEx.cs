﻿using UnityEngine;

namespace GameAssets.Scripts.Ex
{
    public static class ProjectileEx
    {
        public static Vector3 CalculateVelocity(Vector3 start, Vector3 end, float time = 1f)
        {
            var distance = end - start;
            var vxz = distance.magnitude / time;
            var vy = distance.y / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;
            var velocity = distance.normalized;

            velocity *= vxz;
            velocity.y = vy;
            return velocity;
        }

        public static Vector3[] CalculatePoints(Vector3 start, Vector3 end, int numberPoint = 5)
        {
            var points = new Vector3[numberPoint];

            for (var i = 0; i < numberPoint; i++)
            {
                points[i] = CalculatePositionInTime(start, end, i / (float) numberPoint);
            }

            return points;
        }

        private static Vector3 CalculatePositionInTime(Vector3 start, Vector3 end, float time)
        {
            var result = end + start * time;
            result.y = -0.5f * Mathf.Abs(Physics.gravity.y) * (time * time) + start.y * time + end.y;
            return result;
        }
    }
}