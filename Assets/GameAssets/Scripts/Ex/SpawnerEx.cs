﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.Manager.Game;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.Ex
{
    public class SpawnerEx : BaseSingleton<SpawnerEx>
    {
        [SerializeField] private Canvas cvWorld;

        [SerializeField] private GameObject emptyPrefab;
        [SerializeField] private GameObject uiToastPrefab;
        [SerializeField] private GameObject uiCombatPrefab;
        [SerializeField] private GameObject uiNotificationPrefab;
        [SerializeField] private GameObject uiImageItemPrefab;

        [ShowIf(nameof(initiation))] [SerializeField]
        private int amount;

        [SerializeField] private bool initiation;

        private static readonly Dictionary<string, Queue<GameObject>> Pooling =
            new Dictionary<string, Queue<GameObject>>();

        private static Transform _toast;
        private static Transform _notification;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Setup()
        {
        }

        private void CheckCombatCamera()
        {
            if (cvWorld.worldCamera == null)
            {
                cvWorld.worldCamera = GameManager.Camera;
            }
        }

        private Transform CreateLocalImageItem(Transform parent)
        {
            return CreateUISpawner(parent, uiImageItemPrefab);
        }

        private Transform CreateToast()
        {
            return CreateSpawner(Vector3.zero, null, uiToastPrefab);
        }

        private Transform CreateCombat(Vector3 position)
        {
            return CreateSpawner(position, cvWorld.transform, uiCombatPrefab);
        }

        private Transform CreateNotification()
        {
            return CreateSpawner(Vector3.zero, null, uiNotificationPrefab);
        }

        private static Transform GetObject(GameObject go, Transform parent, bool ui)
        {
            if (!Pooling.TryGetValue(go.name, out var queue))
            {
                return ui ? CreateUIObject(go, parent) : CreateObject(go, parent);
            }

            if (queue.Count == 0)
            {
                return ui ? CreateUIObject(go, parent) : CreateObject(go, parent);
            }

            var clone = queue.Dequeue();

            if (clone != null && clone.activeInHierarchy)
            {
                clone = queue.FirstOrDefault(x => x != null && !x.activeInHierarchy);

                queue.Clear();
            }

            if (clone == null)
            {
                clone = (ui ? CreateUIObject(go, parent) : CreateObject(go, parent)).gameObject;
            }

            clone.transform.SetParent(parent);
            clone.SetActive(true);

            return clone.transform;
        }

        private static Transform CreateObject(GameObject go, Transform parent)
        {
            var clone = Instantiate(go, Vector3.zero, Quaternion.identity, parent);

            clone.name = go.name;
            return clone.transform;
        }

        private static Transform CreateUIObject(GameObject go, Transform parent)
        {
            var clone = Instantiate(go, parent);

            clone.name = go.name;
            return clone.transform;
        }

        private static async void DestroyObject(Component go, float destroyTime)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(destroyTime));

            if (go == null)
            {
                return;
            }

            if (Pooling.TryGetValue(go.name, out var queue))
            {
                queue.Enqueue(go.gameObject);
            }
            else
            {
                var newQueue = new Queue<GameObject>();

                newQueue.Enqueue(go.gameObject);
                Pooling.Add(go.name, newQueue);
            }

            go.gameObject.SetActive(false);
        }

        public static Transform CreateSpawner(Vector3 position, Transform parent, GameObject prefab)
        {
            var go = GetObject(prefab, parent, false);

            go.transform.position = position;
            return go;
        }

        public static Transform CreateUISpawner(Transform parent, GameObject prefab)
        {
            return GetObject(prefab, parent, true);
        }

        public static void DestroySpawner(Transform go, float destroyTime = 0f)
        {
            DestroyObject(go, destroyTime);
        }

        public static Transform CreateDestroy(Vector3 value, Transform parent,
            GameObject prefab, float destroyTime = 0f)
        {
            var clone = CreateSpawner(value, parent, prefab);

            DestroySpawner(clone, destroyTime);
            return clone;
        }

        public static Transform CreateDestroyUI(Transform parent, GameObject prefab, float destroyTime = 0f)
        {
            var clone = CreateUISpawner(parent, prefab);

            DestroySpawner(clone, destroyTime);
            return clone;
        }

        public static void CreateToast(string msg, float destroyTime = 1.5f)
        {
            if (_toast != null)
            {
                DestroySpawner(_toast, destroyTime);
            }

            _toast = Instance.CreateToast();

            var container = _toast.GetComponentInChildren<RectTransform>();
            var content = _toast.GetComponentInChildren<TextMeshProUGUI>();

            content.text = msg;

            SetPosition(container);
            DestroySpawner(_toast, destroyTime);
        }

        public static Transform CreateCombat(Vector3 position, string text)
        {
            Instance.CheckCombatCamera();

            var clone = Instance.CreateCombat(position);

            clone.GetComponent<Combat>().SetText(text);
            return clone;
        }

        public static void CreateNotification(string message, string title = "Thông báo", Action onOk = null)
        {
            DestroyNotification();

            _notification = Instance.CreateNotification();
            _notification.GetComponentInChildren<Notification>()?.Initiation(message, title, onOk);
        }

        public static async void DoMoveAndScaleToTarget(Transform parent, GameObject prefab, Vector3 target, float min,
            float max, float scale, float duration, float radius, Action<Transform, Vector2> onCreatePrefab)
        {
            var rnd = Random.Range(min, max);

            for (var i = 0; i < rnd; i++)
            {
                var sequence = DOTween.Sequence();
                var go = CreateUISpawner(parent, prefab);
                var vector = Random.insideUnitCircle * radius;

                onCreatePrefab?.Invoke(go, vector);

                sequence.Join(go.transform.DOMove(target, duration))
                    .Join(go.transform.DOScale(scale, duration))
                    .Play()
                    .OnComplete(() => DestroySpawner(go));

                await UniTask.Delay(TimeSpan.FromSeconds(0.05f));
            }
        }

        public static void DestroyNotification()
        {
            DestroySpawner(_notification);
        }

        public static Transform CreateImageItem(Transform parent)
        {
            return Instance.CreateLocalImageItem(parent);
        }

        private static void SetPosition(RectTransform rt)
        {
            rt.anchorMin = new Vector2(0.5f, 0);
            rt.anchorMax = new Vector2(0.5f, 0);
            rt.anchoredPosition = new Vector2(0.5f, 100f);
        }
    }
}