﻿using System;
using System.Text.RegularExpressions;
using GameAssets.Scripts.Base.Interface;
using GameAssets.Scripts.General;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.Ex
{
    public class ValidatorEx : MonoBehaviour, IInitialize
    {
        [SerializeField] private ValidatorType validatorType;

        [ShowIf("@this.validatorType == GameAssets.Scripts.General.ValidatorType.Password")] [SerializeField]
        private FormatType formatType;

        [SerializeField] private TMP_InputField txtInputField;

        [ShowIf("@this.validatorType == GameAssets.Scripts.General.ValidatorType.ConfirmPassword")] [SerializeField]
        private TMP_InputField txtInputPassword;

        [ShowIf("@this.validatorType == GameAssets.Scripts.General.ValidatorType.String")] [SerializeField]
        private string title;

        [ShowIf("@this.validatorType != GameAssets.Scripts.General.ValidatorType.Email")]
        [Range(6, 100)]
        [SerializeField]
        private int minimum;

        [ShowIf("@this.validatorType == GameAssets.Scripts.General.ValidatorType.String")] [SerializeField]
        private bool checkSpecialCharacter;

        public string Error { get; private set; }

        public bool Success { get; private set; }

        private bool _disable;

        private void Awake()
        {
            txtInputField.onValueChanged.AddListener(OnValueChanged);
        }

        private void OnDestroy()
        {
            txtInputField.onValueChanged.RemoveAllListeners();
        }

        private void OnValueChanged(string value)
        {
            switch (validatorType)
            {
                case ValidatorType.String:
                    if (string.IsNullOrEmpty(txtInputField.text))
                    {
                        ShowError($"{title} can't be empty");
                        return;
                    }

                    if (txtInputField.text.Length < minimum)
                    {
                        ShowError($"{title} at least {minimum} characters");
                        return;
                    }

                    if (txtInputField.text[0].IsEnglishLetter())
                    {
                        if (checkSpecialCharacter && Regex.IsMatch(txtInputField.text, Const.PATTERN_SPECIAL_CHAR))
                        {
                            ShowError($"{title} doesn't contain special characters");
                            return;
                        }

                        DestroyError();
                    }
                    else
                    {
                        ShowError("The first character must be a letter");
                    }

                    break;

                case ValidatorType.Email:
                    if (string.IsNullOrEmpty(txtInputField.text))
                    {
                        ShowError("Email cannot be empty");
                        return;
                    }

                    if (Regex.IsMatch(txtInputField.text, Const.PATTERN_EMAIL))
                    {
                        DestroyError();
                    }
                    else
                    {
                        ShowError("Email invalidate");
                    }

                    break;

                case ValidatorType.Password:
                    CheckPasswordFormat(false);
                    break;

                case ValidatorType.ConfirmPassword:
                    CheckPasswordFormat(true);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void CheckPasswordFormat(bool confirmPassword)
        {
            var txt = confirmPassword ? "Confirm password" : "Password";

            if (string.IsNullOrEmpty(txtInputField.text))
            {
                ShowError($"{txt} cannot be empty");
                return;
            }

            switch (formatType)
            {
                case FormatType.LowerNumber:
                    if (Regex.IsMatch(txtInputField.text, Const.PatternLowerNumber(minimum)))
                    {
                        if (confirmPassword && txtInputField.text != txtInputPassword.text)
                        {
                            ShowError($"{txt} doesn't match");
                            return;
                        }

                        DestroyError();
                    }
                    else
                    {
                        if (MinimumPassword())
                        {
                            ShowError($"{txt} must at least a lower letter and a number");
                        }
                    }

                    break;

                case FormatType.LowerNumberSpecial:
                    if (Regex.IsMatch(txtInputField.text, Const.PatternLowerNumberSpecial(minimum)))
                    {
                        if (confirmPassword && txtInputField.text != txtInputPassword.text)
                        {
                            ShowError($"{txt} doesn't match");
                            return;
                        }

                        DestroyError();
                    }
                    else
                    {
                        if (MinimumPassword())
                        {
                            ShowError($"{txt} must at least a lower letter, a number and a special character");
                        }
                    }

                    break;

                case FormatType.UpperLowerNumber:
                    if (Regex.IsMatch(txtInputField.text, Const.PatternUpperLowerNumber(minimum)))
                    {
                        if (confirmPassword && txtInputField.text != txtInputPassword.text)
                        {
                            ShowError($"{txt} doesn't match");
                            return;
                        }

                        DestroyError();
                    }
                    else
                    {
                        if (MinimumPassword())
                        {
                            ShowError($"{txt} must at least a upper letter, a lower letter and a number");
                        }
                    }

                    break;

                case FormatType.UpperLowerNumberSpecial:
                    if (Regex.IsMatch(txtInputField.text, Const.PatternUpperLowerNumberSpecial(minimum)))
                    {
                        if (confirmPassword && txtInputField.text != txtInputPassword.text)
                        {
                            ShowError("Password doesn't match");
                            return;
                        }

                        DestroyError();
                    }
                    else
                    {
                        if (MinimumPassword())
                        {
                            ShowError($"{txt} must at least a upper letter, " +
                                      "a lower letter, a number and a special character");
                        }
                    }

                    break;
            }
        }

        private void ShowError(string error)
        {
            Error = error;

            if (!_disable)
            {
                Success = false;
            }
            else
            {
                _disable = false;
            }
        }

        private void DestroyError()
        {
            Success = true;
            // SpawnerHelper.CreateToast($"{validatorType} success", 2f);
        }

        private bool MinimumPassword()
        {
            if (txtInputField.text.Length >= minimum)
            {
                return true;
            }

            ShowError($"Password must at least {minimum} chars");
            return false;
        }

        public void ResetError()
        {
            _disable = true;
            txtInputField.text = string.Empty;
        }

        [Button]
        public void Initialize()
        {
            txtInputField = GetComponent<TMP_InputField>();

            if (validatorType.Equals(ValidatorType.Password) || validatorType.Equals(ValidatorType.ConfirmPassword))
            {
                txtInputField.contentType = TMP_InputField.ContentType.Password;
            }
        }
    }
}