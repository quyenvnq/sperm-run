﻿using System.Linq;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.UI.UIPlayMenu;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay
{
    public class Ability : MonoBehaviour
    {
        [Header("[SPRITE]")] [PreviewField(50, ObjectFieldAlignment.Left)] [SerializeField]
        private Sprite icEnableTop;

        [PreviewField(50, ObjectFieldAlignment.Left)] [SerializeField]
        private Sprite icEnableBottom;

        [PreviewField(50, ObjectFieldAlignment.Left)] [SerializeField]
        private Sprite icDisableTop;

        [PreviewField(50, ObjectFieldAlignment.Left)] [SerializeField]
        private Sprite icDisableBottom;

        [Space] [SerializeField] private Model.Ability ability;

        [SerializeField] private Button btnUpgrade;

        [SerializeField] private Image imgTop;
        [SerializeField] private Image imgBottom;

        [SerializeField] private TextMeshProUGUI txtLevel;
        [SerializeField] private TextMeshProUGUI txtPrice;

        [SerializeField] private bool updateSperm;

        private UIPlayMenu _uiPlayMenu;

        private void Start()
        {
            _uiPlayMenu = UIManager.Instance.GetUI<UIPlayMenu>();

            foreach (var x in UserDataManager.Instance.userDataSave.abilities
                .Where(x => ability.id == x.id))
            {
                ability.SetAbility(x);
                break;
            }

            UpdateAbility();
            CanUpgradeAbility();

            btnUpgrade.AddListener(() =>
            {
                if (!ability.CanUpgrade())
                {
                    return;
                }

                _uiPlayMenu.UpdateGold();
                UpdateAbility();

                if (updateSperm)
                {
                    Player.Instance.BuySperm(1);
                }

                AudioManager.Instance.PlaySound("upgrade");
                GameEvent.DoCanUpgradeAbility();
            });

            GameEvent.OnCanUpgradeAbility += CanUpgradeAbility;
        }

        private void OnDestroy()
        {
            GameEvent.OnCanUpgradeAbility -= CanUpgradeAbility;
        }

        private void UpdateAbility()
        {
            if (ability.IsMaxLevel())
            {
                txtLevel.text = "MAX";

                foreach (var x in txtPrice.transform.parent)
                {
                    var t = x as Transform;

                    if (t != null)
                    {
                        t.gameObject.SetActive(false);
                    }
                }

                btnUpgrade.SetInteractable(false, false);
            }
            else
            {
                txtLevel.text = $"LEVEL {ability.level}";
                txtPrice.text = $"{ability.Price()}";
            }
        }

        private void CanUpgradeAbility()
        {
            if (ability.Price() > UserDataManager.Instance.userDataSave.gold)
            {
                if (!ability.IsMaxLevel())
                {
                    btnUpgrade.SetInteractable(false);
                }

                imgTop.sprite = icDisableTop;
                imgBottom.sprite = icDisableBottom;
            }
            else
            {
                if (!ability.IsMaxLevel())
                {
                    btnUpgrade.SetInteractable(true);
                }

                imgTop.sprite = icEnableTop;
                imgBottom.sprite = icEnableBottom;
            }
        }
    }
}