﻿using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager.Audio;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Baby : MonoBehaviour
    {
        [SerializeField] private Rigidbody rb;
        [SerializeField] private Animator animator;

        [SerializeField] private float gravity = -9.81f;

        #region MonoBehaviour

        private void Awake()
        {
            animator.enabled = false;
        }

        private void Update()
        {
            if (!rb.isKinematic)
            {
                transform.rotation = Quaternion.LookRotation(rb.velocity);
            }
        }

        #endregion

        #region Collider

        #endregion

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Finish"))
            {
                return;
            }

            var bonus = other.GetComponent<Bonus>();

            rb.ResetInertia();

            rb.isKinematic = true;
            animator.enabled = true;

            var t = transform;
            var position = t.position;

            position = new Vector3(position.x, 1.25f, position.z);
            t.position = position;
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);

            AudioManager.Instance.PlayVibrate();
            Player.Instance.BonusMultiple(bonus.Value);
            GameEvent.DoWin();
        }

        #region Public

        public void Fire(Vector3 target)
        {
            var t = transform;
            var tp = t.position;

            CameraEx.Instance.MoveToTarget(tp, t);

            Physics.gravity = new Vector3(Physics.gravity.x, gravity, Physics.gravity.z);

            rb.isKinematic = false;
            rb.velocity = ProjectileEx.CalculateVelocity(tp, target);
        }

        #endregion

        #region Private

        #endregion

        #region Protected

        #endregion
    }
}