using GameAssets.Scripts.Ex;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Bonus : MonoBehaviour
    {
        [SerializeField] private new Collider collider;

        [SerializeField] private TextMeshProUGUI txtBonus;

        [SerializeField] private float value;

        public float Value => value;

        private void Awake()
        {
            txtBonus.text = $"x{value:0.0}";
        }

        public Vector3 Random()
        {
            return collider.bounds.Random();
        }
    }
}