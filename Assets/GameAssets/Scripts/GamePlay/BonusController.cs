﻿using System.Collections.Generic;
using GameAssets.Scripts.Base;
using UnityEngine;
using System;

namespace GameAssets.Scripts.GamePlay
{
    public class BonusController : BaseSingleton<BonusController>
    {
        [SerializeField] private List<Bonus> bonuses = new List<Bonus>();

        public Bonus Bonus()
        {
            var percent =
                (float) Math.Round((decimal) (Level.Instance.MaxSpermCanCollect() / (float) Player.Instance.ShownSperm),
                    2);

            var bonus = bonuses[0];

            if (percent < bonus.Value)
            {
                return bonus;
            }

            bonus = bonuses[bonuses.Count - 1];

            if (percent > bonus.Value)
            {
                return bonus;
            }

            for (var i = 1; i < bonuses.Count - 2; i++)
            {
                bonus = bonuses[i];

                if (percent >= bonuses[i].Value && percent <= bonuses[i + 1].Value)
                {
                    return bonus;
                }
            }

            return bonus;
        }
    }
}