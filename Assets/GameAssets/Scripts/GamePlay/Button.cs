﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay
{
    [RequireComponent(typeof(EventTrigger))]
    public class Button : MonoBehaviour
    {
        [ShowIf(nameof(otherTarget))] [SerializeField]
        private Transform target;

        [Range(0.85f, 0.95f)] [SerializeField] private float scale = 0.85f;

        [SerializeField] private bool otherTarget;
        [SerializeField] private bool destroySystemButton = true;

        private EventTrigger _eventTrigger;
        private Image _image;

        private Action _onClick;

        private readonly Color32 _whiteColor = new Color32(255, 255, 255, 255);
        private readonly Color32 _greyColor = new Color32(255, 255, 255, 128);

        private Vector3 _holdScale;
        private Vector3 _originScale;

        private static bool _processing;
        private bool _interactable = true;

        private void OnValidate()
        {
            if (_image == null)
            {
                _image = GetComponent<Image>();
            }

            if (!destroySystemButton)
            {
                return;
            }

            var btn = GetComponent<UnityEngine.UI.Button>();

            if (btn != null)
            {
                DestroyImmediate(btn, true);
            }
        }

        private void Awake()
        {
            _eventTrigger = GetComponent<EventTrigger>();
            _holdScale = new Vector3(scale, scale, scale);
            _originScale = transform.localScale;

            var entry1 = new EventTrigger.Entry
            {
                eventID = EventTriggerType.PointerDown
            };

            var entry2 = new EventTrigger.Entry
            {
                eventID = EventTriggerType.PointerUp
            };

            var entry3 = new EventTrigger.Entry
            {
                eventID = EventTriggerType.PointerExit
            };

            entry1.callback.AddListener(_ => Scale(_holdScale));
            entry2.callback.AddListener(_ => ToOriginScale(false));
            entry3.callback.AddListener(_ => ToOriginScale(true));

            _eventTrigger.triggers.AddRange(new[]
            {
                entry1,
                entry2,
                entry3
            });
        }

        private void OnDestroy()
        {
            transform.DOKill();
        }

        private void Scale(Vector3 scale)
        {
            if (_processing || !_interactable)
            {
                return;
            }

            _processing = true;

            var t = otherTarget ? target : transform;

            t.DOScale(scale, 0.1f);
        }

        private void ToOriginScale(bool exit)
        {
            if (!_processing || !_interactable)
            {
                return;
            }

            var t = otherTarget ? target : transform;

            t.DOScale(_originScale - Vector3.one / 10f, 0f)
                .OnComplete(() => t.DOScale(_originScale, 0.25f)
                    .SetEase(Ease.OutBounce)
                    .OnComplete(() =>
                    {
                        if (!exit)
                        {
                            _onClick?.Invoke();
                        }

                        t.localScale = _originScale;
                        _processing = false;
                    }));
        }

        public void AddListener(Action onClick)
        {
            _onClick = onClick;
        }

        public void RemoveListener()
        {
            _onClick = null;
        }

        public void SetInteractable(bool value, bool changeColor = true)
        {
            if (_image != null && changeColor)
            {
                _image.color = value ? _whiteColor : _greyColor;
            }

            _interactable = value;
        }
    }
}