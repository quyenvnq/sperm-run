using System;
using DG.Tweening;
using GameAssets.Scripts.Ex;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Hand : MonoBehaviour
    {
        [SerializeField] private Transform rotate;
        [SerializeField] private Transform exitPoint;
        [SerializeField] private Vector3 endMove = new Vector3(1f, 0f, 0f);

        [SerializeField] private float durationMove = 0.25f;
        [SerializeField] private float durationRotate = 0.25f;

        private void OnValidate()
        {
            transform.Children(t =>
            {
                Debug.Log(t.name);
                return false;
            });
        }

        public void Leave(Obstacle obstacle)
        {
            obstacle.MoveToHand(transform);
            
            transform.DOLocalMove(endMove, durationMove)
                .OnComplete(() =>
                {
                    rotate.SetParent(transform.parent);
                    rotate.DOLocalRotate(Vector3.up * 180f, durationRotate);
                });
        }
    }
}