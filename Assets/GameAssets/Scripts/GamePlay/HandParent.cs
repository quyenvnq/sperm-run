using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class HandParent : MonoBehaviour
    {
        [SerializeField] private Hand hand1;
        [SerializeField] private Hand hand2;

        public void Leave(Obstacle obs1, Obstacle obs2)
        {
            hand1.Leave(obs1);
            hand2.Leave(obs2);
        }
    }
}