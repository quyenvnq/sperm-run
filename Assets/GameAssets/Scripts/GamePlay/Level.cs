﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    [Serializable]
    public class WindowGroup
    {
        public List<Window> windows = new List<Window>();

        public bool group;
    }

    [ExecuteInEditMode]
    public class Level : BaseSingleton<Level>
    {
        [SerializeField] private List<WindowGroup> windowGroups = new List<WindowGroup>();

        [SerializeField] private Model.Ability spermAbility;

        [Button]
        public override void Initialize()
        {
            var xes = GetComponentsInChildren<Window>();

            windowGroups.Clear();

            for (var i = 0; i < xes.Length; i++)
            {
                xes[i].name = $"Window {i}";

                windowGroups.Add(new WindowGroup
                {
                    windows = new List<Window>
                    {
                        xes[i]
                    },
                    group = false
                });
            }
        }

        public int MaxSpermCanCollect()
        {
            var total = UserDataManager.Instance.userDataSave.abilities
                .Where(x => x.id == spermAbility.id)
                .Select(x => x.level).FirstOrDefault();

            foreach (var x in windowGroups)
            {
                if (x.windows.Count <= 0)
                {
                    continue;
                }

                Window window = null;
                Window w1;

                if (x.group)
                {
                    w1 = x.windows[0];

                    var w2 = x.windows[1];

                    if (w1 != null && w2 != null)
                    {
                        window = w1.WindowType <= w2.WindowType ? w1 : w2;
                    }
                }
                else
                {
                    w1 = x.windows[0];

                    if (w1 != null)
                    {
                        switch (w1.WindowType)
                        {
                            case WindowType.Multiple:
                            case WindowType.Add:
                                window = x.windows[0];
                                break;
                        }
                    }
                }

                if (window != null)
                {
                    Result(window, ref total);
                }
            }

            return total;
        }

        private static void Result(Window window, ref int total)
        {
            switch (window.WindowType)
            {
                case WindowType.Multiple:
                    total *= window.Value;
                    break;

                case WindowType.Add:
                    total += window.Value;
                    break;

                case WindowType.Minus:
                    total -= window.Value;
                    break;

                case WindowType.Divide:
                    if (window.Value > 0)
                    {
                        total /= window.Value;
                    }

                    break;
            }
        }
    }
}