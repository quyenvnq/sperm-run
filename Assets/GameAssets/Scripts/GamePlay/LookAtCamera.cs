﻿using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class LookAtCamera : MonoBehaviour
    {
        [SerializeField] private Vector3 rotationAfterLook;

        private Transform _camera;

        private void Awake()
        {
            if (Camera.main is { })
            {
                _camera = Camera.main.transform;
            }
        }

        private void Update()
        {
            transform.LookAt(_camera);

            transform.localRotation = Quaternion.Euler(rotationAfterLook);
        }
    }
}