﻿using GameAssets.Scripts.Ex;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GamePlay
{
    public class Obstacle : MonoBehaviour
    {
        public SphereCollider sphereCollider;
        private HingeJoint _hingeJoint;
        private Rigidbody _rb;

        public int value;

        private void OnValidate()
        {
            value = Random.Range(1, 3);
            sphereCollider = GetComponent<SphereCollider>();
        }

        public void MoveToPancake(ref Rigidbody prevRb, int spring, float damper, Vector3 offset, Transform parent)
        {
            var t = transform;

            t.SetParent(prevRb.transform);

            t.localPosition = offset;
            transform.localRotation = Quaternion.Euler(Vector3.zero);

            _rb = gameObject.AddComponent<Rigidbody>();
            _hingeJoint = gameObject.AddComponent<HingeJoint>();

            _rb.mass = 0f;
            _hingeJoint.connectedBody = prevRb;
            _hingeJoint.autoConfigureConnectedAnchor = true;
            _hingeJoint.anchor = Vector3.zero;
            _hingeJoint.axis = Vector3.right + Vector3.up;
            _hingeJoint.useSpring = true;
            _hingeJoint.useLimits = true;
            prevRb = _rb;

            _hingeJoint.spring = new JointSpring
            {
                spring = spring,
                damper = damper,
                targetPosition = 0f
            };

            _hingeJoint.limits = new JointLimits
            {
                min = 0f,
                max = 0f,
                bounciness = 1,
                bounceMinVelocity = 1000,
                contactDistance = 0
            };

            transform.SetParent(parent);
            _rb.ResetInertia();
            Destroy(sphereCollider);
        }

        public void MoveToCoffee(ref Rigidbody prevRb, int spring, float damper, Vector3 offset, Transform parent,
            float minX, float maxX)
        {
            MoveToPancake(ref prevRb, spring, damper, offset, parent);

            var position = transform.localPosition;
            var x = Mathf.Clamp(position.x, minX, maxX);

            position = new Vector3(x, position.y, position.z);
            transform.localPosition = position;
        }

        public void MoveRandom(Vector3 end)
        {
            Destroy(_hingeJoint);
            Destroy(_rb);

            var rnd = Random.insideUnitSphere * 2f;

            rnd.y = 0f;
            transform.position = end + rnd;
            transform.localRotation = Quaternion.Euler(Vector3.zero);
        }

        public void MoveToHand(Transform parent)
        {
            var t = transform;

            t.SetParent(parent);

            t.localRotation = Quaternion.Euler(Vector3.up * 45f);
            t.localPosition = Vector3.zero;
            t.localScale = new Vector3(1f, 2f, 2f);

            Destroy(_hingeJoint);
            Destroy(_rb);
        }
    }
}