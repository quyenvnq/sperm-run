using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Dreamteck.Splines;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Other
{
    public class PlayerCoffee : BaseCharacter
    {
        [SerializeField] private List<Transform> tables = new List<Transform>();
        [SerializeField] private BaseHandle baseHandle;
        [SerializeField] public SplineFollower splineFollower;

        [SerializeField] private Vector3 offset = new Vector3(0f, 0.1f, 0f);
        [SerializeField] private Vector3 punchScale = Vector3.one;

        [SerializeField] private int spring = 100;

        [SerializeField] private float damper = 250000f;
        [SerializeField] private float minX;
        [SerializeField] private float maxX;
        [SerializeField] private float durationPunchScale = 2f;
        [SerializeField] private float moveForwardSpeed = 300f;

        private readonly List<Obstacle> _obstacles = new List<Obstacle>();

        private Rigidbody _prevRb;
        private Transform _parent;

#region MonoBehaviour

        protected override void Awake()
        {
            base.Awake();
            splineFollower.onNode += OnNodePassed;

            if (Camera.main is { })
            {
                Camera.main.transform.SetParent(transform.parent);
            }

            _prevRb = rb;
            _parent = transform.parent;

            GameEvent.OnStartGame += StartGame;
        }

        public override void InnerUpdate()
        {
            if (baseHandle.Delta == Vector3.zero)
            {
                rb.ResetInertia();
                return;
            }

            var t = transform;
            var move = new Vector3(baseHandle.Delta.x, 0f, 0f);

            t.Translate(move * (stat.moveSpeed * Time.deltaTime));

            var tp = t.localPosition;
            var x = Mathf.Clamp(tp.x, minX, maxX);

            tp = new Vector3(x, tp.y, tp.z);
            transform.localPosition = tp;
        }

        public override void PreInnerFixedUpdate()
        {
            base.PreInnerFixedUpdate();

            if (GameManager.Instance.IsGameState(GameState.MoveToEnd))
            {
                transform.parent.Translate(transform.forward * moveForwardSpeed * Time.fixedDeltaTime);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            GameEvent.OnStartGame -= StartGame;
        }

#endregion

#region Collision

        private async void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Obstacle"))
            {
                var obstacle = other.GetComponent<Obstacle>();

                if (obstacle == null)
                {
                    return;
                }

                _obstacles.Add(obstacle);
                obstacle.MoveToCoffee(ref _prevRb, spring, damper, offset, _parent, minX, maxX);

                for (var i = _obstacles.Count - 1; i >= 0; i--)
                {
                    _obstacles[i].transform.DOPunchScale(punchScale, durationPunchScale, 5, 0.5f);

                    await UniTask.Delay(TimeSpan.FromSeconds(durationPunchScale / 2f));
                }
            }
            else if (other.CompareTag("Hand"))
            {
                var hand = other.GetComponent<HandParent>();

                if (hand == null)
                {
                    return;
                }

                if (_obstacles.Count <= 1)
                {
                    GameEvent.DoLose();
                    return;
                }

                var obs = _obstacles[_obstacles.Count - 1];
                var obs2 = _obstacles[_obstacles.Count - 2];

                _obstacles.Remove(obs);
                _obstacles.Remove(obs2);
                hand.Leave(obs, obs2);
            }
            else if (other.CompareTag("Finish"))
            {
                var count = _obstacles.Count < tables.Count ? _obstacles.Count : tables.Count;

                for (var i = 0; i < count; i++)
                {
                    var x = _obstacles[_obstacles.Count - 1];

                    x.MoveToHand(tables[i]);
                    _obstacles.Remove(x);
                }

                foreach (var x in _obstacles)
                {
                    Destroy(x.gameObject);
                }
                
                _obstacles.Clear();
            }
            else if (other.CompareTag("Respawn"))
            {
                GameManager.Instance.SetGameState(GameState.Pause);
            }
        }

#endregion

#region Public

        private void StartGame()
        {
            splineFollower.follow = true;
        }

#endregion

#region Private

        private void OnNodePassed(IReadOnlyList<SplineTracer.NodeConnection> passed)
        {
            if (passed.Count <= 0 || !string.Equals(passed[0].node.name, "Final Node",
                    StringComparison.CurrentCultureIgnoreCase))
            {
                return;
            }

            splineFollower.follow = false;

            GameManager.Instance.SetGameState(GameState.MoveToEnd);
        }

#endregion

#region Protected

#endregion
    }
}