using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Dreamteck.Splines;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Other
{
    public class PlayerPancake : BaseCharacter
    {
        [SerializeField] private BaseHandle baseHandle;
        [SerializeField] public SplineFollower splineFollower;
        [SerializeField] private Transform end;

        [SerializeField] private Vector3 offset = new Vector3(0f, 0.1f, 0f);

        [SerializeField] private int spring = 100;

        [SerializeField] private float damper = 250000f;
        [SerializeField] private float minX;
        [SerializeField] private float maxX;

        private readonly List<Obstacle> _obstacles = new List<Obstacle>();

        private Rigidbody _prevRb;
        private Transform _parent;

#region MonoBehaviour

        protected override void Awake()
        {
            base.Awake();

            splineFollower.onNode += OnNodePassed;

            if (Camera.main is { })
            {
                Camera.main.transform.SetParent(transform.parent);
            }

            _prevRb = rb;
            _parent = transform.parent;
            
            GameEvent.OnStartGame += StartGame;
        }

        public override void InnerUpdate()
        {
            if (baseHandle.Delta == Vector3.zero)
            {
                rb.ResetInertia();
                return;
            }

            var t = transform;
            var move = new Vector3(baseHandle.Delta.x, 0f, 0f);

            t.Translate(move * (stat.moveSpeed * Time.deltaTime));

            var tp = t.localPosition;
            var x = Mathf.Clamp(tp.x, minX, maxX);

            tp = new Vector3(x, tp.y, tp.z);
            transform.localPosition = tp;
        }

#endregion

#region Collision

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Obstacle"))
            {
                return;
            }

            var obstacle = other.GetComponent<Obstacle>();

            if (obstacle == null)
            {
                return;
            }

            _obstacles.Add(obstacle);
            obstacle.MoveToPancake(ref _prevRb, spring, damper, offset, _parent);
        }

#endregion

#region Public

        private void StartGame()
        {
            splineFollower.follow = true;
        }

#endregion

#region Private

        private async void OnNodePassed(IReadOnlyList<SplineTracer.NodeConnection> passed)
        {
            if (passed.Count <= 0 ||
                !string.Equals(passed[0].node.name, "Final Node", StringComparison.CurrentCultureIgnoreCase))
            {
                return;
            }

            GameManager.Instance.SetGameState(GameState.Pause);

            await UniTask.Delay(TimeSpan.FromSeconds(1f));

            transform.DOMove(end.position, 1f)
                .OnComplete(() =>
                {
                    foreach (var x in _obstacles)
                    {
                        x.MoveRandom(end.position);
                    }

                    GameEvent.DoWin();
                });
        }

#endregion

#region Protected

#endregion
    }
}