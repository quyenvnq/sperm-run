using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Base.Interface;
using GameAssets.Scripts.Ex;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay
{
    [Serializable]
    public class PageData
    {
        public Dictionary<string, Transform> slots = new Dictionary<string, Transform>();

        public Transform page;
    }

    public class Page : MonoBehaviour, IInitialize
    {
        [Header("[TRANSFORM]")] [SerializeField]
        private Transform pageSlotParent;

        [Header("[GAME OBJECT]")] [SerializeField]
        private GameObject pageSlotPrefab;

        [HideIf(nameof(showSlotAsPage))] [SerializeField]
        private GameObject slotPrefab;

        [Header("[BUTTON PREVIOUS]")] [SerializeField]
        private Button btnPreviousPage;

        [ShowIf(nameof(showDisableSpriteBtnPrev))] [SerializeField]
        private Image imgPrevPage;

        [ShowIf(nameof(showDisableSpriteBtnPrev))] [SerializeField]
        private Sprite icDisablePreviousPage;

        [Header("[BUTTON NEXT]")] [SerializeField]
        private Button btnNextPage;

        [ShowIf(nameof(showDisableSpriteBtnNext))] [SerializeField]
        private Image imgNextPage;

        [ShowIf(nameof(showDisableSpriteBtnNext))] [SerializeField]
        private Sprite icDisableNextPage;

        [ShowIf(nameof(showPageNumber))] [Header("[TEXT]")] [SerializeField]
        private TextMeshProUGUI txtPageNumber;

        [Header("[INT]")] [HideIf(nameof(showSlotAsPage))] [SerializeField]
        private int slotInPage = 5;

        [Header("[BOOL]")] [HideIf(nameof(showSlotAsPage))] [SerializeField]
        private bool showPageNumber;

        [HideIf(nameof(showSlotAsPage))] [SerializeField]
        private bool showSlotEmpty;

        [Tooltip("If showSlotAsPage is true then add, remove, update will dont working")] [SerializeField]
        private bool showSlotAsPage;

        [Tooltip("If showDisableSpriteBtnNext is true then instead of hiding it will change it to an image")]
        [SerializeField]
        private bool showDisableSpriteBtnNext;

        [Tooltip("If showDisableSpriteBtnPrev is true then instead of hiding it will change it to an image")]
        [SerializeField]
        private bool showDisableSpriteBtnPrev;

        private const string KEY_EMPTY = "EMPTY";

        private readonly List<PageData> _pages = new List<PageData>();

        private Func<Transform, string> _createSlot;

        private Sprite _icNextPage;
        private Sprite _icPrevPage;

        private int _totalItem;
        private int _indexPage;
        private int _maxIndexPageMoved;

        #region MonoBehaviour

        private void Start()
        {
            if (_icPrevPage == null && imgPrevPage != null)
            {
                _icPrevPage = imgPrevPage.sprite;
            }

            if (_icNextPage == null && imgNextPage != null)
            {
                _icNextPage = imgNextPage.sprite;
            }

            btnPreviousPage.AddListener(() =>
            {
                if (_indexPage - 1 < 0)
                {
                    return;
                }

                _pages[_indexPage - 1].page.gameObject.SetActive(true);
                _pages[_indexPage].page.gameObject.SetActive(false);

                _indexPage--;

                if (_indexPage == 0)
                {
                    SetPrevPage(false);
                }

                SetNextPage(true);
                ShowPageNumber();
            });

            btnNextPage.AddListener(() =>
            {
                if (_indexPage + 1 >= _totalItem)
                {
                    return;
                }

                if (_indexPage + 1 <= _totalItem / slotInPage)
                {
                    _indexPage++;
                }

                if (_indexPage > _maxIndexPageMoved && _maxIndexPageMoved < _totalItem / slotInPage)
                {
                    _maxIndexPageMoved = _indexPage;

                    DrawPage();
                }

                if (_indexPage * slotInPage + (showSlotAsPage ? 1 : _pages[_indexPage].page.childCount) >= _totalItem)
                {
                    SetNextPage(false);
                }

                _pages[_indexPage].page.gameObject.SetActive(true);
                _pages[_indexPage - 1].page.gameObject.SetActive(false);

                SetPrevPage(true);
                ShowPageNumber();
            });
        }

        #endregion

        #region Public

        public void Initialize()
        {
            _maxIndexPageMoved = 0;
            _indexPage = 0;

            if (showSlotAsPage)
            {
                slotInPage = 1;
            }

            RemovePage();
            SetPrevPage(false);
        }

        public void SetTotalItem(int total)
        {
            Start();

            _totalItem = total;

            if (_totalItem <= 0)
            {
                DisableButton();
            }
        }

        /// <summary>
        /// Create new slot in inventory, must be return id item
        /// </summary>
        /// <param name="createSlot"></param>
        public void OnCreateSlot(Func<Transform, string> createSlot)
        {
            _createSlot = createSlot;

            Initialize();
            DrawPage();
        }

        public void OnUpdateSlot(string id, Action<Transform> updateSlot)
        {
            if (showSlotAsPage)
            {
                return;
            }

            foreach (var x in _pages)
            {
                if (x.slots.TryGetValue(id, out var y))
                {
                    updateSlot(y);
                }
            }
        }

        public void OnRemoveSlot(string id, Action<Transform> removeSlot)
        {
            if (!showSlotAsPage)
            {
                OnUpdateSlot(id, removeSlot);
            }
        }

        /// <summary>
        /// Add new item to inventory
        /// </summary>
        /// <param name="addSlot">Add new item</param>
        /// <param name="currentPageNotFinalPage">If the current page is not the last page</param>
        public void OnAddSlot(Func<Transform, string> addSlot, Func<bool> currentPageNotFinalPage = null)
        {
            void ShowNextPage()
            {
                _totalItem++;

                if (_totalItem > (_indexPage + 1) * slotInPage && !btnNextPage.gameObject.activeInHierarchy)
                {
                    SetNextPage(true);
                }
            }

            if (showSlotAsPage)
            {
                CreatePage();

                if (!btnNextPage.gameObject.activeInHierarchy)
                {
                    SetNextPage(true);
                }
            }
            else
            {
                if (showSlotEmpty)
                {
                    var x = _pages[_pages.Count - 1];

                    for (var j = 0; j < x.slots.Count; j++)
                    {
                        var y = x.slots.ElementAt(j);

                        if (!y.Key.StartsWith(KEY_EMPTY))
                        {
                            continue;
                        }

                        var value = addSlot?.Invoke(y.Value);

                        if (!x.slots.Remove(y.Key))
                        {
                            continue;
                        }

                        x.slots.Insert(j, value, y.Value);

                        ShowNextPage();
                        return;
                    }

                    if (currentPageNotFinalPage != null && currentPageNotFinalPage())
                    {
                        ShowNextPage();
                    }
                }
                else
                {
                    if (_pages.Count < 1)
                    {
                        CreatePage();
                        DisableButton();
                    }

                    var count = _pages[_indexPage].page.childCount;

                    if (count + 1 > slotInPage && _indexPage < _totalItem / slotInPage)
                    {
                        if (currentPageNotFinalPage != null && currentPageNotFinalPage())
                        {
                            ShowNextPage();
                        }
                    }
                    else
                    {
                        var slot = DrawSlot(count + 1);

                        _pages[_indexPage].slots.Add(addSlot?.Invoke(slot)!, slot);

                        ShowNextPage();
                    }
                }
            }
        }

        #endregion

        #region Private

        private void DisableButton()
        {
            SetNextPage(false);
            SetPrevPage(false);
        }

        private void SetPrevPage(bool value)
        {
            if (value)
            {
                if (showDisableSpriteBtnPrev)
                {
                    imgPrevPage.sprite = _icPrevPage;
                }

                btnPreviousPage.SetInteractable(true, false);
                btnPreviousPage.gameObject.SetActive(true);
            }
            else
            {
                if (showDisableSpriteBtnPrev)
                {
                    imgPrevPage.sprite = icDisablePreviousPage;

                    btnPreviousPage.SetInteractable(false, false);
                    btnPreviousPage.gameObject.SetActive(true);
                }
                else
                {
                    btnPreviousPage.SetInteractable(false, false);
                    btnPreviousPage.gameObject.SetActive(false);
                }
            }
        }

        private void SetNextPage(bool value)
        {
            if (value)
            {
                if (showDisableSpriteBtnNext)
                {
                    imgNextPage.sprite = _icNextPage;
                }

                btnNextPage.SetInteractable(true, false);
                btnNextPage.gameObject.SetActive(true);
            }
            else
            {
                if (showDisableSpriteBtnNext)
                {
                    imgNextPage.sprite = icDisableNextPage;

                    btnNextPage.SetInteractable(false, false);
                    btnNextPage.gameObject.SetActive(true);
                }
                else
                {
                    btnNextPage.SetInteractable(false);
                    btnNextPage.gameObject.SetActive(false);
                }
            }
        }

        private void ShowPageNumber()
        {
            if (showPageNumber && txtPageNumber != null)
            {
                txtPageNumber.text = $"{_indexPage + 1}/{_totalItem / slotInPage}";
            }
        }

        private Transform DrawSlot(int index)
        {
            var slot = SpawnerEx.CreateUISpawner(_pages[_indexPage].page, slotPrefab);

            slot.name = $"Slot {index}";
            return slot;
        }

        private Transform CreatePage()
        {
            var page = SpawnerEx.CreateUISpawner(pageSlotParent, pageSlotPrefab);

            page.name = $"Page {_indexPage}";

            _pages.Add(new PageData
            {
                page = page,
                slots = new Dictionary<string, Transform>()
            });

            return page;
        }

        private void DrawPage()
        {
            void LocalPageNumber()
            {
                SetNextPage(false);
                ShowPageNumber();
            }

            var page = CreatePage();

            if (showSlotAsPage)
            {
                _createSlot?.Invoke(page);

                SetNextPage(_indexPage + 1 < _totalItem);
                return;
            }

            var index = _indexPage * slotInPage;
            var slotEmpty = false;

            for (var x = index; x < index + slotInPage; x++)
            {
                if (_pages[_indexPage].page.childCount < slotInPage)
                {
                    if (index + _pages[_indexPage].page.childCount < _totalItem)
                    {
                        var slot = DrawSlot(x);

                        if (_createSlot == null)
                        {
                            continue;
                        }

                        var key = _createSlot(slot);

                        if (!_pages[_indexPage].slots.ContainsKey(key))
                        {
                            _pages[_indexPage].slots.Add(key, slot);
                        }
                    }
                    else
                    {
                        if (showSlotEmpty)
                        {
                            slotEmpty = true;

                            _pages[_indexPage].slots.Add($"{KEY_EMPTY} {Guid.NewGuid()}", DrawSlot(x));
                        }
                        else
                        {
                            LocalPageNumber();
                            return;
                        }
                    }
                }
                else
                {
                    LocalPageNumber();
                    return;
                }
            }

            if (slotEmpty)
            {
                LocalPageNumber();
            }

            if (_indexPage * slotInPage < _totalItem)
            {
                SetNextPage(true);
            }
        }

        private void RemovePage()
        {
            foreach (var x in _pages.Where(x => x.page != null))
            {
                SpawnerEx.DestroySpawner(x.page);
            }

            _pages.Clear();
        }

        #endregion

        #region Protected

        #endregion
    }
}