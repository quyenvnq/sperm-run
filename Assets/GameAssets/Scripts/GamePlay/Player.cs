﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Dreamteck.Splines;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.UI.UIGameStart;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GamePlay
{
    public class Player : BaseCharacter
    {
        [SerializeField] private BaseHandle baseHandle;
        [SerializeField] private SplineFollower splineFollower;
        [SerializeField] private SpermController spermController;
        [SerializeField] private Model.Ability spermAbility;

        [SerializeField] private TextMeshProUGUI txtSpermValue;

        [SerializeField] private GameObject uiSperm;

        [SerializeField] private float spermPrice = 0.5f;
        [SerializeField] private float duration = 5f;
        [SerializeField] private float minX;
        [SerializeField] private float maxX;

        public List<Sperm> Sperms { get; } = new List<Sperm>();
        public static Player Instance { get; private set; }

        public int ShownSperm { get; private set; }

        public float TotalGold { get; private set; }

        private UIGameStart _uiGameStart;

        private bool _changeSperm;

        private int _collision;

        #region MonoBehaviour

        protected override void Awake()
        {
            base.Awake();

            if (Instance == null)
            {
                Instance = this;
            }

            _uiGameStart = UIManager.Instance.GetUI<UIGameStart>();
            ShownSperm = Sperms.Count;

            splineFollower.onNode += OnNodePassed;

            if (Camera.main is { })
            {
                Camera.main.transform.SetParent(transform.parent);
            }
            
            GameEvent.OnStartGame += StartGame;
        }

        private void Start()
        {
            foreach (var x in UserDataManager.Instance.userDataSave.abilities
                .Where(x => x.id == spermAbility.id))
            {
                spermController.AddSperm(x.Effect());
                break;
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            GameEvent.OnStartGame -= StartGame;
        }

        public override void InnerUpdate()
        {
            if (baseHandle.Delta == Vector3.zero)
            {
                rb.ResetInertia();
                return;
            }

            var t = transform;
            var move = new Vector3(baseHandle.Delta.x, 0f, 0f);

            t.Translate(move * (stat.moveSpeed * Time.deltaTime));

            var tp = t.localPosition;
            var x = Mathf.Clamp(tp.x, minX, maxX);

            tp = new Vector3(x, tp.y, tp.z);
            transform.localPosition = tp;
        }

        #endregion

        #region Collision

        private void OnTriggerEnter(Collider other)
        {
            CheckCollision(other);
        }

        #endregion

        #region Public

        private void StartGame()
        {
            splineFollower.follow = true;
        }

        public void UpdatePregnantFill(Sperm sperm)
        {
            _collision++;

            var percent = _collision / LevelManager.Instance.Level.target;

            _uiGameStart.UpdatePregnantFill(percent);

            if (percent * 100 <= 100f)
            {
                Pregnancy.Instance.UpdateFill(percent * 100);
            }

            if (Sperms.IndexOf(sperm) != Sperms.Count - 1)
            {
                return;
            }

            splineFollower.follow = false;

            if (_collision >= LevelManager.Instance.Level.target)
            {
                Pregnancy.Instance.Fire();
            }
            else
            {
                GameEvent.DoLose();
            }
        }

        public void UpdateSpermValue(Vector3 spermScale, int maxShown, int bonus)
        {
            if (ShownSperm >= maxShown)
            {
                ShownSperm += bonus;

                var rnd = Random.Range(10, 15);
                var lst = new List<Sperm>();

                lst.AddRange(Sperms);

                for (var i = 0; i < rnd; i++)
                {
                    var index = Random.Range(0, lst.Count);

                    if (lst[index] == null || index >= lst.Count - 1)
                    {
                        continue;
                    }

                    lst[index].transform.DOScale(spermScale, 0.15f).From(0f);
                    lst.RemoveAt(index);
                }
            }
            else
            {
                ShownSperm = Sperms.Count;

                if (ShownSperm >= maxShown)
                {
                    ShownSperm += bonus;
                }
            }

            txtSpermValue.NumberCounter(ShownSperm, 0.25f);

            if (Sperms.Count > 0)
            {
                return;
            }

            splineFollower.follow = false;

            uiSperm.gameObject.SetActive(false);

            GameEvent.DoLose();
        }

        public void BuySperm(int value)
        {
            spermController.AddSperm(value);
        }

        public void BonusMultiple(float value)
        {
            TotalGold = ShownSperm * value * spermPrice;
        }

        #endregion

        #region Private

        private async void CheckCollision(Component other)
        {
            switch (other.tag)
            {
                case "Window" when !_changeSperm:
                    var window = other.GetComponent<Window>();

                    if (window == null)
                    {
                        return;
                    }

                    _changeSperm = true;

                    switch (window.WindowType)
                    {
                        case WindowType.Add:
                            AudioManager.Instance.PlaySound("blue_gate");

                            spermController.AddSperm(window.Value);
                            break;

                        case WindowType.Minus:
                            AudioManager.Instance.PlaySound("red_gate");

                            spermController.RemoveSperm(window.Value);
                            break;

                        case WindowType.Divide:
                            AudioManager.Instance.PlaySound("red_gate");

                            if (window.Value > 1)
                            {
                                spermController.RemoveSperm(ShownSperm - ShownSperm / window.Value);
                            }

                            break;

                        case WindowType.Multiple:
                            AudioManager.Instance.PlaySound("blue_gate");

                            if (window.Value > 1)
                            {
                                spermController.AddSperm(ShownSperm * (window.Value - 1));
                            }

                            break;
                    }

                    SpawnerEx.DestroySpawner(other.transform);

                    await UniTask.Delay(TimeSpan.FromSeconds(0.25f));

                    _changeSperm = false;
                    break;

                case "Obstacle":
                    spermController.RemoveSperm(other.GetComponent<Obstacle>().value);

                    SpawnerEx.DestroySpawner(other.transform);
                    break;
            }
        }

        private async void OnNodePassed(IReadOnlyList<SplineTracer.NodeConnection> passed)
        {
            if (passed.Count <= 0
                || !string.Equals(passed[0].node.name, "Final Node",
                    StringComparison.CurrentCultureIgnoreCase))
            {
                return;
            }

            GameManager.Instance.SetGameState(GameState.Pause);

            uiSperm.SetActive(false);

            foreach (var x in Sperms)
            {
                x.DoMove(Pregnancy.Instance.transform, duration);

                await UniTask.Delay(TimeSpan.FromSeconds(0.1f));
            }
        }

        #endregion

        #region Protected

        #endregion
    }
}