using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GamePlay
{
    public class Pregnancy : BaseSingleton<Pregnancy>
    {
        [SerializeField] private List<GameObject> babies = new List<GameObject>();

        [HideIf(nameof(egg))] [SerializeField] private SkinnedMeshRenderer blendshape;

        [ShowIf(nameof(egg))] [SerializeField] private Transform eggModel;
        [SerializeField] private Transform exitPoint;

        [SerializeField] private float duration = 2f;

        [SerializeField] private bool egg;

        private Vector3 _scale;

        protected override void Awake()
        {
            base.Awake();

            if (!egg)
            {
                return;
            }

            _scale = eggModel.localScale;
            eggModel.localScale = Vector3.one;
        }

        public async void Fire()
        {
            SpawnerEx.CreateSpawner(exitPoint.position, null, babies[Random.Range(0, babies.Count)])
                .GetComponent<Baby>()
                .Fire(BonusController.Instance.Bonus().Random());

            await UniTask.Delay(TimeSpan.FromSeconds(1f));

            gameObject.SetActive(false);
        }

        public void UpdateFill(float value)
        {
            if (!egg)
            {
                DOTween.To(() => blendshape.GetBlendShapeWeight(0),
                    x => blendshape.SetBlendShapeWeight(0, x),
                    value, duration);
            }
            else
            {
                var scale = _scale;

                if (value < _scale.x)
                {
                    scale = new Vector3(value, value, value);
                }

                eggModel.DOScale(scale * _scale.x / 100f, duration);
            }
        }
    }
}