﻿using DG.Tweening;
using GameAssets.Scripts.Ex;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Sperm : MonoBehaviour
    {
        #region MonoBehaviour

        #endregion

        #region Collider

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Pregnancy"))
            {
                return;
            }

            var t = transform;

            t.DOKill();

            SpawnerEx.DestroySpawner(t);
            Player.Instance.UpdatePregnantFill(this);
        }

        #endregion

        #region Public

        public void DoMove(Transform target, float duration)
        {
            var position = target.position;
            var t = new Vector3(position.x, transform.position.y, position.z);

            transform.DOMove(t, duration);
        }

        #endregion

        #region Private

        #endregion

        #region Protected

        #endregion
    }
}