﻿using DG.Tweening;
using GameAssets.Scripts.Ex;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class SpermController : MonoBehaviour
    {
        [SerializeField] private Player player;

        [SerializeField] private GameObject spermPrefab;

        [Range(0.15f, 2f)] [SerializeField] private float radius = 1f;

        [Range(100, 500)] [SerializeField] private int maxShownSperm = 100;

        private Vector3 _spermScale;

        private int _orbit;
        private int _orbitSize;
        private int _index = 1;

        private float _singleSpermColliderSizeX;

        #region Mono Behaviour

        private void Awake()
        {
            _singleSpermColliderSizeX = ((BoxCollider) player.collider).size.x;
            _spermScale = spermPrefab.transform.localScale;
        }

        #endregion

        #region Public

        public void AddSperm(int count)
        {
            var temp = count;

            for (var i = 0; i < count; i++)
            {
                if (player.Sperms.Count < maxShownSperm)
                {
                    temp--;

                    var clone = SpawnerEx.CreateSpawner(Vector3.zero, transform, spermPrefab);

                    clone.localPosition = SetSpermLocation();
                    clone.localRotation = Quaternion.Euler(0f, 180f, 0f);

                    ChangeOrbit(true);

                    clone.DOScale(_spermScale, 0.15f).From(0f);
                    player.Sperms.Add(clone.GetComponent<Sperm>());
                }
                else
                {
                    break;
                }
            }

            player.UpdateSpermValue(_spermScale, maxShownSperm, temp);
        }

        public void RemoveSperm(int count)
        {
            var n = count;

            if (player.Sperms.Count <= count)
            {
                n = player.Sperms.Count;
            }

            for (var i = 0; i < n; i++)
            {
                var sperm = Random.Range(0, player.Sperms.Count);

                SpawnerEx.DestroySpawner(player.Sperms[sperm].transform);

                player.Sperms.RemoveAt(sperm);

                ChangeOrbit(false);
            }

            player.UpdateSpermValue(_spermScale, maxShownSperm, -n);
        }

        #endregion

        #region Private

        private Vector3 SetSpermLocation()
        {
            var pos = Vector3.zero;

            if (_orbit == 0)
            {
                return pos;
            }

            pos.y = Mathf.Cos(360f / _orbitSize * Mathf.PI / 180 * _index) * _orbit * radius / 4f;
            pos.x = Mathf.Sin(360f / _orbitSize * Mathf.PI / 180 * _index) * _orbit * radius / 4f;
            _index++;

            return pos;
        }

        private void ChangeOrbit(bool added)
        {
            if (added)
            {
                if (_index <= _orbitSize)
                {
                    return;
                }

                _orbit++;

                AdjustColliderSize(1);

                _orbitSize = CalculateOrbitSize(_orbit);
                _index = 1;
            }
            else
            {
                _index--;

                if (_index != 0)
                {
                    return;
                }

                _orbit--;

                AdjustColliderSize(-1);

                _orbitSize = CalculateOrbitSize(_orbit);
                _index = _orbitSize;
            }
        }

        private static int CalculateOrbitSize(int orbit)
        {
            return Mathf.RoundToInt(2 * Mathf.PI * orbit) + 1;
        }

        private void AdjustColliderSize(int diff)
        {
            if (_orbit <= 1)
            {
                return;
            }

            var box = (BoxCollider) player.collider;
            var offset = 2 * _singleSpermColliderSizeX;
            var radiusCoeff = radius / 100 / _singleSpermColliderSizeX;
            var signedOffset = offset * radiusCoeff * diff;
            var size = box.size;

            size = new Vector3(size.x + signedOffset, size.y + signedOffset, size.z);
            box.size = size;
        }

        #endregion
    }
}