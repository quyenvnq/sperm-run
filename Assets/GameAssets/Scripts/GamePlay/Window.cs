using System;
using GameAssets.Scripts.General;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GamePlay
{
    public class Window : MonoBehaviour
    {
        [SerializeField] private Image imgGradient;

        [SerializeField] private TextMeshProUGUI txtPoint;

        [SerializeField] private WindowType windowType;

        [SerializeField] private int value;

        [SerializeField] private bool move;

        [ShowIf(nameof(move))] [SerializeField]
        private float moveSpeed = 2f;

        [ShowIf(nameof(move))] [SerializeField]
        private float xMin;

        [ShowIf(nameof(move))] [SerializeField]
        private float xMax;

        public WindowType WindowType => windowType;

        public int Value => value;

        private readonly Color32 _blue = new Color32(88, 243, 255, 255);
        private readonly Color32 _red = new Color32(255, 79, 76, 255);

        #region MonoBehaviour

        private void Awake()
        {
            value = Random.Range(1, 4);
            windowType = (WindowType) Random.Range(0, 3);

            switch (windowType)
            {
                case WindowType.Multiple:
                    UpdateGradient(true);
                    UpdatePoint("x");
                    break;

                case WindowType.Add:
                    UpdateGradient(true);
                    UpdatePoint("+");
                    break;

                case WindowType.Minus:
                    UpdateGradient(false);
                    UpdatePoint("-");
                    break;

                case WindowType.Divide:
                    UpdateGradient(false);
                    UpdatePoint("÷");
                    break;
            }
        }

        private void LateUpdate()
        {
            if (!move)
            {
                return;
            }

            transform.Translate(-transform.right * (moveSpeed * Time.deltaTime));

            if (transform.localPosition.x <= xMin && moveSpeed > 0)
            {
                moveSpeed *= -1;
            }
            else if (transform.localPosition.x >= xMax && moveSpeed < 0)
            {
                moveSpeed *= -1;
            }
        }

        #endregion

        #region Collider

        #endregion

        #region Public

        #endregion

        #region Private

        /// <summary>
        /// update gradient
        /// </summary>
        /// <param name="value">true for blue and reverse</param>
        private void UpdateGradient(bool value)
        {
            if (imgGradient != null)
            {
                imgGradient.color = value ? _blue : _red;
            }
        }

        private void UpdatePoint(string type)
        {
            if (txtPoint != null)
            {
                txtPoint.text = $"{type}{value}";
            }
        }

        #endregion

        #region Protected

        #endregion
    }
}