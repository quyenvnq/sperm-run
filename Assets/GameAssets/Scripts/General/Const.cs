﻿namespace GameAssets.Scripts.General
{
    public static class Const
    {
        #region Request

        public const string BASE_URL = "https://api.pandatrx.com";
        public const string BASE_URL_WORLD_TIME = "https://worldtimeapi.org/api/timezone/Asia/Ho_Chi_Minh";
        public const string API_REGISTER = "/def_create_acc";
        public const string API_LOGIN = "/def_get_data_user";

        #endregion

        public const string PATH_USER = "users";
        public const string PATH_CONFIG = "config";
        public const string PATH_CONFIG_DOCUMENT = "data";
        public const string PATH_EXCHANGE_SHOP = "exchange_shop";

        public const string URL_RECHARGE = "https://deposit-gold.web.app/";
        public const string URL_CHARGE = "exchange_shop";

        public const string PATTERN_EMAIL = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
        public const string PATTERN_SPECIAL_CHAR = @"[^A-Za-z0-9]";

        public const string SAVE_DATA = "UserSaveData";
        public const string PASSWORD_HASH = "PASSWORD_HASHQUSDROKNEW";
        public const string SALT_KEY = "SALT_KEYQUSDROKNEW";
        public const string VI_KEY = "VI_KEYQUSDROKNEW";
        public const string HASH = "abcdefghijklmnopqrstuvwxyz0123456789";

        public const string AQUA = "#00ffffff";
        public const string BLACK = "#000000ff";
        public const string BLUE = "#0000ffff";
        public const string BROWN = "#a52a2aff";
        public const string CYAN = "#00ffffff";
        public const string DARK_BLUE = "#0000a0ff";
        public const string GREEN = "#008000ff";
        public const string LIGHT_GREY = "#A0A0A0ff";
        public const string GREY = "#808080ff";
        public const string DARK_GREY = "#404040ff";
        public const string LIGHT_BLUE = "#add8e6ff";
        public const string LIME = "#00ff00ff";
        public const string MAGENTA = "#ff00ffff";
        public const string MAROON = "#800000ff";
        public const string NAVY = "#000080ff";
        public const string OLIVE = "#808000ff";
        public const string ORANGE = "#ffa500ff";
        public const string PURPLE = "#800080ff";
        public const string RED = "#ff0000ff";
        public const string SILVER = "#c0c0c0ff";
        public const string TEAL = "#008080ff";
        public const string WHITE = "#ffffffff";
        public const string YELLOW = "#ffff00ff";

        public const string BOLD = "b";
        public const string ITALIC = "i";
        public const string SIZE = "size";
        public const string COLOR = "color";

        public const double EPSILON_MIN = 2.2250738585072014E-308d;

        public const long LIGHT_DURATION = 20;
        public const long MEDIUM_DURATION = 40;
        public const long HEAVY_DURATION = 80;

        public const int MIN_INTERVAL = 10;
        public const int INTERVAL = 20;
        public const int MAX_INTERVAL = 30;

        public const int LIGHT_AMPLITUDE = 40;
        public const int MEDIUM_AMPLITUDE = 120;
        public const int HEAVY_AMPLITUDE = 255;

        public static string PatternLowerNumber(int minimum)
        {
            return $@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{{{minimum},}}$";
        }

        public static string PatternLowerNumberSpecial(int minimum)
        {
            return $@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{{{minimum},}}$";
        }

        public static string PatternUpperLowerNumber(int minimum)
        {
            return $@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{{{minimum},}}$";
        }

        public static string PatternUpperLowerNumberSpecial(int minimum)
        {
            return $@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{{{minimum},}}$";
        }
    }
}