﻿namespace GameAssets.Scripts.General
{
    public enum VibrationType
    {
        Selection,
        Success,
        Warning,
        Failure,
        LightImpact,
        MediumImpact,
        HeavyImpact
    }

    public enum CharacterState
    {
        IdleOrRespawn = 0,
        Move = 1,
        Attack = 2,
        Death = 3
    }

    public enum GameState
    {
        Lobby,
        Loading,
        Pause,
        Playing,
        Lose,
        Win,
        Draw,
        LostConnected,
        ReConnected,
        MoveToEnd
    }

    public enum FitType
    {
        Uniform,
        Width,
        Height,
        FixedRows,
        FixedColumns,
        FixedBoth
    }

    public enum Alignment
    {
        Horizontal,
        Vertical
    }

    public enum PriorityLevel
    {
        Low,
        Normal,
        High
    }

    public enum ValidatorType
    {
        String,
        Email,
        Password,
        ConfirmPassword
    }

    public enum FormatType
    {
        LowerNumber,
        LowerNumberSpecial,
        UpperLowerNumber,
        UpperLowerNumberSpecial
    }

    public enum AngleDirection
    {
        None,
        Up,
        Down,
        Left,
        Right
    }

    public enum AnimationType
    {
        Idle = 0,
        Move = 1,
        Run = 2,
        Jump = 3,
        Fall = 4,
        Attack = 5,
        Death = 6
    }

    public enum FilterType
    {
    }

    public enum WindowType
    {
        Multiple = 0,
        Add = 1,
        Minus = 2,
        Divide = 3
    }
}