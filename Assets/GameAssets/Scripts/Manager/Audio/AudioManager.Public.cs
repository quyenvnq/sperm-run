﻿namespace GameAssets.Scripts.Manager.Audio
{
    public partial class AudioManager
    {
        public async void PlaySound(string clipName, int loopTimes = 1)
        {
            var clip = GetClip(clipName);

            if (clip == null)
            {
                return;
            }

            if (audioSound.isPlaying)
            {
                audioSound.Stop();
            }

            if (loopTimes < 2)
            {
                audioSound.PlayOneShot(clip);
            }
            else
            {
                _loop = 0;
                audioSound.clip = clip;

                await Loop(clip, loopTimes);
            }
        }

        public void PlayMusic(string clipName)
        {
            if (audioMusic.clip != null && clipName.Equals(audioMusic.clip.name))
            {
                return;
            }

            var clip = GetClip(clipName);

            if (clip == null)
            {
                return;
            }

            if (audioMusic.isPlaying)
            {
                audioMusic.Stop();
            }

            audioMusic.clip = clip;

            audioMusic.Play();
        }

        public void PlayVibrate()
        {
            if (UserDataManager.Instance.userDataSave.vibration)
            {
                AndroidVibration.Vibrate();
            }
        }

        public void ChangeSoundVolume(float volume)
        {
            audioSound.volume = volume;

            if (soundAsMusic)
            {
                audioMusic.volume = volume;
            }

            UserDataManager.Instance.SaveSoundVolume(volume);

            UpdateImageSound();

            if (volume <= 0f)
            {
                audioSound.Stop();

                if (soundAsMusic)
                {
                    audioMusic.Stop();
                }
            }
            else
            {
                if (soundAsMusic && !audioMusic.isPlaying)
                {
                    audioMusic.Play();
                }
            }
        }

        public void ChangeMusicVolume(float volume)
        {
            audioMusic.volume = volume;

            UserDataManager.Instance.SaveMusicVolume(volume);

            ChangeImgMusic();

            if (volume <= 0f)
            {
                audioMusic.Stop();
            }
            else
            {
                if (!audioMusic.isPlaying)
                {
                    audioMusic.Play();
                }
            }
        }

        public void Initiation()
        {
            if (btnSound != null)
            {
                UpdateImageSound();

                if (useSlider)
                {
                    sliderSound.value = UserDataManager.Instance.userDataSave.soundVolume;
                }

                btnSound.onClick.RemoveAllListeners();

                btnSound.onClick.AddListener(() =>
                {
                    UserDataManager.Instance.SaveSoundVolume(audioSound.volume > 0f ? 0f : 1f);

                    UpdateImageSound();

                    if (!useSlider)
                    {
                        return;
                    }

                    if (audioSound.isPlaying)
                    {
                        sliderSound.value = 1f;

                        if (soundAsMusic)
                        {
                            sliderSound.value = 1f;
                        }
                    }
                    else
                    {
                        sliderSound.value = 0f;

                        if (soundAsMusic)
                        {
                            sliderSound.value = 0f;
                        }
                    }
                });

                if (soundMini && btnSoundMini != null)
                {
                    btnSoundMini.onClick.RemoveAllListeners();

                    btnSoundMini.onClick.AddListener(() =>
                    {
                        UserDataManager.Instance.SaveSoundVolume(audioSound.volume > 0f ? 0f : 1f);

                        UpdateImageSound();

                        if (!useSlider)
                        {
                            return;
                        }

                        if (audioSound.isPlaying)
                        {
                            sliderSound.value = 1f;

                            if (soundAsMusic)
                            {
                                sliderSound.value = 1f;
                            }
                        }
                        else
                        {
                            sliderSound.value = 0f;

                            if (soundAsMusic)
                            {
                                sliderSound.value = 0f;
                            }
                        }
                    });
                }
            }

            if (btnMusic != null)
            {
                ChangeImgMusic();

                if (useSlider)
                {
                    sliderMusic.value = UserDataManager.Instance.userDataSave.musicVolume;
                }

                btnMusic.onClick.RemoveAllListeners();

                btnMusic.onClick.AddListener(() =>
                {
                    UserDataManager.Instance.SaveMusicVolume(audioMusic.volume > 0f ? 0f : 1f);

                    ChangeImgMusic();

                    if (useSlider)
                    {
                        sliderMusic.value = UserDataManager.Instance.userDataSave.musicVolume;
                    }
                });
            }

            if (btnVibration != null)
            {
                ChangeImgVibration();

                btnVibration.onClick.RemoveAllListeners();

                btnVibration.onClick.AddListener(() =>
                {
                    UserDataManager.Instance.SetVibration();

                    ChangeImgVibration();
                });
            }

            if (!useSlider)
            {
                return;
            }

            if (sliderMusic != null)
            {
                sliderMusic.value = UserDataManager.Instance.userDataSave.musicVolume;

                sliderMusic.onValueChanged.RemoveAllListeners();
                sliderMusic.onValueChanged.AddListener(ChangeMusicVolume);
            }

            if (sliderSound == null)
            {
                return;
            }

            sliderSound.value = UserDataManager.Instance.userDataSave.soundVolume;

            sliderSound.onValueChanged.RemoveAllListeners();
            sliderSound.onValueChanged.AddListener(ChangeSoundVolume);
        }
    }
}