﻿using GameAssets.Scripts.Event;
using UnityEngine;
using UnityEngine.Rendering;

namespace GameAssets.Scripts.Manager.Game
{
    public partial class GameManager
    {
        protected override void Awake()
        {
            base.Awake();

            GameEvent.OnWin += OnWin;
            GameEvent.OnDraw += OnDraw;
            GameEvent.OnLose += OnLose;

            GameEvent.OnFailedInternet += OnFailedInternet;
            GameEvent.OnConnectedInternet += OnConnectedInternet;

            Camera = Camera.main;
            QualitySettings.vSyncCount = 0;

#if UNITY_EDITOR || !UNITY_WEBGL
            Application.targetFrameRate = 120;
            OnDemandRendering.renderFrameInterval = 1;
#endif
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            GameEvent.OnWin -= OnWin;
            GameEvent.OnDraw -= OnDraw;
            GameEvent.OnLose -= OnLose;

            GameEvent.OnFailedInternet -= OnFailedInternet;
            GameEvent.OnConnectedInternet -= OnConnectedInternet;
        }

        public override void InnerUpdate()
        {
            foreach (var x in characters)
            {
                x.currentState?.UpdateState();
            }
        }
    }
}