﻿using GameAssets.Scripts.Base;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.Manager.Game
{
    public partial class GameManager
    {
        public GameState currentState;

        public void AddBaseCharacter(params BaseCharacter[] character)
        {
            foreach (var x in character)
            {
                if (!characters.Contains(x))
                {
                    characters.Add(x);
                }
            }
        }

        public void SetGameState(GameState gameState)
        {
            if (Equals(currentState, gameState))
            {
                return;
            }

            currentState = gameState;

            Debug.Log($"GAME STATE => {gameState}");
        }

        public bool IsGameState(GameState state)
        {
            return currentState == state;
        }
    }
}