﻿using System.Collections.Generic;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.UI.UIGameComplete;
using GameAssets.Scripts.UI.UIGameDraw;
using GameAssets.Scripts.UI.UIGameOver;
using UnityEngine;

namespace GameAssets.Scripts.Manager.Game
{
    public partial class GameManager : BaseSingleton<GameManager>
    {
        [SerializeField] private List<BaseCharacter> characters = new List<BaseCharacter>();

        private void OnWin()
        {
            UIManager.Instance.HideAllAndShowUI<UIGameComplete>();

            if (IsGameState(GameState.Win))
            {
                return;
            }

            AudioManager.Instance.PlaySound("level_finish");
            UserDataManager.Instance.LevelUp();

            SetGameState(GameState.Win);
        }

        private void OnDraw()
        {
            UIManager.Instance.HideAllAndShowUI<UIGameDraw>();

            if (IsGameState(GameState.Draw))
            {
                return;
            }

            AudioManager.Instance.PlaySound("level_failed");
            GameEvent.DoStopBaseCharacter();

            SetGameState(GameState.Draw);
        }

        private void OnLose()
        {
            UIManager.Instance.HideAllAndShowUI<UIGameOver>();

            if (IsGameState(GameState.Lose))
            {
                return;
            }

            AudioManager.Instance.PlaySound("level_failed");
            GameEvent.DoStopBaseCharacter();

            SetGameState(GameState.Lose);
        }

        private void OnFailedInternet()
        {
            if (!IsGameState(GameState.Playing))
            {
                return;
            }

            SetGameState(GameState.LostConnected);

            SpawnerEx.CreateNotification("Please enable your wifi/3g to continue", "Thông báo",
                () => SceneManager.LoadScene("Game Menu"));
        }

        private void OnConnectedInternet()
        {
            if (!IsGameState(GameState.LostConnected))
            {
                return;
            }

            SetGameState(GameState.Playing);

            SpawnerEx.CreateNotification("Reconnected");
        }
    }
}