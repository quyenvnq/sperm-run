﻿using System.Collections.Generic;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager.Audio;
using UnityEngine;

namespace GameAssets.Scripts.Manager
{
    public class LevelManager : BaseSingleton<LevelManager>
    {
        [SerializeField] private List<Model.Level> levels = new List<Model.Level>();

        [SerializeField] private Transform parent;

        [SerializeField] private bool isCreate = true;

        public Model.Level Level { get; private set; }

        protected override void Awake()
        {
            base.Awake();

            LoadLevel();
        }

        private void LoadLevel()
        {
            if (!isCreate || levels.Count < 1)
            {
                return;
            }

            Level = levels[UserDataManager.Instance.userDataSave.level % levels.Count];

            if (Level == null)
            {
                return;
            }

            var clone = SpawnerEx.CreateSpawner(Vector3.zero, parent, Level.prefab);

            clone.transform.localPosition = Vector3.zero;

            if (Level != null && Level.clip != null)
            {
                AudioManager.Instance.PlayMusic(Level.clip.name);
            }
        }
    }
}