﻿using GameAssets.Scripts.Base;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.Manager.Localization
{
    [RequireComponent(typeof(Image))]
    public class LocalizationImage : BaseLocalization<Image, Sprite>
    {
        protected override void ChangeValue(SystemLanguage language, object value)
        {
            var icon = value as Sprite;

            if (icon != null)
            {
                main.sprite = icon;
            }
        }
    }
}