using System;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Manager.Localization
{
    public class LocalizationManager : BaseSingleton<LocalizationManager>
    {
        [DisableIf(nameof(awakeSystemLanguage))] [SerializeField]
        private SystemLanguage languageDefault;

        [SerializeField] private bool awakeSystemLanguage;

        public SystemLanguage LanguageDefault()
        {
            if (PlayerPrefs.HasKey(nameof(SystemLanguage)))
            {
                return (SystemLanguage) Enum.Parse(typeof(SystemLanguage),
                    PlayerPrefs.GetString(nameof(SystemLanguage)));
            }

            return languageDefault;
        }

        private void OnValidate()
        {
#if UNITY_EDITOR
            if (awakeSystemLanguage)
            {
                languageDefault = Application.systemLanguage;
            }
#endif
        }

        public void ChangeLanguage(SystemLanguage localizationLanguage)
        {
            languageDefault = localizationLanguage;

            PlayerPrefs.SetString(nameof(SystemLanguage), languageDefault.ToString());
            GameEvent.DoChangeLanguage(localizationLanguage);
        }
    }
}