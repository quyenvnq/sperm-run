﻿using GameAssets.Scripts.Base;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.Manager.Localization
{
    [RequireComponent(typeof(TMP_InputField))]
    public class LocalizationPlaceholderTMPInputField : BaseLocalization<TextMeshProUGUI, string>
    {
        public override void Initialize()
        {
            foreach (var x in GetComponentsInChildren<TextMeshProUGUI>())
            {
                if (x.name != "Placeholder")
                {
                    continue;
                }

                main = x;
                break;
            }
        }

        protected override void ChangeValue(SystemLanguage language, object value)
        {
            if (value is string s)
            {
                main.text = s;
            }
        }
    }
}