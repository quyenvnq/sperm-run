﻿using GameAssets.Scripts.Base;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.Manager.Localization
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizationTMPro : BaseLocalization<TextMeshProUGUI, string>
    {
        protected override void ChangeValue(SystemLanguage language, object value)
        {
            if (value is string s)
            {
                main.text = s;
            }
        }
    }
}