using DG.Tweening;
using UnityEngine.SceneManagement;
using SM = UnityEngine.SceneManagement.SceneManager;

namespace GameAssets.Scripts.Manager
{
    public static class SceneManager
    {
        public static void ResetCurrentScene()
        {
            LoadScene(SM.GetActiveScene().name);
        }

        public static void LoadScene(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Single)
        {
            DOTween.Clear(true);
            SM.LoadSceneAsync(sceneName, loadSceneMode);
        }

        public static void LoadSplash()
        {
            LoadScene("Game Splash", LoadSceneMode.Additive);
        }

        public static string CurrentScene => SM.GetActiveScene().name;
    }
}