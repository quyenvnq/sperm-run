#define UNITY_FB_SDK
#define UNITY_FIREBASE_SDK
#define UNITY_IRONSOURCE_SDK

#if UNITY_FB_SDK
using Facebook.Unity;
#endif
#if UNITY_FIREBASE_SDK
using Firebase;
using Firebase.Analytics;
#endif
using System;
using GameAssets.Scripts.Base;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Manager
{
    public class SdkManager : BaseSingleton<SdkManager>
    {
        [Header("[FACEBOOK]")] [SerializeField]
        private bool initFbSdk;

        [Header("[FIREBASE]")] [SerializeField]
        private bool initFirebaseSdk;

        [ShowIf(nameof(initFirebaseSdk))] [SerializeField]
        private bool initFirebaseAnalytics = true;

        [Header("[IRON SOURCE]")] [SerializeField]
        private bool initIronSource;

        [ShowIf(nameof(initIronSource))] [SerializeField]
        private string appKey;

        #region MonoBehaviour

        protected override void Awake()
        {
            base.Awake();

            try
            {
                if (initFbSdk)
                {
#if UNITY_FB_SDK
                    FB.Init(FbInitComplete);
                    Debug.Log($"FB Called with {FB.AppId}");
#endif
                }

                if (initFirebaseSdk)
                {
#if UNITY_FIREBASE_SDK
                    FirebaseApp.CheckAndFixDependenciesAsync()
                        .ContinueWith(task =>
                        {
                            if (initFirebaseAnalytics)
                            {
                                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                            }
                        });
#endif
                }

                if (initIronSource)
                {
#if UNITY_IRONSOURCE_SDK
                    if (string.IsNullOrEmpty(appKey) || string.IsNullOrWhiteSpace(appKey))
                    {
                        Debug.LogError("App key iron source can't null/empty/white space");
                        return;
                    }

                    try
                    {
                        IronSource.Agent.init(appKey, IronSourceAdUnits.INTERSTITIAL);
                        IronSource.Agent.validateIntegration();
                        IronSource.Agent.loadInterstitial();

                        IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
                        IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
                        IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
                        IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent;
                        IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
                        IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
                        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;

                        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
                        IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
                        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
                        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent +=
                            RewardedVideoAvailabilityChangedEvent;
                        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
                        IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
                        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
                        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
                    }
                    catch (Exception ex)
                    {
                        Debug.Log($"Your phone haven't wifi to init: {ex}");
                    }
#endif
                }
            }
            catch
            {
                // ignored
            }
        }

        private void OnValidate()
        {
#if UNITY_EDITOR
            /*var buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            var scripts = new List<string>();

            PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup, out var scripting);

            if (initFbSdk && !scripting.Contains("UNITY_FB_SDK"))
            {
                scripts.Add("UNITY_FB_SDK");
            }
            else if (initFirebaseSdk && !scripting.Contains("UNITY_FIREBASE_SDK"))
            {
                scripts.Add("UNITY_FIREBASE_SDK");
            }

            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, string.Join(";", scripts.ToArray()));*/
#endif
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

#if UNITY_IRONSOURCE_SDK
            try
            {
                IronSourceEvents.onInterstitialAdReadyEvent -= InterstitialAdReadyEvent;
                IronSourceEvents.onInterstitialAdLoadFailedEvent -= InterstitialAdLoadFailedEvent;
                IronSourceEvents.onInterstitialAdShowSucceededEvent -= InterstitialAdShowSucceededEvent;
                IronSourceEvents.onInterstitialAdShowFailedEvent -= InterstitialAdShowFailedEvent;
                IronSourceEvents.onInterstitialAdClickedEvent -= InterstitialAdClickedEvent;
                IronSourceEvents.onInterstitialAdOpenedEvent -= InterstitialAdOpenedEvent;
                IronSourceEvents.onInterstitialAdClosedEvent -= InterstitialAdClosedEvent;

                IronSourceEvents.onRewardedVideoAdOpenedEvent -= RewardedVideoAdOpenedEvent;
                IronSourceEvents.onRewardedVideoAdClickedEvent -= RewardedVideoAdClickedEvent;
                IronSourceEvents.onRewardedVideoAdClosedEvent -= RewardedVideoAdClosedEvent;
                IronSourceEvents.onRewardedVideoAvailabilityChangedEvent -= RewardedVideoAvailabilityChangedEvent;
                IronSourceEvents.onRewardedVideoAdStartedEvent -= RewardedVideoAdStartedEvent;
                IronSourceEvents.onRewardedVideoAdEndedEvent -= RewardedVideoAdEndedEvent;
                IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;
                IronSourceEvents.onRewardedVideoAdShowFailedEvent -= RewardedVideoAdShowFailedEvent;
            }
            catch (Exception ex)
            {
                Debug.Log($"Your phone haven't wifi to remove event: {ex}");
            }
#endif
        }

        private void OnApplicationPause(bool status)
        {
#if UNITY_IRONSOURCE_SDK
            try
            {
                IronSource.Agent.onApplicationPause(status);
            }
            catch (Exception ex)
            {
                Debug.Log($"Your phone haven't wifi to application pause: {ex}");
            }
#endif
        }

        #endregion

        #region FACEBOOK

        private static void FbInitComplete()
        {
            Debug.Log("FB init complete");
        }

        #endregion

        #region FIREBASE

        #endregion

        #region IRON SOURCE

        public static void ShowInterstitialAd()
        {
#if UNITY_IRONSOURCE_SDK
            try
            {
                if (IronSource.Agent.isInterstitialReady())
                {
                    IronSource.Agent.showInterstitial();
                }
                else
                {
                    SceneManager.ResetCurrentScene();
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Your phone haven't wifi to show interstitial ad: {ex}");
                SceneManager.ResetCurrentScene();
            }
#endif
        }

        public static void ShowRewardAd()
        {
#if UNITY_IRONSOURCE_SDK
            try
            {
                if (IronSource.Agent.isRewardedVideoAvailable())
                {
                    IronSource.Agent.showRewardedVideo();
                }
                else
                {
                    SceneManager.ResetCurrentScene();
                }
            }
            catch (Exception ex)
            {
                Debug.Log($"Your phone haven't wifi to show reward ad: {ex}");
                SceneManager.ResetCurrentScene();
            }
#endif
        }

        // Invoked when the initialization process has failed.
        // @param description - string - contains information about the failure.
        private void InterstitialAdLoadFailedEvent(IronSourceError error)
        {
        }

        // Invoked when the ad fails to show.
        // @param description - string - contains information about the failure.
        private void InterstitialAdShowFailedEvent(IronSourceError error)
        {
            SceneManager.ResetCurrentScene();
        }

        // Invoked when end user clicked on the interstitial ad
        private void InterstitialAdClickedEvent()
        {
        }

        // Invoked when the interstitial ad closed and the user goes back to the application screen.
        private void InterstitialAdClosedEvent()
        {
            try
            {
                IronSource.Agent.loadInterstitial();
            }
            catch (Exception ex)
            {
                Debug.Log($"Your phone haven't wifi to load interstitial ad again: {ex}");
            }

            SceneManager.ResetCurrentScene();
        }

        // Invoked when the Interstitial is Ready to shown after load function is called
        private void InterstitialAdReadyEvent()
        {
        }

        // Invoked when the Interstitial Ad Unit has opened
        private void InterstitialAdOpenedEvent()
        {
        }

        // Invoked right before the Interstitial screen is about to open.
        // NOTE - This event is available only for some of the networks. 
        // You should treat this event as an interstitial impression, but rather use InterstitialAdOpenedEvent
        private void InterstitialAdShowSucceededEvent()
        {
            SceneManager.ResetCurrentScene();
        }

        //Your Activity will lose focus. Please avoid performing heavy 
        //tasks till the video ad will be closed.
        private void RewardedVideoAdOpenedEvent()
        {
        }

        //Invoked when the RewardedVideo ad view is about to be closed.
        //Your activity will now regain its focus.
        private void RewardedVideoAdClosedEvent()
        {
            SceneManager.ResetCurrentScene();
        }

        //Invoked when there is a change in the ad availability status.
        //@param - available - value will change to true when rewarded videos are available. 
        //You can then show the video by calling showRewardedVideo().
        //Value will change to false when no videos are available.
        private void RewardedVideoAvailabilityChangedEvent(bool available)
        {
            //Change the in-app 'Traffic Driver' state according to availability.
        }

        //Invoked when the user completed the video and should be rewarded. 
        //If using server-to-server callbacks you may ignore this events and wait for 
        // the callback from the  ironSource server.
        //@param - placement - placement object which contains the reward data
        private void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
        {
            SceneManager.ResetCurrentScene();
        }

        //Invoked when the Rewarded Video failed to show
        //@param description - string - contains information about the failure.
        private void RewardedVideoAdShowFailedEvent(IronSourceError error)
        {
            SceneManager.ResetCurrentScene();
        }

        // ----------------------------------------------------------------------------------------
        // Note: the events below are not available for all supported rewarded video ad networks. 
        // Check which events are available per ad network you choose to include in your build. 
        // We recommend only using events which register to ALL ad networks you include in your build. 
        // ----------------------------------------------------------------------------------------

        //Invoked when the video ad starts playing. 
        private void RewardedVideoAdStartedEvent()
        {
        }

        //Invoked when the video ad finishes playing. 
        private void RewardedVideoAdEndedEvent()
        {
        }

        //Invoked when the video ad is clicked. 
        private void RewardedVideoAdClickedEvent(IronSourcePlacement placement)
        {
        }

        #endregion
    }
}