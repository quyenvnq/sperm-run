﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Manager
{
    [ExecuteInEditMode]
    public class UIManager : BaseSingleton<UIManager>
    {
        [SerializeField] private List<BaseUI> uis = new List<BaseUI>();

        [ValueDropdown(nameof(Dropdown))] [SerializeField]
        private string uiAwake;

        private Dictionary<string, BaseUI> _uiDictionaries = new Dictionary<string, BaseUI>();

        #region MonoBehaviour

        protected override void Awake()
        {
            base.Awake();

            foreach (var x in uis)
            {
                x.gameObject.SetActive(false);
            }

            _uiDictionaries = uis.ToDictionary(x => x.GetType().Name, x => x);

            _uiDictionaries[uiAwake].DoAnimation(true);
        }

        #endregion

        #region Public

        public void HideAllAndShowUI<T>() where T : BaseUI
        {
            foreach (var x in uis)
            {
                x.DoAnimation(string.Equals(x.GetType().Name, typeof(T).Name,
                    StringComparison.CurrentCultureIgnoreCase));
            }
        }

        public T GetUI<T>() where T : BaseUI
        {
            return _uiDictionaries.TryGetValue(typeof(T).Name, out var x) ? (T) x : null;
        }

        public T ShowUI<T>() where T : BaseUI
        {
            var t = GetUI<T>();

            if (t != null)
            {
                t.DoAnimation(true);
            }

            return t;
        }

        public void HideUI<T>() where T : BaseUI
        {
            GetUI<T>()?.DoAnimation(false);
        }

        public bool IsUI<T>() where T : BaseUI
        {
            var t = GetUI<T>();
            return t != null && t.gameObject.activeInHierarchy;
        }

        #endregion

        #region Private

        [Button]
        private List<ValueDropdownItem> Dropdown()
        {
            var dropdown = new List<ValueDropdownItem>
            {
                new ValueDropdownItem
                {
                    Text = "None",
                    Value = "None"
                }
            };

            uis.Clear();

            void FindBase(IEnumerable tr)
            {
                foreach (var x in tr)
                {
                    var t = x as Transform;

                    if (t == null)
                    {
                        continue;
                    }

                    FindBase(t);

                    var ui = t.GetComponent<BaseUI>();

                    if (ui == null)
                    {
                        continue;
                    }

                    dropdown.Add(new ValueDropdownItem
                    {
                        Text = ui.name,
                        Value = ui.name
                    });

                    uis.Add(ui);
                }
            }

            FindBase(transform);

            uiAwake = dropdown[1].Text;
            return dropdown;
        }

        #endregion

        #region Protected

        #endregion
    }
}