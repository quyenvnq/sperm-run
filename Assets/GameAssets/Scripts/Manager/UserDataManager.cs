﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Manager
{
    public class UserDataManager : BaseSingleton<UserDataManager>
    {
        [SerializeField] private List<Ability> abilities = new List<Ability>();

        public UserDataSave userDataSave;

        #region MonoBehaviour

        protected override void Awake()
        {
            base.Awake();

            userDataSave = GetData();

            if (userDataSave == null)
            {
                ResetData();
            }
        }

        #endregion

        #region Public

        public void SetVibration()
        {
            userDataSave.vibration = !userDataSave.vibration;

            SaveData();
        }

        public void LevelUp()
        {
            userDataSave.level++;

            SaveData();
        }

        public bool CheckBuy(float price)
        {
            if (userDataSave.gold <= 0 || userDataSave.gold < price)
            {
                return false;
            }

            AddGold(-price);
            return true;
        }

        public void AddGold(float value)
        {
            userDataSave.gold += value;

            SaveData();
        }

        public void AddDiamond(float value)
        {
            userDataSave.diamond += value;

            SaveData();
        }

        public void SetRemovedAds()
        {
            userDataSave.removedAds = true;

            SaveData();
        }

        public void UpdateAbility(Ability ability)
        {
            foreach (var x in userDataSave.abilities
                .Where(x => x.id == ability.id))
            {
                x.SetAbility(ability);
                break;
            }
        }

        public void SaveMusicVolume(float volume)
        {
            userDataSave.musicVolume = volume;

            SaveData();
        }

        public void SaveSoundVolume(float volume)
        {
            userDataSave.soundVolume = volume;

            SaveData();
        }

        public bool HasMusic => userDataSave.musicVolume > 0f;
        public bool HasSound => userDataSave.soundVolume > 0f;

        public override void Initialize()
        {
            abilities.Clear();
            abilities.AddRange(Resources.LoadAll<Ability>("Ability"));
        }

        #endregion

        #region Private

        private static UserDataSave GetData()
        {
            var json = Decrypt(PlayerPrefs.GetString(Const.SAVE_DATA));

            if (json.Length > 0)
            {
                Debug.Log($"GAME DATA => {json}");
            }

            return JsonConvert.DeserializeObject<UserDataSave>(json);
        }

        private static string Encrypt(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            var keyBytes = new Rfc2898DeriveBytes(Const.PASSWORD_HASH,
                Encoding.ASCII.GetBytes(Const.SALT_KEY)).GetBytes(256 / 8);

            var symmetricKey = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.Zeros
            };

            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(Const.VI_KEY));
            byte[] cipherTextBytes;

            using var memoryStream = new MemoryStream();
            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
            {
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();

                cipherTextBytes = memoryStream.ToArray();

                cryptoStream.Close();
            }

            memoryStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }

        private static string Decrypt(string encryptedText)
        {
            var cipherTextBytes = Convert.FromBase64String(encryptedText);
            var keyBytes = new Rfc2898DeriveBytes(Const.PASSWORD_HASH,
                Encoding.ASCII.GetBytes(Const.SALT_KEY)).GetBytes(256 / 8);

            var symmetricKey = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.None
            };

            var decrypt = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(Const.VI_KEY));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decrypt, CryptoStreamMode.Read);
            var plainTextBytes = new byte[cipherTextBytes.Length];
            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

            memoryStream.Close();
            cryptoStream.Close();

            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }

        [Button]
        private void SaveData()
        {
            var json = JsonConvert.SerializeObject(userDataSave);

            Debug.Log($"GAME DATA SAVED => {json}");
            PlayerPrefs.SetString(Const.SAVE_DATA, Encrypt(json));
            PlayerPrefs.Save();
        }

        [Button]
        private void ResetData()
        {
            userDataSave = new UserDataSave
            {
                level = 0,
                diamond = 0,
                gold = 0,
                musicVolume = 1f,
                soundVolume = 1f,
                vibration = true,
                removedAds = false,
                abilities = new List<Ability>()
            };

            userDataSave.abilities.AddRange(abilities);

            PlayerPrefs.DeleteAll();

            SaveData();
        }

        #endregion

        #region Protected

        #endregion
    }
}