﻿using System;
using System.Collections.Generic;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Manager;
using UnityEngine;

namespace GameAssets.Scripts.Model
{
    [CreateAssetMenu(fileName = "Ability", menuName = "UI/Ability", order = 0)]
    public class Ability : BaseScriptableObject
    {
        public List<int> mores = new List<int>();
        public List<int> effects = new List<int>();

        public int maxLevel;
        public int level;
        public int baseEffect;

        public bool IsMaxLevel()
        {
            return level >= maxLevel;
        }

        public int Effect()
        {
            return level == 0 ? baseEffect : effects[(level - 1) % effects.Count];
        }

        public int Price()
        {
            return mores[level % mores.Count];
        }

        public bool CanUpgrade()
        {
            var price = Price();

            if (UserDataManager.Instance.userDataSave.gold <= 0
                || UserDataManager.Instance.userDataSave.gold < price
                || level >= maxLevel)
            {
                return false;
            }

            level++;

            UserDataManager.Instance.UpdateAbility(this);
            UserDataManager.Instance.AddGold(-price);
            return true;
        }

        public void SetAbility(Ability ability)
        {
            level = ability.level;
        }
    }

    [Serializable]
    public class AbilityData : BaseScriptableObjectData
    {
        public int level;

        public void SetAbility(Ability ability)
        {
            level = ability.level;
        }
    }
}