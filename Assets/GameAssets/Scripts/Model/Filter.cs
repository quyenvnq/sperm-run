﻿using System;
using GameAssets.Scripts.GamePlay;
using Button = UnityEngine.UI.Button;

namespace GameAssets.Scripts.Model
{
    [Serializable]
    public class Filter
    {
        public Button btnFilter;
        public Page pageFilter;
        public General.FilterType filterType;
    }
}