﻿using GameAssets.Scripts.Base;
using UnityEngine;

namespace GameAssets.Scripts.Model
{
    [CreateAssetMenu(fileName = "Level", menuName = "UI/Level", order = 0)]
    public class Level : BaseScriptableObject
    {
        public AudioClip clip;
        public GameObject prefab;

        public int level;
        public int energy;
        public int time = 60;
        public float target;

        public bool completed;
    }
}