﻿using System;
using System.Collections.Generic;

namespace GameAssets.Scripts.Model
{
    [Serializable]
    public class UserDataSave
    {
        public int level;
        
        public float gold;
        public float diamond;

        public float soundVolume;
        public float musicVolume;
        
        public bool vibration;
        public bool removedAds;

        public List<Ability> abilities = new List<Ability>();
    }
}