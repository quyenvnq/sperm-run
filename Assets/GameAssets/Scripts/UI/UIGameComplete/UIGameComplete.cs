using GameAssets.Scripts.Base;
using GameAssets.Scripts.GamePlay;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIGameComplete
{
    public partial class UIGameComplete : BaseUI
    {
        [SerializeField] private Model.Ability incomeAbility;
        
        [SerializeField] private Button btnGameComplete;
        [SerializeField] private Button btnAdsGold;

        [SerializeField] private TextMeshProUGUI txtGold;
        [SerializeField] private TextMeshProUGUI txtAdsGold;
    }
}