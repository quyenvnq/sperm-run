using GameAssets.Scripts.Manager;

namespace GameAssets.Scripts.UI.UIGameDraw
{
    public partial class UIGameDraw
    {
        private void Awake()
        {
            btnGameDraw.AddListener(SceneManager.ResetCurrentScene);
        }
    }
}