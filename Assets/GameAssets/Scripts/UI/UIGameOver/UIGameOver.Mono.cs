using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.Manager;

namespace GameAssets.Scripts.UI.UIGameOver
{
    public partial class UIGameOver
    {
        private void Awake()
        {
            btnGameOver.AddListener(SdkManager.ShowInterstitialAd);

            txtGold.text = $"{Player.Instance.TotalGold}";
        }
    }
}