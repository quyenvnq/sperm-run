using GameAssets.Scripts.Base;
using GameAssets.Scripts.GamePlay;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIGameOver
{
    public partial class UIGameOver : BaseUI
    {
        [SerializeField] private Button btnGameOver;

        [SerializeField] private TextMeshProUGUI txtGold;
    }
}