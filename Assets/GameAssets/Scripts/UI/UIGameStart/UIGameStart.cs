using DG.Tweening;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UIGameStart
{
    public class UIGameStart : BaseUI
    {
        [SerializeField] private GameObject lbPregnant;

        [SerializeField] private Image imgPregnantFill;

        [SerializeField] private TextMeshProUGUI txtLevel;
        [SerializeField] private TextMeshProUGUI txtGold;

        [SerializeField] private float pregnantFillDuration = 1f;

        #region MonoBehaviour

        private void Start()
        {
            txtLevel.text = $"Level {UserDataManager.Instance.userDataSave.level + 1}";
            txtGold.text = $"{UserDataManager.Instance.userDataSave.gold:0}";

            GameManager.Instance.SetGameState(GameState.Playing);
            GameEvent.DoStartGame();
        }

        #endregion

        #region Public

        public void UpdatePregnantFill(float percent)
        {
            if (!lbPregnant.activeInHierarchy)
            {
                lbPregnant.SetActive(true);
            }

            imgPregnantFill.DOFillAmount(percent, pregnantFillDuration);
        }

        #endregion

        #region Private

        #endregion

        #region Protected

        #endregion
    }
}