using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIPlayMenu
{
    public class UIPlayMenu : BaseUI
    {
        [SerializeField] private TextMeshProUGUI txtLevel;
        [SerializeField] private TextMeshProUGUI txtGold;

        #region MonoBehaviour

        private void Start()
        {
            txtLevel.text = $"Level {UserDataManager.Instance.userDataSave.level + 1}";

            UpdateGold();
        }

        #endregion

        #region Public

        public void UpdateGold()
        {
            txtGold.NumberCounter(UserDataManager.Instance.userDataSave.gold);
        }

        #endregion

        #region Private

        #endregion

        #region Protected

        #endregion
    }
}